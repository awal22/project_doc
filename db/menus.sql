-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_title` varchar(255) NOT NULL,
  `menu_url` varchar(255) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `menu_parent` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu_title_menu_url` (`menu_title`,`menu_url`),
  KEY `menu_title_idx` (`menu_title`),
  KEY `menu_url_idx` (`menu_url`),
  KEY `menu_parent_idx` (`menu_parent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menus` (`id`, `menu_title`, `menu_url`, `menu_order`, `menu_parent`, `active`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1,	'Dashboard',	'dashboard',	1,	0,	1,	'2019-10-09 16:08:19',	NULL,	1,	NULL),
(2,	'Settings',	'#',	2,	0,	1,	'2019-10-09 16:16:08',	'2019-10-19 03:36:08',	1,	1),
(3,	'User',	'user',	1,	2,	1,	'2019-10-09 16:16:08',	NULL,	1,	NULL),
(4,	'Role',	'role',	2,	2,	1,	'2019-10-09 16:16:08',	NULL,	1,	NULL),
(5,	'peralatan',	'peralatan',	5,	0,	1,	'2019-10-13 08:51:11',	'2019-10-19 16:40:20',	1,	1),
(6,	'Menu',	'menu',	3,	2,	1,	'2019-10-13 10:17:51',	NULL,	1,	NULL),
(7,	'Lokasi',	'#',	3,	0,	1,	'2019-10-19 06:05:12',	'2019-10-19 16:22:06',	1,	1),
(8,	'Kobu',	'kobu',	1,	7,	1,	'2019-10-19 06:37:38',	NULL,	1,	NULL),
(9,	'Kacab Pembina',	'kacab_pembina',	2,	7,	1,	'2019-10-19 16:01:51',	NULL,	1,	NULL),
(10,	'LPPNPI',	'lppnpi',	3,	7,	1,	'2019-10-19 16:23:04',	NULL,	1,	NULL),
(11,	'Master',	'#',	4,	0,	1,	'2019-10-19 16:40:10',	NULL,	1,	NULL),
(12,	'Merek',	'merk',	1,	11,	1,	'2019-10-19 16:41:25',	'2019-10-19 16:44:50',	1,	1),
(13,	'Tipe',	'tipe',	2,	11,	1,	'2019-10-19 16:56:56',	NULL,	1,	NULL),
(14,	'Lokasi Alat',	'lokasi_alat',	3,	11,	1,	'2019-10-19 17:03:51',	NULL,	1,	NULL),
(15,	'Fungsi',	'fungsi',	4,	11,	1,	'2019-10-19 17:04:34',	NULL,	1,	NULL),
(16,	'Pelayanan',	'pelayanan',	5,	11,	1,	'2019-10-19 17:09:16',	NULL,	1,	NULL);

-- 2019-10-20 05:29:41
