-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP VIEW IF EXISTS `v_user_roles`;
CREATE TABLE `v_user_roles` (`id` int(11), `ur_user` int(11), `ur_role` int(11), `created_at` timestamp, `updated_at` timestamp, `created_by` int(11), `updated_by` int(11), `user_name` varchar(100), `user_password` varchar(255), `user_email` varchar(255), `active` int(11), `role_code` char(2), `role_name` varchar(100));


DROP TABLE IF EXISTS `v_user_roles`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_user_roles` AS select `ur`.`id` AS `id`,`ur`.`ur_user` AS `ur_user`,`ur`.`ur_role` AS `ur_role`,`ur`.`created_at` AS `created_at`,`ur`.`updated_at` AS `updated_at`,`ur`.`created_by` AS `created_by`,`ur`.`updated_by` AS `updated_by`,`u`.`user_name` AS `user_name`,`u`.`user_password` AS `user_password`,`u`.`user_email` AS `user_email`,`u`.`active` AS `active`,`r`.`role_code` AS `role_code`,`r`.`role_name` AS `role_name` from ((`user_roles` `ur` join `users` `u` on((`ur`.`ur_user` = `u`.`id`))) join `roles` `r` on((`ur`.`ur_role` = `r`.`id`)));

-- 2019-10-20 05:31:23
