-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `menu_roles`;
CREATE TABLE `menu_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mr_menu` int(11) NOT NULL,
  `mr_role` int(11) NOT NULL,
  `mr_create` int(11) NOT NULL,
  `mr_read` int(11) NOT NULL,
  `mr_update` int(11) NOT NULL,
  `mr_delete` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mr_menu_mr_role` (`mr_menu`,`mr_role`),
  KEY `mr_menu_idx` (`mr_menu`),
  KEY `mr_role_idx` (`mr_role`),
  CONSTRAINT `menu_roles_ibfk_1` FOREIGN KEY (`mr_menu`) REFERENCES `menus` (`id`),
  CONSTRAINT `menu_roles_ibfk_2` FOREIGN KEY (`mr_role`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `menu_roles` (`id`, `mr_menu`, `mr_role`, `mr_create`, `mr_read`, `mr_update`, `mr_delete`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1,	1,	1,	1,	1,	1,	1,	'2019-10-09 16:08:36',	NULL,	1,	NULL),
(2,	2,	1,	1,	1,	1,	1,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(3,	3,	1,	1,	1,	1,	1,	'2019-10-09 16:16:46',	NULL,	1,	NULL),
(4,	4,	1,	1,	1,	1,	1,	'2019-10-09 16:16:46',	NULL,	1,	NULL),
(5,	5,	1,	1,	1,	1,	1,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(7,	6,	1,	1,	1,	1,	1,	'2019-10-13 10:18:31',	NULL,	1,	NULL),
(8,	7,	1,	0,	1,	0,	0,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(9,	7,	2,	0,	1,	0,	0,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(10,	8,	1,	1,	1,	1,	1,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(11,	8,	2,	1,	1,	0,	0,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(12,	9,	1,	1,	1,	1,	1,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(13,	9,	2,	1,	1,	0,	0,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(14,	10,	1,	1,	1,	1,	1,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(15,	10,	2,	1,	1,	0,	0,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(16,	11,	1,	0,	1,	0,	0,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(17,	11,	2,	0,	1,	0,	0,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(18,	12,	1,	1,	1,	1,	1,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(19,	12,	2,	0,	1,	0,	0,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(20,	13,	1,	1,	1,	1,	1,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(21,	13,	2,	0,	1,	0,	0,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(22,	14,	1,	1,	1,	1,	1,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(23,	14,	2,	0,	1,	0,	0,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(24,	15,	1,	1,	1,	1,	1,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(25,	15,	2,	0,	1,	0,	0,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(26,	16,	1,	1,	1,	1,	1,	'0000-00-00 00:00:00',	NULL,	1,	NULL),
(27,	16,	2,	0,	1,	0,	0,	'0000-00-00 00:00:00',	NULL,	1,	NULL);

-- 2019-10-20 05:30:17
