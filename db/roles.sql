-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_code` char(2) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_code` (`role_code`),
  KEY `role_code_idx` (`role_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `roles` (`id`, `role_code`, `role_name`, `active`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1,	'00',	'administrator',	1,	'2019-10-09 16:04:49',	NULL,	1,	NULL),
(2,	'01',	'writer',	0,	'2019-10-13 09:37:39',	NULL,	1,	NULL);

-- 2019-10-20 05:30:08
