-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ur_user` int(11) NOT NULL,
  `ur_role` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ur_user_ur_role` (`ur_user`,`ur_role`),
  KEY `ur_role` (`ur_role`),
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`ur_user`) REFERENCES `users` (`id`),
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`ur_role`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user_roles` (`id`, `ur_user`, `ur_role`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1,	1,	1,	'2019-10-09 16:06:57',	NULL,	1,	NULL);

-- 2019-10-20 05:30:38
