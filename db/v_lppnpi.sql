-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP VIEW IF EXISTS `v_lppnpi`;
CREATE TABLE `v_lppnpi` (`id` int(11), `nama` varchar(50), `provinsi_id` int(11), `created_at` datetime, `created_by` int(11), `updated_at` datetime, `updated_by` int(11), `provinsi` varchar(50));


DROP TABLE IF EXISTS `v_lppnpi`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_lppnpi` AS select `l`.`id` AS `id`,`l`.`nama` AS `nama`,`l`.`provinsi_id` AS `provinsi_id`,`l`.`created_at` AS `created_at`,`l`.`created_by` AS `created_by`,`l`.`updated_at` AS `updated_at`,`l`.`updated_by` AS `updated_by`,`kp`.`nama` AS `provinsi` from (`lppnpi` `l` join `kacab_pembina` `kp` on((`l`.`provinsi_id` = `kp`.`id`)));

-- 2019-10-20 05:31:05
