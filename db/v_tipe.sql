-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP VIEW IF EXISTS `v_tipe`;
CREATE TABLE `v_tipe` (`id` int(11), `idmerk` int(11), `nama` varchar(50), `created_at` datetime, `created_by` int(11), `updated_at` datetime, `updated_by` int(11), `nama_merk` varchar(50));


DROP TABLE IF EXISTS `v_tipe`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_tipe` AS select `t`.`id` AS `id`,`t`.`idmerk` AS `idmerk`,`t`.`nama` AS `nama`,`t`.`created_at` AS `created_at`,`t`.`created_by` AS `created_by`,`t`.`updated_at` AS `updated_at`,`t`.`updated_by` AS `updated_by`,`m`.`nama` AS `nama_merk` from (`tipe` `t` join `merk` `m` on((`t`.`idmerk` = `m`.`id`)));

-- 2019-10-20 05:31:18
