-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email` (`user_email`),
  KEY `user_name_idx` (`user_name`),
  KEY `user_email_idx` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `user_name`, `user_password`, `user_email`, `active`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1,	'admin',	'$2y$10$FSkAyFIazz5RrRCs36/lM.x5LiiK/gcTFBi7TtAFJ2bjRPQsPEOwq',	'admin@localhost.com',	1,	'2019-10-09 16:06:35',	NULL,	1,	NULL);

-- 2019-10-20 05:30:28
