-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP VIEW IF EXISTS `v_peralatan`;
CREATE TABLE `v_peralatan` (`id` int(11), `fasilitas` enum('Communication','Navigation','Surveillance','ATC Automation'), `nama` varchar(45), `lat` varchar(45), `long` varchar(45), `tahun_installasi` int(11), `frekuensi` varchar(45), `ident` varchar(45), `merk_id` int(11), `lokasi_alat_id` int(11), `fungsi_id` int(11), `pelayanan_id` int(11), `lppnpi_id` int(11), `poweroutput` varchar(45), `catatan` text, `foto` varchar(45), `kondisi` varchar(45), `created_at` datetime, `created_by` int(11), `updated_at` datetime, `updated_by` int(11), `merk` varchar(50), `lokasi_alat` varchar(50), `fungsi` varchar(50), `pelayanan` varchar(45), `lppnpi` varchar(50));


DROP TABLE IF EXISTS `v_peralatan`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_peralatan` AS select `p`.`id` AS `id`,`p`.`fasilitas` AS `fasilitas`,`p`.`nama` AS `nama`,`p`.`lat` AS `lat`,`p`.`long` AS `long`,`p`.`tahun_installasi` AS `tahun_installasi`,`p`.`frekuensi` AS `frekuensi`,`p`.`ident` AS `ident`,`p`.`merk_id` AS `merk_id`,`p`.`lokasi_alat_id` AS `lokasi_alat_id`,`p`.`fungsi_id` AS `fungsi_id`,`p`.`pelayanan_id` AS `pelayanan_id`,`p`.`lppnpi_id` AS `lppnpi_id`,`p`.`poweroutput` AS `poweroutput`,`p`.`catatan` AS `catatan`,`p`.`foto` AS `foto`,`p`.`kondisi` AS `kondisi`,`p`.`created_at` AS `created_at`,`p`.`created_by` AS `created_by`,`p`.`updated_at` AS `updated_at`,`p`.`updated_by` AS `updated_by`,`m`.`nama` AS `merk`,`la`.`nama` AS `lokasi_alat`,`f`.`nama` AS `fungsi`,`ply`.`nama` AS `pelayanan`,`l`.`nama` AS `lppnpi` from (((((`peralatan` `p` join `merk` `m` on((`p`.`merk_id` = `m`.`id`))) join `lokasi_alat` `la` on((`p`.`lokasi_alat_id` = `la`.`id`))) join `fungsi` `f` on((`p`.`fungsi_id` = `f`.`id`))) join `pelayanan` `ply` on((`p`.`pelayanan_id` = `ply`.`id`))) join `lppnpi` `l` on((`p`.`lppnpi_id` = `l`.`id`)));

-- 2019-10-22 18:54:39
