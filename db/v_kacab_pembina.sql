-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP VIEW IF EXISTS `v_kacab_pembina`;
CREATE TABLE `v_kacab_pembina` (`id` int(11), `nama` varchar(50), `kobu_id` int(11), `created_at` datetime, `created_by` int(11), `updated_at` datetime, `updated_by` int(11), `nama_kobu` varchar(20));


DROP TABLE IF EXISTS `v_kacab_pembina`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_kacab_pembina` AS select `kp`.`id` AS `id`,`kp`.`nama` AS `nama`,`kp`.`kobu_id` AS `kobu_id`,`kp`.`created_at` AS `created_at`,`kp`.`created_by` AS `created_by`,`kp`.`updated_at` AS `updated_at`,`kp`.`updated_by` AS `updated_by`,`k`.`nama` AS `nama_kobu` from (`kacab_pembina` `kp` join `kobu` `k` on((`kp`.`kobu_id` = `k`.`id`)));

-- 2019-10-20 05:30:58
