-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP VIEW IF EXISTS `v_menu_roles`;
CREATE TABLE `v_menu_roles` (`id` int(11), `mr_menu` int(11), `mr_role` int(11), `mr_create` int(11), `mr_read` int(11), `mr_update` int(11), `mr_delete` int(11), `created_at` timestamp, `updated_at` timestamp, `created_by` int(11), `updated_by` int(11), `menu_title` varchar(255), `menu_url` varchar(255), `menu_order` int(11), `id_parent` int(11), `menu_parent` varchar(255), `role_code` char(2), `role_name` varchar(100));


DROP TABLE IF EXISTS `v_menu_roles`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_menu_roles` AS select `mr`.`id` AS `id`,`mr`.`mr_menu` AS `mr_menu`,`mr`.`mr_role` AS `mr_role`,`mr`.`mr_create` AS `mr_create`,`mr`.`mr_read` AS `mr_read`,`mr`.`mr_update` AS `mr_update`,`mr`.`mr_delete` AS `mr_delete`,`mr`.`created_at` AS `created_at`,`mr`.`updated_at` AS `updated_at`,`mr`.`created_by` AS `created_by`,`mr`.`updated_by` AS `updated_by`,`m`.`menu_title` AS `menu_title`,`m`.`menu_url` AS `menu_url`,`m`.`menu_order` AS `menu_order`,`m`.`menu_parent` AS `id_parent`,`mp`.`menu_title` AS `menu_parent`,`r`.`role_code` AS `role_code`,`r`.`role_name` AS `role_name` from (((`menu_roles` `mr` join `menus` `m` on((`mr`.`mr_menu` = `m`.`id`))) join `roles` `r` on((`mr`.`mr_role` = `r`.`id`))) left join `menus` `mp` on((`m`.`menu_parent` = `mp`.`id`)));

-- 2019-10-20 05:31:11
