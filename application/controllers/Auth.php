<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */

class Auth extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('user/V_user_role_model', 'vUserRole');
  }

  public function index()
  {
    if ($this->session->has_userdata('isLoggedIn')) {
      redirect('dashboard');
    }
    else {
      $this->load->view('login/index');
    }
  }

  public function login()
  {
    $user_email = $this->input->post('user_email', TRUE);
    $user_password = $this->input->post('user_password', TRUE);

    $param['where']['user_email'] = $user_email;
    $user = $this->vUserRole->get_data($param);
    
    if ( ! is_null($user)) {
      if (password_verify($user_password, $user->row()->user_password)) {
        $data['userId'] = $user->row()->ur_user;
        $data['roleId'] = $user->row()->ur_role;
        $data['userName'] = $user->row()->user_name;
        $data['userEmail'] = $user->row()->user_email;
        $data['roleCode'] = $user->row()->role_code;
        $data['roleName'] = $user->row()->role_name;
        $data['createdAt'] = $user->row()->created_at;
        $data['isLoggedIn'] = TRUE;

        $this->session->set_userdata($data);

        redirect('dashboard');
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Password salah";
        $this->session->set_flashdata($flashdata);

        redirect('Auth');
      }
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Email tidak terdaftar";
			$this->session->set_flashdata($flashdata);

      redirect('Auth');
    }
  }

  public function logout()
  {
    $this->session->sess_destroy();
    redirect('Auth');
  }
}