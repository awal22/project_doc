<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */

class Master_model extends CI_Model
{
  public function get_data(Array $param)
  {
    if (array_key_exists('select', $param)) {
      $this->db->select($param['select']);
    }

    if (array_key_exists('order_by', $param)) {
      foreach ($param['order_by'] as $key => $value) {
        $this->db->order_by($key, $value);
      }
    }

    if (array_key_exists('distinct', $param)) {
      $this->db->distinct();
    }

    if (array_key_exists('where', $param)) {
      if (is_array($param['where'])) {
        foreach ($param['where'] as $key => $value) {
          $this->db->where($key, $value);
        }
      }
      else {
        $this->db->where($param['where']);
      }
    }

    if (array_key_exists('or_where', $param)) {
      if (is_array($param['or_where'])) {
        foreach ($param['or_where'] as $key => $value) {
          $this->db->or_where($key, $value);
        }
      }
      else {
        $this->db->or_where($param['or_where']);
      }
    }

    if (array_key_exists('like', $param)) {
      if (is_array($param['like'])) {
        foreach ($param['like'] as $key => $value) {
          $this->db->like($key, $value);
        }
      }
      else {
        $this->db->like($param['like']);
      }
    }

    if (array_key_exists('or_like', $param)) {
      if (is_array($param['or_like'])) {
        foreach ($param['or_like'] as $key => $value) {
          $this->db->or_like($key, $value);
        }
      }
      else {
        $this->db->or_like($param['or_like']);
      }
    }

    $this->db->from($param['table']);

    return $this->db->get();
  }

  public function insert(Array $param)
  {
    $this->db->insert($param['table'], $param['data']);
    
    if (array_key_exists('affected_rows', $param)) {
      return $this->db->affected_rows();
    }
  }

  public function update(Array $param)
  {
    if (array_key_exists('where', $param)) {
      if (is_array($param['where'])) {
        foreach ($param['where'] as $key => $value) {
          $this->db->where($key, $value);
        }
      }
      else {
        $this->db->where($param['where']);
      }
    }

    $this->db->update($param['table'], $param['data']);

    if (array_key_exists('affected_rows', $param)) {
      return $this->db->affected_rows();
    }
  }

  public function delete(Array $param)
  {
    if (array_key_exists('where', $param)) {
      if (is_array($param['where'])) {
        foreach ($param['where'] as $key => $value) {
          $this->db->where($key, $value);
        }
      }
      else {
        $this->db->where($param['where']);
      }
    }

    $this->db->delete($param['table']);
  }

  private function _get_datatables_query(Array $param)
  {
    $this->db->from($param['table']);

    $i = 0;

    foreach ($param['column_search'] as $item) // loop column
    {
      if($param['post']['search']['value']) // if datatable send POST for search
      {
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $param['post']['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $param['post']['search']['value']);
        }

        if(count($param['column_search']) - 1 == $i) //last loop
        {
          $this->db->group_end(); //close bracket
        }
      }
      $i++;
    }

    if(isset($param['post']['order'])) // here order processing
    {
      $this->db->order_by($param['column_order'][$param['post']['order']['0']['column']], $param['post']['order']['0']['dir']);
    }
    else if(isset($param['order']))
    {
      $order = $param['order'];
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables(Array $param)
  {
    $this->_get_datatables_query($param);
    if($param['post']['length'] != -1)
    {
      $this->db->limit($param['post']['length'], $param['post']['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered(Array $param)
  {
    $this->_get_datatables_query($param);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all($table)
  {
    $this->db->from($table);
    return $this->db->count_all_results();
  }
}