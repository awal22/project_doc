<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CNSA DNP | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url("vendor/almasaeed2010/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css"); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url("vendor/almasaeed2010/adminlte/bower_components/font-awesome/css/font-awesome.min.css"); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url("vendor/almasaeed2010/adminlte/bower_components/Ionicons/css/ionicons.min.css"); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url("vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css"); ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- jQuery 3 -->
    <script src="<?php echo base_url("vendor/almasaeed2010/adminlte/bower_components/jquery/dist/jquery.min.js"); ?>"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url("vendor/almasaeed2010/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"); ?>"></script>
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="<?php echo site_url(); ?>"><b>CNSA DNP</a>
      </div>
      <!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <?php
        if ( ! is_null($this->session->flashdata('alert_class'))) {
          ?>
          <div class="alert alert-<?php echo $this->session->flashdata('alert_class'); ?> alert-dismissible" role="alert" id="my-alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $this->session->flashdata('alert_text'); ?>
          </div>
          <?php
        }
        ?>

        <form action="<?php echo site_url('Auth/login'); ?>" method="post">
          <div class="form-group has-feedback">
            <input type="email" name="user_email" id="user_email" class="form-control" placeholder="Email" autofocus="autofocus" autocomplete="off">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="user_password" id="user_password" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>
          </div>
        </form>

        <!-- <div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
            Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
            Google+</a>
        </div> -->
        <!-- /.social-auth-links -->

        <!-- <a href="<?php echo site_url('Forgot'); ?>">I forgot my password</a><br>
        <a href="<?php echo site_url('Register'); ?>" class="text-center">Register a new membership</a> -->

      </div>
      <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

    <script>
    $(document).ready (function(){
      $("#my-alert").fadeTo(4000, 500).slideUp(500, function(){
        $("#my-alert").slideUp(500);
      });
    });
    </script>
  </body>
</html>