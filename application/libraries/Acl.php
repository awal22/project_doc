<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */

class Acl
{
  var $CI;

  function __construct()
  {
    $this->CI =& get_instance();
    $this->CI->load->model('menu/V_menu_role_model', 'vMenuRole');
  }

  public function check_auth()
  {
    if (! $this->CI->session->has_userdata('isLoggedIn')) {
			$flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus login terlebih dahulu";
			$this->CI->session->set_flashdata($flashdata);

      redirect("Auth");
    }
  }

  public function check_create($modul)
  {
    $param["where"]["mr_role"] = $this->CI->session->userdata("roleId");
    $param["where"]["menu_url"] = $modul;
    $param["select"] = "menu_title, mr_create";

		$result = $this->CI->vMenuRole->get_data($param)->row();
		// var_dump($result);echo $result;exit;
		if ($result->mr_create === "0" OR $result === FALSE)
		{
			$flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda tidak memiliki hak akses untuk insert data di modul ".$result->menu_title;
			$this->CI->session->set_flashdata($flashdata);

			redirect(ucfirst($modul));
		}
  }

  public function check_read($modul)
  {
    $param["where"]["mr_role"] = $this->CI->session->userdata("roleId");
    $param["where"]["menu_url"] = $modul;
    $param["select"] = "menu_title, mr_read";

		$result = $this->CI->vMenuRole->get_data($param)->row();
		// var_dump($result);echo $result;exit;
		if ($result->mr_read === "0" OR $result === FALSE)
		{
			$flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda tidak memiliki hak akses ke modul ".$result->menu_title;
			$this->CI->session->set_flashdata($flashdata);

			redirect("Dashboard");
		}
  }

  public function check_update($modul)
  {
    $param["where"]["mr_role"] = $this->CI->session->userdata("roleId");
    $param["where"]["menu_url"] = $modul;
    $param["select"] = "menu_title, mr_update";

		$result = $this->CI->vMenuRole->get_data($param)->row();
		// var_dump($result);echo $result;exit;
		if ($result->mr_update === "0" OR $result === FALSE)
		{
			$flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda tidak memiliki hak akses untuk update data di modul ".$result->menu_title;
			$this->CI->session->set_flashdata($flashdata);

			redirect(ucfirst($modul));
		}
  }

  public function check_delete($modul)
  {
    $param["where"]["mr_role"] = $this->CI->session->userdata("roleId");
    $param["where"]["menu_url"] = $modul;
    $param["select"] = "menu_title, mr_delete";

		$result = $this->CI->vMenuRole->get_data($param)->row();
		// var_dump($result);echo $result;exit;
		if ($result->mr_delete === "0" OR $result === FALSE)
		{
			$flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda tidak memiliki hak akses untuk delete data di modul ".$result->menu_title;
			$this->CI->session->set_flashdata($flashdata);

			redirect(ucfirst($modul));
		}
  }
}