<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Awaluddin <awaluddinbinusep@gmail.com>
 */
class Arsip_crew extends MX_Controller
{
  private $modul = 'Arsip_crew';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));
    $this->load->library('upload');
    $this->load->model('arsip_crew/arsip_crew_model', 'arsip_crew');
    $this->load->model('arsip_crew/v_crew_model', 'v_arsip_crew');
    $this->load->model('user/user_model', 'user');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'index', $data);
  }

  public function add()
  {
    $this->acl->check_create(strtolower($this->modul));

    $param['where']['user_jabatan'] = 157;
    //$row = $this->user->get_data($param);
    $qryUserModel = $this->user->get_data($param);
    $data['crew'] = $qryUserModel->result();

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'Add';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'form', $data);
  }

  public function edit(int $id = NULL)
  {
    $this->acl->check_update(strtolower($this->modul));

    if ( ! is_null($id)) {
      $param['where']['arsip_crew_id'] = $id;
      $row = $this->arsip_crew->get_data($param);

      if ($row->num_rows() > 0) {
        $data['row'] = $row->row();
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak ditemukan";
        $this->session->set_flashdata($flashdata);
  
        redirect(strtolower($this->modul));
      }

      $param['where']['user_jabatan'] = 157;
      //$row = $this->user->get_data($param);
      $qryUserModel = $this->v_arsip_crew->get_data();
      $data['crew'] = $qryUserModel->result();
  
      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Edit';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'form', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
			$this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function simpan()
  {
    if($_FILES['arsip_file']['name'])
    {
      $fileName = "file_1_".time();
      $config['file_name'] = $fileName;
      $config['upload_path'] = './arsip_file/';
      $config['allowed_types'] = 'ico|gif|jpg|png|JPG|PNG|JPEG|jpeg|PDF|pdf';
      $config['max_size'] = 2048;

      $this->upload->initialize($config);
      if($this->upload->do_upload('arsip_file'))
      {
        $arsip = $this->upload->data();
        $arsip_file = $arsip['file_name'];
        }
        else
        {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
    }
    else
    {
      $arsip_file = "";
    }

    $id = $this->input->post('id', TRUE);
    $nomor = $this->input->post('nomor', TRUE);
    $nama = $this->input->post('nama', TRUE);
    $arsip_deskripsi = $this->input->post('deskripsi', TRUE);
    $tgl_expired = $this->input->post('expired', TRUE);
    $save = $this->input->post('save', TRUE);

    switch ($save) {
      case 'save':
        $data['arsip_crew_nomor'] = $nomor;
        $data['arsip_crew_nama'] = $nama;
        $data['arsip_crew_deskripsi'] = $arsip_deskripsi;
        $data['arsip_crew_file'] = $arsip_file;
        $data['arsip_crew_expired'] = $tgl_expired;
        $data['arsip_crew_created_at'] = date('Y-m-d H:i:s');
        $data['arsip_crew_created_by'] = $this->session->userdata('userId');
        $insert = $this->arsip_crew->insert($data);

        if ($insert == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else 
        {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
        
      if($_FILES['arsip_file']['name'])
        {
          $fileName = "file_1_".time();
          $config['file_name'] = $fileName;
          

          $this->upload->initialize($config);
          if($this->upload->do_upload('arsip_file'))
          {
            $file_arsip = $this->upload->data();
            $arsip_file = $file_arsip['file_name'];
            }
            else
            {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
          }
        }
        

      case 'update':
      $config['upload_path'] = './arsip_file/';
      $config['allowed_types'] = 'ico|gif|jpg|png|JPG|PNG|jpeg|JPEG|PDF|pdf';
      $config['max_size'] = 2048;
      $this->upload->initialize($config);
        if($this->upload->do_upload('arsip_file'))
        {
          $file_arsip = $this->upload->data();
          $arsip_update = $file_arsip['file_name'];
          $param['data']['arsip_crew_nomor'] = $nomor;
          $param['data']['arsip_crew_nama'] = $nama;
          $param['data']['arsip_crew_deskripsi'] = $arsip_deskripsi;
          $param['data']['arsip_crew_expired'] = $tgl_expired;
          $param['data']['arsip_crew_file'] = $arsip_update;
         
          $param['data']['arsip_crew_update_at'] = date('Y-m-d H:i:s');
          $param['data']['arsip_crew_update_by'] = $this->session->userdata('userId');
          $param['where']['arsip_crew_id'] = $id;
        }
        else
        {
          $param['data']['arsip_crew_nomor'] = $nomor;
          $param['data']['arsip_crew_nama'] = $nama;
          $param['data']['arsip_crew_deskripsi'] = $arsip_deskripsi;
          $param['data']['arsip_crew_expired'] = $tgl_expired;
         
          $param['data']['arsip_crew_update_at'] = date('Y-m-d H:i:s');
          $param['data']['arsip_crew_update_by'] = $this->session->userdata('userId');
          $param['where']['arsip_crew_id'] = $id;
        }
        $update = $this->arsip_crew->update($param);

        if ($update == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));   
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

  public function hapus(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));

    $param['where']['arsip_crew_id'] = $id;
    $this->arsip_crew->delete($param);

    $flashdata["alert_class"] = "success";
    $flashdata["alert_text"] = "Data berhasil dihapus";
    $this->session->set_flashdata($flashdata);

    redirect(strtolower($this->modul));
  }

  public function datagrid()
  {
    $list = $this->v_arsip_crew->get_datatables($this->input->post(NULL, TRUE));
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rw->arsip_crew_nomor;
      $row[] = $rw->arsip_crew_nama;
      $row[] = $rw->arsip_crew_deskripsi;
      $row[] = $rw->arsip_crew_expired;
      $row[] = $rw->status_dokumen;
      if($rw->arsip_crew_file)
      {
          $row[] = '<a href="'. base_url('arsip_file/'.$rw->arsip_crew_file).'" target="_blank">'.$rw->arsip_crew_file.'</a>';
      }
      else
      {
          $row[] = '(No file)';
      }
      $row[] = $rw->arsip_crew_created_at;
      $row[] = "<div class=\"text-center\">
              <a href=\"".site_url(strtolower($this->modul)."/edit/".$rw->arsip_crew_id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>
              <button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->arsip_crew_id."','".$rw->arsip_crew_nomor."');\">Delete</button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->v_arsip_crew->count_all(),
      "recordsFiltered" => $this->v_arsip_crew->count_filtered($this->input->post(NULL, TRUE)),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }

}