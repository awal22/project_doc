<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Table <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul.'/add'); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus-circle"></i> Tambah</a>
        <button type="button" title="Refresh" onclick="reload_table();" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-refresh"></i> Refresh</button>
      </span>
    </div>
  </div>

  <div class="box-body">
  
    <?php
    if ( ! is_null($this->session->flashdata('alert_class')))
    {
      ?>
      <div id="alert" class="alert alert-<?php echo $this->session->flashdata('alert_class'); ?>" role="alert"><?php echo $this->session->flashdata('alert_text'); ?></div>
      <?php
    }
    ?>

    <table id="table-<?php echo $modul; ?>" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th class="text-center">No</th>
          <th class="text-center">Nama Pegawai</th>
          <th class="text-center">Tanggal Ijin</th>
          <th class="text-center">Status</th>
          <th class="text-center">Jabatan</th>
          <th class="text-center">Created At</th>
          <th class="text-center" style="width:200px;">Action</th>
        </tr>
      </thead>

      <tbody>
      </tbody>
    </table>

  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $("#alert").fadeTo(2000, 500).slideUp(500, function(){
    $("#alert").slideUp(500);
  });

  //datatables
  var table = $('#table-<?php echo $modul; ?>').DataTable({

    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": "<?php echo site_url($modul.'/datagrid')?>",
      "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      {
        "targets": [ 0 ], //first column
        "orderable": false, //set not orderable
      },
      {
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },

    ],

  });

  //check all
  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });

  $("#success-alert").fadeTo(30000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
  });
});

function deleteItem(i,a)
{
  if (confirm("Anda yakin menghapus data "+a+"?"))
  {
    window.location = "<?php echo site_url($modul.'/hapus/'); ?>"+i;
  }
  return false;
}

function reload_table()
{
  $('#table-<?php echo $modul; ?>').DataTable().ajax.reload(); //reload datatable ajax
}
</script>