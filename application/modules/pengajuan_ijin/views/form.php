<div class="row">
  <div class="col-md-12">
    <div class="box box-solid box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Form <?php echo $page_header; ?></h3>
        <div class="box-tools pull-right">
          <span class="label label-primary">
            <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
          </span>
        </div>
      </div>

      <div class="box-body">

        <?php
        $id = isset($row) ? $row->ijin_id : '';
        $user_name = isset($row) ? $row->ijin_nama : '';
        $id_user = $this->session->userdata('userId');
        $ijin_jenis = isset($row) ? $row->ijin_jenis : '';
        $ijin_status = isset($row) ? $row->ijin_status : '';
        $keterangan = isset($row) ? $row->ijin_ket : '';
        $tanggal = isset($row) ? $row->ijin_tanggal : '';
        $btn_val = isset($row->ijin_id) ? 'update' : 'save';

        ?>

        <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post">
          <input type="hidden" name="id" value="<?php echo $id; ?>">
          <input type="hidden" name="user_name" id="user_name" value="<?php echo $user_name; ?>">
          <input type="hidden" name="id_user" id="id_user" value="<?php echo $id_user; ?>">
          <input type="hidden" name="tanggal_pengajuan" id="tanggal_pengajuan" value="<?php echo $tanggal; ?>">

          <div class="row">
          <div class="col-md-4">
            <div class="form-group">
                <label for="tanggal_ijin">Tanggal Ijin</label>
                <input type="text" name="tanggal" id="tanggal" value="<?php echo $tanggal; ?>" class="form-control" placeholder="Tanggal Ijin" autofocus="autofocus" autocomplete="off" readonly>
              </div>
          </div>
          <div class="col-md-9">
            <div class="form-group">
              <label for="nama">Jenis Ijin</label>
              <select name="jenis" id="jenis" class="form-control">  
                <option value="0" <?php echo $selected = $ijin_jenis == "0" ? "selected=\"selected\"" : "";?>>Pilih Jenis</option>
                <option value="1" <?php echo $selected = $ijin_jenis == "1" ? "selected=\"selected\"" : "";?>>Ijin Cuti</option>
                <option value="2" <?php echo $selected = $ijin_jenis == "2" ? "selected=\"selected\"" : "";?>>Ijin Sakit</option>
                <option value="3" <?php echo $selected = $ijin_jenis == "3" ? "selected=\"selected\"" : "";?>>Ijin Menikah</option>
                <option value="4" <?php echo $selected = $ijin_jenis == "4" ? "selected=\"selected\"" : "";?>>Ijin Darurat</option>
              </select>
            </div>
          
            <div class="form-group">
              <label for="keterangan">Keterangan Cuti</label>
              <textarea name="keterangan" id="keterangan" cols="30" rows="10" placeholder="Keterangan Cuti..." class="form-control"><?php echo $keterangan; ?></textarea>
            </div>

            <div class="form-group">
              <label for="nama">Status Ijin</label>
              <select name="status" id="status" class="form-control">  
                <option value="0" <?php echo $selected = $ijin_status == "0" ? "selected=\"selected\"" : "";?>>Pengajuan</option>
                <option value="1" <?php echo $selected = $ijin_status == "1" ? "selected=\"selected\"" : "";?>>Disetujui</option>
                <option value="2" <?php echo $selected = $ijin_status == "2" ? "selected=\"selected\"" : "";?>>Tidak disetujui</option>
              </select>
            </div>
          
        <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
      </form>
      </div>   
    </div>
  </div>  
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box box-solid box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Data Log Activity Crew Tanggal <?php echo $tanggal; ?></h3>
      </div>
      <div class="box-body table-responsive">
     

        <table class="table table-bordered table-striped table-hover" id="log_activity">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Crew</th>
              <th>Kapal</th>
              <th>Tgl Berlayar</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>

<script>
  $(document).ready(function () {
    $("#tanggal").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      orientation: "bottom auto",
      todayHighlight: true,
      todayBtn: "linked"
    });


  var table_manifest = $('#log_activity').DataTable({

    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": "<?php echo site_url('pengajuan_ijin/manifest_grid')?>",
      "type": "POST",
      "data": function ( data ) {
        data.user_name = $('#user_name').val();
        data.tanggal = $('#tanggal_pengajuan').val();
      }

    },

    //Set column definition initialisation properties.
    "columnDefs": [
      {
        "targets": [ 0 ], //first column
        "orderable": false, //set not orderable
      },
      {
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },

    ],

  });
  });
  </script>