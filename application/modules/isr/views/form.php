<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->id : '';
    $no_rekomendasi = isset($row) ? $row->no_rekomendasi : '';
    $no_isr = isset($row) ? $row->no_isr : '';
    $tanggal_berlaku = isset($row) ? $row->masa_berlaku : '';
    $lppnpi_id = isset($row) ? $row->no_lppnpi_id : '';
    $provinsi_id = isset($row) ? $row->provinsi_id : '';
    $kobu_id = isset($row) ? $row->kobu_id : '';

    $bukti = isset($row) ? $row->bukti : '';
    $btn_val = isset($row) ? 'update' : 'save';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $id; ?>">

      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="no_rekomendasi">NO Rekomendasi</label>
            <input type="text" name="no_rekomendasi" id="no_rekomendasi" value="<?php echo $no_rekomendasi; ?>" class="form-control" placeholder="NO Rekomendasi" autofocus="autofocus" autocomplete="off">
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="no_isr">NO ISR</label>
            <input type="text" name="no_isr" id="no_isr" value="<?php echo $no_isr; ?>" class="form-control" placeholder="NO ISR" autofocus="autofocus" autocomplete="off">
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="tanggal_terbit">Masa Berlaku</label>
            <input type="text" name="tanggal_berlaku" id="tanggal_berlaku" value="<?php echo $tanggal_berlaku; ?>" class="form-control" placeholder="Tanggal Berlaku" autofocus="autofocus" autocomplete="off">
          </div>
        </div>
      </div>  

      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="kobu">KOBU</label>
            <select name="kobu" id="kobu" class="form-control">
              <option value="">Pilih KOBU</option>
              <?php
              foreach ($kobu as $vKobu) {
                $selKobu = $kobu_id == $vKobu->id ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vKobu->id; ?>" <?php echo $selKobu; ?>><?php echo $vKobu->nama; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="kacab_pembina">Kacab Pembina</label>
            <select name="kacab_pembina" id="kacab_pembina" class="form-control">
              <option value="">Pilih Kacab Pembina</option>
              <?php
              if (isset($kacabPembina)) {
                foreach ($kacabPembina as $vKacabPembina) {
                  $selKacabPembina = $provinsi_id == $vKacabPembina->id ? 'selected="selected"' : '';
                  ?>
                  <option value="<?php echo $vKacabPembina->id; ?>" <?php echo $selKacabPembina; ?>><?php echo $vKacabPembina->nama; ?></option>
                  <?php
                }
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="lppnpi">LPPNPI</label>
            <select name="lppnpi_id" id="lppnpi_id" class="form-control">
              <option value="">Pilih LPPNPI</option>
              <?php
              foreach ($lppnpi as $vLppnpi) {
                $selLppnpi = $lppnpi_id == $vLppnpi->id ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vLppnpi->id; ?>" <?php echo $selLppnpi; ?>><?php echo $vLppnpi->nama; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>
      </div>
      
      <div class="row"> 
        <div class="col-md-4"> 
          <div class="form-group">
            <label for="bukti">Bukti</label>
            <input type="file" name="bukti" id="bukti">
          </div>
        </div>  
      </div>

      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>


<script>
$(document).ready(function () {
  $("#tanggal_berlaku").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    orientation: "bottom auto",
    todayHighlight: true,
    todayBtn: "linked"
  });

  $("#kobu").change(function () {
    var kobu_id = $("#kobu").val();
    $.ajax({
      url: '<?php echo site_url('kacab_pembina/get_data_by_kobu');?>',
      data: {kobu_id: kobu_id},
      method: 'POST',
      dataType: 'json',
      success: function (response) {
        $("#kacab_pembina").empty();
        $("#kacab_pembina").append('<option value="">Pilih Kacab Pembina</option');
        $.each(response, function (key, value) {
          $("#kacab_pembina").append('<option value="'+value.id+'">'+value.nama+'</option>');
        })
      }
    });
  });

  $("#kacab_pembina").change(function () {
    var provinsi_id = $("#kacab_pembina").val();
    $.ajax({
      url: '<?php echo site_url('lppnpi/get_data_by_provinsi');?>',
      data: {provinsi_id: provinsi_id},
      method: 'POST',
      dataType: 'json',
      success: function (response) {
        $("#lppnpi_id").empty();
        $("#lppnpi_id").append('<option value="">Pilih LPPNPI</option');
        $.each(response, function (key, value) {
          $("#lppnpi_id").append('<option value="'+value.id+'">'+value.nama+'</option>');
        })
      }
    });
  });
});
</script>