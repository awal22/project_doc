<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Identitas_website extends MX_Controller
{
  private $modul = 'Identitas_website';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));
    $this->load->library('upload');
    $this->load->model('identitas_website/identitas_web_model', 'identitas');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);
    $id = "id_identitas";
    $param['order_by']['id_identitas'] = $id;
    $row = $this->identitas->get_data($param);
    if ($row->num_rows() > 0) {
      $data['row'] = $row->row();
    }

    $this->template->load('templates/admin', 'form', $data);
  }

  public function simpan()
  {
    if($_FILES['favicon']['name'])
    {
      $fileName = "file_1_".time();
      $config['file_name'] = $fileName;
      $config['upload_path'] = './foto/';
      $config['allowed_types'] = 'ico|gif|jpg|png';
      $config['max_size'] = 2048;

      $this->upload->initialize($config);
      if($this->upload->do_upload('favicon'))
      {
        $gbr = $this->upload->data();
        $favicon = $gbr['file_name'];
        }
        else
        {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
    }
    else
    {
      $favicon = "";
    }

    $id = $this->input->post('id', TRUE);
    $nama = $this->input->post('nama_web', TRUE);
    $alamat = $this->input->post('alamat_web', TRUE);
    $kontak = $this->input->post('kontak_web', TRUE);
    $twitter = $this->input->post('twitter_web', TRUE);
    $facebook = $this->input->post('facebook_web', TRUE);
    $instagram = $this->input->post('instagram_web', TRUE);
    $google = $this->input->post('google_web', TRUE);
    $linkedin = $this->input->post('linkedin_web', TRUE);
    $email = $this->input->post('email_web', TRUE);
    $deskripsi = $this->input->post('meta_deskripsi', TRUE);
    $keyword = $this->input->post('meta_keyword', TRUE);
    $save = $this->input->post('save', TRUE);

    switch ($save) {
      case 'save':
        $data['nama_website'] = $nama;
        $data['alamat_website'] = $alamat;
        $data['kontak_website'] = $kontak;
        $data['twitter_website'] = $twitter;
        $data['facebook_website'] = $facebook;
        $data['instagram_website'] = $instagram;
        $data['google_website'] = $google;
        $data['linkedin_website'] = $linkedin;
        $data['email_website'] = $email;
        $data['meta_deskripsi'] = $deskripsi;
        $data['meta_keyword'] = $keyword;
        $data['favicon'] = $favicon;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->session->userdata('userId');
        $insert = $this->identitas->insert($data);

        if ($insert == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else 
        {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
        
      if($_FILES['favicon']['name'])
        {
          $fileName = "file_1_".time();
          $config['file_name'] = $fileName;
          

          $this->upload->initialize($config);
          if($this->upload->do_upload('favicon'))
          {
            $gbr = $this->upload->data();
            $favicon = $gbr['file_name'];
            }
            else
            {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
          }
        }

      case 'update':
      $config['upload_path'] = './foto/';
      $config['allowed_types'] = 'ico|gif|jpg|png';
      $config['max_size'] = 2048;
      $this->upload->initialize($config);
        if($this->upload->do_upload('favicon'))
        {
          $gbr = $this->upload->data();
          $favicon_update = $gbr['file_name'];
          $param['data']['nama_website'] = $nama;
          $param['data']['alamat_website'] = $alamat;
          $param['data']['kontak_website'] = $kontak;
          $param['data']['twitter_website'] = $twitter;
          $param['data']['facebook_website'] = $facebook;
          $param['data']['instagram_website'] = $instagram;
          $param['data']['google_website'] = $google;
          $param['data']['linkedin_website'] = $linkedin;
          $param['data']['email_website'] = $email;
          $param['data']['meta_deskripsi'] = $deskripsi;
          $param['data']['meta_keyword'] = $keyword;
          $param['data']['favicon'] = $favicon_update;
         
          $param['data']['updated_at'] = date('Y-m-d H:i:s');
          $param['data']['updated_by'] = $this->session->userdata('userId');
          $param['where']['id_identitas'] = $id;
        }
        else
        {
          $param['data']['nama_website'] = $nama;
          $param['data']['alamat_website'] = $alamat;
          $param['data']['kontak_website'] = $kontak;
          $param['data']['twitter_website'] = $twitter;
          $param['data']['facebook_website'] = $facebook;
          $param['data']['instagram_website'] = $instagram;
          $param['data']['google_website'] = $google;
          $param['data']['linkedin_website'] = $linkedin;
          $param['data']['email_website'] = $email;
          $param['data']['meta_deskripsi'] = $deskripsi;
          $param['data']['meta_keyword'] = $keyword;
         
          $param['data']['updated_at'] = date('Y-m-d H:i:s');
          $param['data']['updated_by'] = $this->session->userdata('userId');
          $param['where']['id_identitas'] = $id;
        }
        $update = $this->identitas->update($param);

        if ($update == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));   
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

}