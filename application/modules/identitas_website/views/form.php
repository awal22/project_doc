<?php
  if ( ! is_null($this->session->flashdata('alert_class')))
  {
    ?>
    <div id="alert" class="alert alert-<?php echo $this->session->flashdata('alert_class'); ?>" role="alert"><?php echo $this->session->flashdata('alert_text'); ?></div>
    <?php
  }
?>
<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->id_identitas : '';
    $nama = isset($row) ? $row->nama_website : '';
    $alamat = isset($row) ? $row->alamat_website : '';
    $kontak = isset($row) ? $row->kontak_website : '';
    $twitter = isset($row) ? $row->twitter_website : '';
    $facebook = isset($row) ? $row->facebook_website : '';
    $instagram = isset($row) ? $row->instagram_website : '';
    $google = isset($row) ? $row->google_website : '';
    $linkedin = isset($row) ? $row->linkedin_website : '';
    $email = isset($row) ? $row->email_website : '';
    $deskripsi = isset($row) ? $row->meta_deskripsi : '';
    $keyword = isset($row) ? $row->meta_keyword : '';
    $foto = isset($row) ? $row->favicon : '';

    $btn_val = isset($row) ? 'update' : 'save';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label for="nama">Nama Website</label>
        <input type="text" name="nama_web" id="nama_web" value="<?php echo $nama; ?>" class="form-control" placeholder="Nama Website" autofocus="autofocus" autocomplete="off">
        <label for="alamat">Alamat Website</label>
        <input type="text" name="alamat_web" id="alamat_web" value="<?php echo $alamat; ?>" class="form-control" placeholder="Alamat URL Website" autofocus="autofocus" autocomplete="off">
        <label for="kontak">Kontak Website</label>
        <input type="text" name="kontak_web" id="kontak_web" value="<?php echo $kontak; ?>" class="form-control" placeholder="Kontak website" autofocus="autofocus" autocomplete="off">
        <label for="twitter">Akun Twitter Official</label>
        <input type="text" name="twitter_web" id="twitter_web" value="<?php echo $twitter; ?>" class="form-control" placeholder="Akun Twitter Official" autofocus="autofocus" autocomplete="off">
        <label for="facebook">Akun Facebook Official</label>
        <input type="text" name="facebook_web" id="facebook_web" value="<?php echo $facebook; ?>" class="form-control" placeholder="Akun Facebook Official" autofocus="autofocus" autocomplete="off">
        <label for="instagram">Akun Instagram Official</label>
        <input type="text" name="instagram_web" id="instagram_web" value="<?php echo $instagram; ?>" class="form-control" placeholder="Akun Instagram Official" autofocus="autofocus" autocomplete="off">
        <label for="google">Akun Google+ Official</label>
        <input type="text" name="google_web" id="google_web" value="<?php echo $google; ?>" class="form-control" placeholder="Akun Google+ Official" autofocus="autofocus" autocomplete="off">
        <label for="linkedin">Akun LinkedIn Official</label>
        <input type="text" name="linkedin_web" id="linkedin_web" value="<?php echo $linkedin; ?>" class="form-control" placeholder="Akun LinkedIn Official" autofocus="autofocus" autocomplete="off">
        <label for="email">Email Website</label>
        <input type="text" name="email_web" id="email_web" value="<?php echo $email; ?>" class="form-control" placeholder="Email Website" autofocus="autofocus" autocomplete="off">
        <label for="meta_deskripsi">Meta Deskripsi</label>
        <input type="text" name="meta_deskripsi" id="meta_deskripsi" value="<?php echo $deskripsi; ?>" class="form-control" placeholder="Meta Deskripsi" autofocus="autofocus" autocomplete="off">
        <label for="meta_keyword">Meta Keyword</label>
        <input type="text" name="meta_keyword" id="meta_keyword" value="<?php echo $keyword; ?>" class="form-control" placeholder="Meta Keyword" autofocus="autofocus" autocomplete="off">
        <label for="favicon">Favicon</label>
        <input type="file" name="favicon" id="favicon" class="form-control" value="<?php echo $foto; ?>">
        <label>Favicon Aktif Saat ini : <?php echo "<img src='".base_url()."foto/$foto'>" ?>
      </div>
      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>