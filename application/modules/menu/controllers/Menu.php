<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Menu extends MX_Controller
{
  private $modul = 'Menu';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));

    $this->load->model('menu/Menu_model', 'menu');
    $this->load->model('menu/V_menu_role_model', 'vMenuRole');
    $this->load->model('role/Role_model', 'role');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));
    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'index', $data);
  }

  public function add()
  {
    $this->acl->check_create(strtolower($this->modul));
    
    $parent = $this->menu->get_data(NULL);
    $data['parent'] = $parent->result();
    
    $role = $this->role->get_data(NULL);
    $data['role'] = $role->result();

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'Add';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'form', $data);
  }

  public function edit(int $id = NULL)
  {
    $this->acl->check_update(strtolower($this->modul));

    if ( ! is_null($id)) {
      $param['where']['id'] = $id;
      $row = $this->menu->get_data($param);

      if ($row->num_rows() > 0) {
        $data['row'] = $row->row();
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak ditemukan";
        $this->session->set_flashdata($flashdata);
  
        redirect(strtolower($this->modul));
      }

      $parent = $this->menu->get_data(NULL);
      $data['parent'] = $parent->result();
      
      $role = $this->role->get_data(NULL);
      $data['role'] = $role->result();
  
      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Edit';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'form', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
			$this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function simpan()
  {
    $id = $this->input->post('id', TRUE);
    $menu_title = $this->input->post('menu_title', TRUE);
    $menu_url = $this->input->post('menu_url', TRUE);
    $menu_order = $this->input->post('menu_order', TRUE);
    $menu_parent = $this->input->post('menu_parent', TRUE);
    $active = $this->input->post('active', TRUE);
    $mr_role = $this->input->post('mr_role', TRUE);
    $mr_create = $this->input->post('mr_create', TRUE);
    $mr_read = $this->input->post('mr_read', TRUE);
    $mr_update = $this->input->post('mr_update', TRUE);
    $mr_delete = $this->input->post('mr_delete', TRUE);
    $save = $this->input->post('save', TRUE);

    switch ($save) {
      case 'save':
        $data['menu']['menu_title'] = $menu_title;
        $data['menu']['menu_url'] = $menu_url;
        $data['menu']['menu_order'] = $menu_order;
        $data['menu']['menu_parent'] = $menu_parent;
        $data['menu']['active'] = $active;
        $data['menu']['created_at'] = date('Y-m-d H:i:s');
        $data['menu']['created_by'] = $this->session->userdata('userId');

        $data['mr']['mr_role'] = $mr_role;
        $data['mr']['mr_create'] = $mr_create;
        $data['mr']['mr_read'] = $mr_read;
        $data['mr']['mr_update'] = $mr_update;
        $data['mr']['mr_delete'] = $mr_delete;
        $data['mr']['mr_role'] = $mr_role;
        $insert = $this->menu->insert($data);

        if ($insert == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;

      case 'update':
        $param['data']['menu_title'] = $menu_title;
        $param['data']['menu_url'] = $menu_url;
        $param['data']['menu_order'] = $menu_order;
        $param['data']['menu_parent'] = $menu_parent;
        $param['data']['active'] = $active;
        $param['data']['updated_at'] = date('Y-m-d H:i:s');
        $param['data']['updated_by'] = $this->session->userdata('userId');
        $param['where']['id'] = $id;

        $param['mr_role'] = $this->input->post('mr_role');
        $param['mr_create'] = $this->input->post('mr_create');
        $param['mr_read'] = $this->input->post('mr_read');
        $param['mr_update'] = $this->input->post('mr_update');
        $param['mr_delete'] = $this->input->post('mr_delete');

        $update = $this->menu->update($param);

        if ($update == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

  public function hapus(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));

    $param['where']['id'] = $id;
    $this->menu->delete_by_id($id);

    $flashdata["alert_class"] = "success";
    $flashdata["alert_text"] = "Data berhasil dihapus";
    $this->session->set_flashdata($flashdata);

    redirect(strtolower($this->modul));
  }

  public function datagrid()
  {
    $list = $this->vMenuRole->get_datatables($this->input->post(NULL, TRUE));
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rw->menu_title;
      $row[] = $rw->menu_url;
      $row[] = $rw->menu_parent;
      $row[] = "<div class=\"text-center\">
              <a href=\"".site_url(strtolower($this->modul)."/edit/".$rw->mr_menu)."\" class=\"btn btn-primary btn-flat btn-xs\" title=\"Edit\"><i class=\"fa fa-edit\"></i></a>
              <button type=\"button\" title=\"Delete\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->mr_menu."','".$rw->menu_title."');\"><i class=\"fa fa-trash\"></i></button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vMenuRole->count_all(),
      "recordsFiltered" => $this->vMenuRole->count_filtered($this->input->post(NULL, TRUE)),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }
}