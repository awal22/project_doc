<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */

class Menu_model extends CI_Model
{
  private $table = 'menus';

  public function get_data(Array $param = NULL)
  {
    if ( ! is_null($param)) {
      if (array_key_exists('select', $param)) {
        $this->db->select($param['select']);
      }

      if (array_key_exists('distinct', $param)) {
        $this->db->distinct();
      }

      if (array_key_exists('where', $param)) {
        if (is_array($param['where'])) {
          foreach ($param['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($param['where']);
        }
      }

      if (array_key_exists('or_where', $param)) {
        if (is_array($param['or_where'])) {
          foreach ($param['or_where'] as $key => $value) {
            $this->db->or_where($key, $value);
          }
        }
        else {
          $this->db->or_where($param['or_where']);
        }
      }

      if (array_key_exists('like', $param)) {
        if (is_array($param['like'])) {
          foreach ($param['like'] as $key => $value) {
            $this->db->like($key, $value);
          }
        }
        else {
          $this->db->like($param['like']);
        }
      }

      if (array_key_exists('or_like', $param)) {
        if (is_array($param['or_like'])) {
          foreach ($param['or_like'] as $key => $value) {
            $this->db->or_like($key, $value);
          }
        }
        else {
          $this->db->or_like($param['or_like']);
        }
      }
    }

    $this->db->from($this->table);

    return $this->db->get();
  }

  public function insert($data)
  {
    $this->db->trans_begin();

    // echo $this->db->set($data['menu'])->get_compiled_insert($this->table);
    $this->db->insert($this->table, $data['menu']);
    $id_menu = $this->db->insert_id();

    foreach ($data['mr']['mr_role'] as $key => $value) {
      $arr_data_ha["mr_read"] = is_null($data['mr']['mr_read']) ? 0: (array_key_exists($value, $data['mr']['mr_read']) ? $data['mr']['mr_read'][$value] : 0);
      $arr_data_ha["mr_create"] = is_null($data['mr']['mr_create']) ? 0: (array_key_exists($value, $data['mr']['mr_create']) ? $data['mr']['mr_create'][$value] : 0);
      $arr_data_ha["mr_update"] = is_null($data['mr']['mr_update']) ? 0 : (array_key_exists($value, $data['mr']['mr_update']) ? $data['mr']['mr_update'][$value] : 0);
      $arr_data_ha["mr_delete"] = is_null($data['mr']['mr_delete']) ? 0 : (array_key_exists($value, $data['mr']['mr_delete']) ? $data['mr']['mr_delete'][$value] : 0);
      $arr_data_ha["mr_role"] = is_null($data['mr']['mr_role']) ? "" : (array_key_exists($value, $data['mr']['mr_role']) ? $data['mr']['mr_role'][$value] : "");
      $arr_data_ha["mr_menu"] = $id_menu;
      $arr_data_ha['created_at'] = date('Y-m-dH:i:s');
      $arr_data_ha['created_by'] = $this->session->userdata('userId');

      // echo $this->db->set($arr_data_ha)->get_compiled_insert('menu_roles');
      $this->db->insert('menu_roles', $arr_data_ha);
    }

    $this->db->trans_complete();
    // exit;
    if ($this->db->trans_status() === FALSE) {
      return "0";
    }
    else {
      return "1";
    }
  }

  public function update(Array $param = NULL)
  {
    if (array_key_exists('where', $param)) {
      $this->db->trans_begin();

      if (is_array($param['where'])) {
        foreach ($param['where'] as $key => $value) {
          $this->db->where($key, $value);
        }
      }
      else {
        $this->db->where($param['where']);
      }
      
      $this->db->update($this->table, $param['data']);

      // delete dulu trus insert baru
      $this->db->where('mr_menu', $param['where']['id']);
      $this->db->delete('menu_roles');

      foreach ($param['mr_role'] as $key => $value) {
        $arr_data_ha["mr_read"] = is_null($param['mr_read']) ? 0: (array_key_exists($value, $param['mr_read']) ? $param['mr_read'][$value] : 0);
        $arr_data_ha["mr_create"] = is_null($param['mr_create']) ? 0: (array_key_exists($value, $param['mr_create']) ? $param['mr_create'][$value] : 0);
        $arr_data_ha["mr_update"] = is_null($param['mr_update']) ? 0 : (array_key_exists($value, $param['mr_update']) ? $param['mr_update'][$value] : 0);
        $arr_data_ha["mr_delete"] = is_null($param['mr_delete']) ? 0 : (array_key_exists($value, $param['mr_delete']) ? $param['mr_delete'][$value] : 0);
        $arr_data_ha["mr_role"] = is_null($param['mr_role']) ? "" : (array_key_exists($value, $param['mr_role']) ? $param['mr_role'][$value] : "");
        $arr_data_ha["mr_menu"] = $param['where']['id'];
        $arr_data_ha['created_at'] = date('Y-m-dH:i:s');
        $arr_data_ha['created_by'] = $this->session->userdata('userId');
        
      
       // $this->db->where('mr_menu', $param['where']['id']);
       // $this->db->where('mr_role', $value);
        
        
        //echo $this->db->set($arr_data_ha)->get_compiled_insert('menu_roles');
        $this->db->insert('menu_roles', $arr_data_ha);
      //  $this->db->update('menu_roles', $arr_data_ha);
      }
   
      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE) {
        return "0";
      }
      else {
        return "1";
      }
    }
  }

  public function delete(Array $param = NULL)
  {
    if ( ! is_null($param)) {
      if (array_key_exists('where', $param)) {
        if (is_array($param['where'])) {
          foreach ($param['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($param['where']);
        }
      }
    }

    $this->db->delete($this->table);
  }

  public function delete_by_id($id)
  {
    $this->db->trans_begin();

    $this->db->where('mr_menu', $id);
    $this->db->delete('menu_roles');

    $this->db->where('id', $id);
    $this->db->delete($this->table);

    $this->db->trans_complete();
  }

  //untuk cek duplicate data termasuk yang is_delete
	function check_all_data($table, $parameter){
		$sql = "SELECT COUNT(*) as jml FROM ".$table." WHERE ".$parameter;
        
		//echo $sql;exit;
		
        $qry = $this->db->query($sql);
        return $qry->row();
	}
}
