<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->id : '';
    $menu_title = isset($row) ? $row->menu_title : '';
    $menu_url = isset($row) ? $row->menu_url : '';
    $menu_order = isset($row) ? $row->menu_order : '';
    $menu_parent = isset($row) ? $row->menu_parent : '';
    $active = isset($row) ? $row->active : '';
    $btn_val = isset($row) ? 'update' : 'save';

    $arr_active = array(2 => 'Aktif Web', 1 => 'Aktif', 0 => 'Tidak Aktif');
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="menu_title">Title</label>
            <input type="text" name="menu_title" id="menu_title" value="<?php echo $menu_title; ?>" class="form-control" placeholder="Title" autofocus="autofocus" autocomplete="off" required="required">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="menu_url">URL</label>
            <input type="text" name="menu_url" id="menu_url" value="<?php echo $menu_url; ?>" class="form-control" placeholder="URL" autocomplete="off" required="required">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label for="menu_order">Order</label>
            <input type="text" name="menu_order" id="menu_order" value="<?php echo $menu_order; ?>" class="form-control" placeholder="Order" autocomplete="off" required="required">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label for="menu_parent">Parent</label>
            <select name="menu_parent" id="menu_parent" class="form-control">
              <option value="0">Pilih</option>
              <?php
              foreach ($parent as $valParent) {
                $sel_parent = $valParent->id == $menu_parent ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $valParent->id; ?>" <?php echo $sel_parent; ?>><?php echo $valParent->menu_title; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="active">Status</label>
            <select name="active" id="active" class="form-control" required="required">
              <option value="">Pilih</option>
              <?php
              foreach ($arr_active as $kActive => $vActive) {
                $sel_active = $kActive == $active ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $kActive; ?>" <?php echo $sel_active; ?>><?php echo $vActive; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>
      </div>

      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <td>Role</td>
              <td><input type="checkbox" id="chk-all-read"> Read</td>
              <td><input type="checkbox" id="chk-all-create"> Create</td>
              <td><input type="checkbox" id="chk-all-update"> Update</td>
              <td><input type="checkbox" id="chk-all-delete"> Delete</td>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($role as $valRole) {
              $row_mr = 0;
              if (isset($row))
              {
                $sql_mr = "SELECT * FROM menu_roles WHERE mr_menu = ".$id." AND mr_role = ".$valRole->id;

                $qry_mr = $this->db->query($sql_mr);
                $row_mr = $qry_mr->num_rows() > 0 ? $qry_mr->row() : 0;
              }

              $mr_read = $row_mr !== 0 ? $row_mr->mr_read : 0;
              $mr_create = $row_mr !== 0 ? $row_mr->mr_create : 0;
              $mr_update = $row_mr !== 0 ? $row_mr->mr_update : 0;
              $mr_delete = $row_mr !== 0 ? $row_mr->mr_delete : 0;

              $checked_read = $mr_read === "1" ? "checked=\"checked\"" : "";
              $checked_create = $mr_create === "1" ? "checked=\"checked\"" : "";
              $checked_update = $mr_update === "1" ? "checked=\"checked\"" : "";
              $checked_delete = $mr_delete === "1" ? "checked=\"checked\"" : "";
              ?>
              <tr>
                <td><?php echo ucwords($valRole->role_name); ?></td>
                <td><input type="checkbox" name="mr_read[<?php echo $valRole->id; ?>]" class="view" value="1" <?php echo $checked_read; ?>></td>
                <td><input type="checkbox" name="mr_create[<?php echo $valRole->id; ?>]" class="insert" value="1" <?php echo $checked_create; ?>></td>
                <td><input type="checkbox" name="mr_update[<?php echo $valRole->id; ?>]" class="update" value="1" <?php echo $checked_update; ?>></td>
                <td><input type="checkbox" name="mr_delete[<?php echo $valRole->id; ?>]" class="delete" value="1" <?php echo $checked_delete; ?>></td>
                <input type="hidden" name="mr_role[<?php echo $valRole->id; ?>]" value="<?php echo $valRole->id; ?>" <?php echo $checked_read; ?>>
              </tr>
            <?php
            }
            ?>
          </tbody>
        </table>
      </div>

      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>

<script>
  $(document).ready(function(){
    $("#menu_parent").select2();
    $("#active").select2();
  
    //check all
    $("#chk-all-read").click(function () {
      $(".view").prop('checked', $(this).prop('checked'));
    });
    $("#chk-all-create").click(function () {
      $(".insert").prop('checked', $(this).prop('checked'));
    });
    $("#chk-all-update").click(function () {
      $(".update").prop('checked', $(this).prop('checked'));
    });
    $("#chk-all-delete").click(function () {
      $(".delete").prop('checked', $(this).prop('checked'));
    });
  });
</script>