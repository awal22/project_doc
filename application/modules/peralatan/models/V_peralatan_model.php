<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */

class V_peralatan_model extends CI_Model
{
  private $table = 'v_peralatan';
  private $column_order = array(null, 'nama', 'fasilitas', 'merk', 'kondisi', 'nama_kobu', 'nama_kacab_pembina', 'lppnpi');
  private $column_search = array('nama', 'fasilitas', 'merk', 'kondisi', 'nama_kobu', 'nama_kacab_pembina', 'lppnpi');
  private $order = array('id' => 'desc');


  public function __construct()
  {
      parent::__construct();
      $this->load->database();
  }

  public function get_data(Array $param = NULL)
  {
    if ( ! is_null($param)) {
      if (array_key_exists('select', $param)) {
        $this->db->select($param['select']);
      }

      if (array_key_exists('order_by', $param)) {
        foreach ($param['order_by'] as $key => $value) {
          $this->db->order_by($key, $value);
        }
      }

      if (array_key_exists('distinct', $param)) {
        $this->db->distinct();
      }

      if (array_key_exists('where', $param)) {
        if (is_array($param['where'])) {
          foreach ($param['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($param['where']);
        }
      }

      if (array_key_exists('or_where', $param)) {
        if (is_array($param['or_where'])) {
          foreach ($param['or_where'] as $key => $value) {
            $this->db->or_where($key, $value);
          }
        }
        else {
          $this->db->or_where($param['or_where']);
        }
      }

      if (array_key_exists('like', $param)) {
        if (is_array($param['like'])) {
          foreach ($param['like'] as $key => $value) {
            $this->db->like($key, $value);
          }
        }
        else {
          $this->db->like($param['like']);
        }
      }

      if (array_key_exists('or_like', $param)) {
        if (is_array($param['or_like'])) {
          foreach ($param['or_like'] as $key => $value) {
            $this->db->or_like($key, $value);
          }
        }
        else {
          $this->db->or_like($param['or_like']);
        }
      }
    }

    $this->db->from($this->table);

    return $this->db->get();
  }

  private function _get_datatables_query()
  {
       
      //add custom filter here
      if($this->input->post('kobu'))
      {
          $this->db->where('kobu_ids', $this->input->post('kobu'));
      }
      if($this->input->post('lppnpi'))
      {
          $this->db->like('lppnpi_id', $this->input->post('lppnpi'));
      }
      if($this->input->post('fasilitas'))
      {
          $this->db->like('fasilitas', $this->input->post('fasilitas'));
      }
      if($this->input->post('kondisi'))
      {
          $this->db->like('kondisi', $this->input->post('kondisi'));
      }

      $this->db->from($this->table);
      $i = 0;
   
      foreach ($this->column_search as $item) // loop column 
      {
          if($_POST['search']['value']) // if datatable send POST for search
          {
               
              if($i===0) // first loop
              {
                  $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                  $this->db->like($item, $_POST['search']['value']);
              }
              else
              {
                  $this->db->or_like($item, $_POST['search']['value']);
              }

              if(count($this->column_search) - 1 == $i) //last loop
                  $this->db->group_end(); //close bracket
          }
          $i++;
      }
       
      if(isset($_POST['order'])) // here order processing
      {
          $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
      } 
      else if(isset($this->order))
      {
          $order = $this->order;
          $this->db->order_by(key($order), $order[key($order)]);
      }
  }

  function get_datatables($post)
  {
    $this->_get_datatables_query($post);
    if($post['length'] != -1)
    {
      $this->db->limit($post['length'], $post['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered($post)
  {
    $this->_get_datatables_query($post);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }


  public function GetPie(){
    $query=$this->db->query("select * from v_peralatan;");
    return $query;
}

}