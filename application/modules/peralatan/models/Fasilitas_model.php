<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */

class Fasilitas_model
{
  public function get_data()
  {
    $arr[] = 'Communication';
    $arr[] = 'Navigation';
    $arr[] = 'Surveillance';
    $arr[] = 'ATC Automation';

    return $arr;
  }
}