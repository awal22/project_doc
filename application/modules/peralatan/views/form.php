<style>
  #map-canvas{
    width: 100%;
    height: 250px;
  }
</style>

<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->id : '';
    $nama = isset($row) ? $row->nama : '';
    $fasilitas = isset($row) ? $row->fasilitas : '';
    if (isset($row)) {
      $lat =  ($row->lat != '') ? $row->lat : '-6.229728';
      $long = ($row->long != '' )? $row->long : '106.6894281';
    } else {
      $lat = '-6.229728';
      $long = '106.6894281';
    }
    $tahun_installasi = isset($row) ? $row->tahun_installasi : '';
    $frekuensi = isset($row) ? $row->frekuensi : '';
    $ident = isset($row) ? $row->ident : '';
    $merk_id = isset($row) ? $row->merk_id : '';
    $lokasi_alat_id = isset($row) ? $row->lokasi_alat_id : '';
    $fungsi_id = isset($row) ? $row->fungsi_id : '';
    $pelayanan_id = isset($row) ? $row->pelayanan_id : '';
    $lppnpi_id = isset($row) ? $row->lppnpi_id : '';
    $poweroutput = isset($row) ? $row->poweroutput : '';
    $catatan = isset($row) ? $row->catatan : '';
    $foto = isset($row) ? $row->foto : '';
    $kondisi = isset($row) ? $row->kondisi : '';
    $readonly_view = (isset($row)) ? "readonly" : "" ;
    $btn_val = isset($row) ? 'update' : 'save';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" name="nama" id="nama" value="<?php echo $nama; ?>" class="form-control" placeholder="Nama" autofocus="autofocus" autocomplete="off">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="fasilitas">Fasilitas</label>
            <select name="fasilitas" id="fasilitas" class="form-control">
              <option value="">Pilih</option>
              <?php
              foreach ($arr_fasilitas as $kFasilitas => $vFasilitas) {
                $selFasilitas = $vFasilitas == $fasilitas ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vFasilitas; ?>" <?php echo $selFasilitas; ?>><?php echo $vFasilitas; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="tahun_installasi">Tahun Installasi</label>
            <input type="text" name="tahun_installasi" id="tahun_installasi" value="<?php echo $tahun_installasi; ?>" class="form-control" placeholder="Tahun Installasi" autocomplete="off">
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="frekuensi">Frekuensi</label>
            <input type="text" name="frekuensi" id="frekuensi" value="<?php echo $frekuensi; ?>" class="form-control" placeholder="Frekuensi" autocomplete="off">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="ident">Ident</label>
            <input type="text" name="ident" id="ident" value="<?php echo $ident; ?>" class="form-control" placeholder="Ident" autocomplete="off">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="merk_id">Merk</label>
            <select name="merk_id" id="merk_id" class="form-control">
              <option value="">Pilih</option>
              <?php
              foreach ($merk as $vMerk) {
                $selMerk = $vMerk->id == $merk_id ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vMerk->id; ?>" <?php echo $selMerk; ?>><?php echo $vMerk->nama; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="lokasi_alat_id">Lokasi</label>
            <select name="lokasi_alat_id" id="lokasi_alat_id" class="form-control">
              <option value="">Pilih</option>
              <?php
              foreach ($lokasi_alat as $vLokasiAlat) {
                $selLokasi = $vLokasiAlat->id == $lokasi_alat_id ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vLokasiAlat->id; ?>" <?php echo $selLokasi; ?>><?php echo $vLokasiAlat->nama; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="fungsi_id">Fungsi</label>
            <select name="fungsi_id" id="fungsi_id" class="form-control">
              <option value="">Pilih</option>
              <?php
              foreach ($fungsi as $vFungsi) {
                $selLokasi = $vFungsi->id == $fungsi_id ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vFungsi->id; ?>" <?php echo $selLokasi; ?>><?php echo $vFungsi->nama; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="pelayanan_id">Pelayanan</label>
            <select name="pelayanan_id" id="pelayanan_id" class="form-control">
              <option value="">Pilih</option>
              <?php
              foreach ($pelayanan as $vPelayanan) {
                $selPelayanan = $vPelayanan->id == $pelayanan_id ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vPelayanan->id; ?>" <?php echo $selPelayanan; ?>><?php echo $vPelayanan->nama; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="lppnpi_id">LPPNPI</label>
            <select name="lppnpi_id" id="lppnpi_id" class="form-control">
              <option value="">Pilih</option>
              <?php
              foreach ($lppnpi as $vLppnpi) {
                $selLppnpi = $vLppnpi->id == $lppnpi_id ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vLppnpi->id; ?>" <?php echo $selLppnpi ?>><?php echo $vLppnpi->nama; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="poweroutpu">Power Output</label>
            <input type="text" name="poweroutput" id="poweroutput" value="<?php echo $poweroutput; ?>" class="form-control" placeholder="Power Output" autocomplete="off">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="kondisi">Kondisi</label>
            <select name="kondisi" id="kondisi" class="form-control" required="required">
              <option value="">Pilih Kondisi</option>
              <?php
              foreach ($arr_kondisi as $vKondisi) {
                $selKondisi = $vKondisi == $kondisi ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vKondisi; ?>" <?php echo $selKondisi; ?>><?php echo $vKondisi; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label for="catatan">Catatan</label>
            <textarea name="catatan" id="catatan" class="form-control" placeholder="Catatan" cols="30" rows="5"><?php echo $catatan; ?></textarea>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for="foto">Upload</label>
            <input type="file" name="foto" id="foto">
            <span id="helpFoto" class="help-block"><span class="text-danger">Max. Size: 2MB | Dimension: 1024 x 768</span></span>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for=""><?php echo $foto; ?></label>
            <?php
            if ($foto != "") {
              ?>
              <img src="<?php echo base_url("foto/".$foto); ?>" class="img-responsive img-thumbnail" alt="Responsive image" style="height: 200px;">
              <?php
            }
            ?>
          </div>
        </div>

        <div class="col-md-12">
        <div class="form-group">
        <label for="level_desc" class="control-label col-sm-2">Search Store Location</label>
        <!-- <div class="col-sm-10"> -->
            <div class="row">
                <div class="col-sm-8">
                <input type="text" id="searchmap" placeholder="Search" class="form-control" autofocus="autofocus" >
				<div id="map-canvas"></div>
                </div>
            </div>
        <!-- </div> -->
    </div>
    </div>

        <div class="col-md-12">
          <div class="form-group">
            <label for="lat">Latitude</label>
            <input type="text" name="lat" id="lat" value="<?php echo $lat; ?>" placeholder="Latitude" class="form-control" readonly>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="long">Longitude</label>
            <input type="text" name="long" id="lng" value="<?php echo $long; ?>" placeholder="Longitude" class="form-control"readonly>
          </div>
        </div>
      </div>

      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>

<!-- The Modal -->
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>

<script>
  $(document).ready(function(){
    $("#fasilitas").select2();
    $("#merk_id").select2();
    $("#lokasi_alat_id").select2();
    $("#fungsi_id").select2();
    $("#pelayanan_id").select2();
    $("#lppnpi_id").select2();
  });
</script>

<script>
  // Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: '<?php echo $lat; ?>', lng: '<?php echo $long; ?>'};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map-canvas'), {zoom: 10, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
// var map2;
// function initMap() {
// 	map2 = new google.maps.Map(document.getElementById('map-show'), {
// 		center: {lat: {{$promotor->promotor_latitude}}, lng: {{$promotor->promotor_longitude}}},
// 		zoom: 8
// 	});
// }
</script>

 <!-- <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA19W9CdOb9LYc-x0e3eUU_8MgvDeith3M&callback=initMap">
    </script>  -->
<?php //}else{ ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA19W9CdOb9LYc-x0e3eUU_8MgvDeith3M&libraries=places"
  type="text/javascript"></script>
  <script>
  var map2;
function initMap() {
	map2 = new google.maps.Map(document.getElementById('map-show'), {
		center: {lat: -34.397, lng: 150.644},
		zoom: 8
	});
}

	var map = new google.maps.Map(document.getElementById('map-canvas'),{
		center:{
			    lat: <?php echo $lat; ?>,
        	lng: <?php echo $long; ?>
		},
		zoom:15
	});

	var marker = new google.maps.Marker({
		position: {
			    lat: <?php echo $lat; ?>,
        	lng: <?php echo $long; ?>
		},
		map: map,
		draggable: true
	});

	var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

	google.maps.event.addListener(searchBox,'places_changed',function(){
		var places = searchBox.getPlaces();
		var bounds = new google.maps.LatLngBounds();
	
		var i, place;
		for(i=0; place=places[i];i++){
  			bounds.extend(place.geometry.location);
			  marker.setPosition(place.geometry.location); //set marker position new...
			  // $('#street').val( place.address_components[i].long_name);
			  // $('#city').val( place.address_components[i+1].long_name);
			  // $('#country').val( place.address_components[i+3].long_name);
			 // $('#zipcode').val( place.address_components[3].long_name);
			
		  }
	
  		map.fitBounds(bounds);
  		map.setZoom(15);
	});
	google.maps.event.addListener(marker,'position_changed',function(){
		var lat = marker.getPosition().lat();
		var lng = marker.getPosition().lng();
		var street = marker.formatted_address;
		$('#lat').val(lat);
		$('#lng').val(lng);
		

		
	});

  </script>