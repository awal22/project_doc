<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Peralatan extends MX_Controller
{
  private $modul = 'Peralatan';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));

    $this->load->model('peralatan/Peralatan_model', 'peralatan');
    $this->load->model('peralatan/V_peralatan_model', 'vPeralatan');
    $this->load->model('peralatan/Fasilitas_model', 'fasilitas');
    $this->load->model('merk/Merk_model', 'merk');
    $this->load->model('lokasi_alat/Lokasi_alat_model', 'lokasiAlat');
    $this->load->model('fungsi/Fungsi_model', 'fungsi');
    $this->load->model('pelayanan/Pelayanan_model', 'pelayanan');
    $this->load->model('lppnpi/Lppnpi_model', 'lppnpi');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'index', $data);
  }

  public function add()
  {
    $this->acl->check_create(strtolower($this->modul));

    $arr_fasilitas = $this->fasilitas->get_data();
    $data['arr_fasilitas'] = $arr_fasilitas;

    $merk = $this->merk->get_data(NULL);
    $data['merk'] = $merk->result();

    $lokasi_alat = $this->lokasiAlat->get_data(NULL);
    $data['lokasi_alat'] = $lokasi_alat->result();

    $fungsi = $this->fungsi->get_data(NULL);
    $data['fungsi'] = $fungsi->result();

    $pelayanan = $this->pelayanan->get_data(NULL);
    $data['pelayanan'] = $pelayanan->result();

    $lppnpi = $this->lppnpi->get_data(NULL);
    $data['lppnpi'] = $lppnpi->result();

    $arr_kondisi = $this->peralatan->kondisi();
    $data['arr_kondisi'] = $arr_kondisi;

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'Add';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'form', $data);
  }

  public function edit(int $id = NULL)
  {
    $this->acl->check_update(strtolower($this->modul));

    if ( ! is_null($id)) {
      $param['where']['id'] = $id;
      $row = $this->peralatan->get_data($param);

      if ($row->num_rows() > 0) {
        $data['row'] = $row->row();
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak ditemukan";
        $this->session->set_flashdata($flashdata);
  
        redirect(strtolower($this->modul));
      }

      $arr_fasilitas = $this->fasilitas->get_data();
      $data['arr_fasilitas'] = $arr_fasilitas;
    
      $merk = $this->merk->get_data(NULL);
      $data['merk'] = $merk->result();
  
      $lokasi_alat = $this->lokasiAlat->get_data(NULL);
      $data['lokasi_alat'] = $lokasi_alat->result();
  
      $fungsi = $this->fungsi->get_data(NULL);
      $data['fungsi'] = $fungsi->result();
  
      $pelayanan = $this->pelayanan->get_data(NULL);
      $data['pelayanan'] = $pelayanan->result();
  
      $lppnpi = $this->lppnpi->get_data(NULL);
      $data['lppnpi'] = $lppnpi->result();
  
      $arr_kondisi = $this->peralatan->kondisi();
      $data['arr_kondisi'] = $arr_kondisi;
  
      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Edit';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'form', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
			$this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function simpan()
  {
    //var_dump($_POST);die();
    $id = $this->input->post('id', TRUE);
    $nama = $this->input->post('nama', TRUE);
    $fasilitas = $this->input->post('fasilitas', TRUE);
    $lat = $this->input->post('lat', TRUE);
    $long = $this->input->post('long', TRUE);
    $tahun_installasi = $this->input->post('tahun_installasi', TRUE);
    $frekuensi = $this->input->post('frekuensi', TRUE);
    $ident = $this->input->post('ident', TRUE);
    $merk_id = $this->input->post('merk_id', TRUE);
    $lokasi_alat_id = $this->input->post('lokasi_alat_id', TRUE);
    $fungsi_id = $this->input->post('fungsi_id', TRUE);
    $pelayanan_id = $this->input->post('pelayanan_id', TRUE);
    $lppnpi_id = $this->input->post('lppnpi_id', TRUE);
    $poweroutput = $this->input->post('poweroutput', TRUE);
    $catatan = $this->input->post('catatan', TRUE);
    $foto = isset($_FILES['foto']) ? $_FILES['foto']['name'] : '';
    $kondisi = $this->input->post('kondisi', TRUE);
    $save = $this->input->post('save', TRUE);

    $image_name = url_title($nama, 'dash', TRUE);
    $file_ext = pathinfo($foto, PATHINFO_EXTENSION);

    switch ($save) {
      case 'save':
        $data['nama'] = $nama;
        $data['fasilitas'] = $fasilitas;
        $data['lat'] = $lat;
        $data['long'] = $long;
        $data['tahun_installasi'] = $tahun_installasi;
        $data['frekuensi'] = $frekuensi;
        $data['ident'] = $ident;
        $data['merk_id'] = $merk_id;
        $data['lokasi_alat_id'] = $lokasi_alat_id;
        $data['fungsi_id'] = $fungsi_id;
        $data['pelayanan_id'] = $pelayanan_id;
        $data['lppnpi_id'] = $lppnpi_id;
        $data['poweroutput'] = $poweroutput;
        $data['catatan'] = $catatan;
        $file_ext == "" ? "" : $data['foto'] = $image_name.".".$file_ext;;
        $data['kondisi'] = $kondisi;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->session->userdata('userId');
        $insert = $this->peralatan->insert($data);

        if ($insert == 1) {
          if ($file_ext == "") {
            $flashdata["alert_class"] = "success";
            $flashdata["alert_text"] = "Data berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
          else {
            // upload foto
            $config['file_name'] = $image_name;
            $config['upload_path'] = './foto/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 2048;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;
  
            $this->load->library('upload', $config);
  
            if ( ! $this->upload->do_upload('foto'))
            {
              $error = array('error' => $this->upload->display_errors());
              var_dump($error);
            }
            else
            {
              $flashdata["alert_class"] = "success";
              $flashdata["alert_text"] = "Data berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
          }  
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;

      case 'update':
        $param['data']['nama'] = $nama;
        $param['data']['fasilitas'] = $fasilitas;
        $param['data']['lat'] = $lat;
        $param['data']['long'] = $long;
        $param['data']['tahun_installasi'] = $tahun_installasi;
        $param['data']['frekuensi'] = $frekuensi;
        $param['data']['ident'] = $ident;
        $param['data']['merk_id'] = $merk_id;
        $param['data']['lokasi_alat_id'] = $lokasi_alat_id;
        $param['data']['fungsi_id'] = $fungsi_id;
        $param['data']['pelayanan_id'] = $pelayanan_id;
        $param['data']['lppnpi_id'] = $lppnpi_id;
        $param['data']['poweroutput'] = $poweroutput;
        $param['data']['catatan'] = $catatan;
        $file_ext == "" ? "" : $param['data']['foto'] = $image_name.".".$file_ext;
        $param['data']['kondisi'] = $kondisi;
        $param['data']['updated_at'] = date('Y-m-d H:i:s');
        $param['data']['updated_by'] = $this->session->userdata('userId');
        $param['where']['id'] = $id;
        
        if ($file_ext == "") {
          $update = $this->peralatan->update($param);
          if ($update == 1) {
            $flashdata["alert_class"] = "success";
            $flashdata["alert_text"] = "Data berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
          else {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
        }
        else {
          $param['where']['id'] = $id;
          $row = $this->peralatan->get_data($param)->row();
          if ($row->foto != '') {
            unlink('foto/'.$row->foto);
          }
          
          // upload bukti
          $config['file_name'] = $image_name;
          $config['upload_path'] = './foto/';
          $config['allowed_types'] = 'gif|jpg|png';
          $config['max_size'] = 2048;
          $config['max_width'] = 1024;
          $config['max_height'] = 768;

          $this->load->library('upload', $config);

          if ( ! $this->upload->do_upload('foto'))
          {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
          }
          else
          {
            $update = $this->peralatan->update($param);
            if ($update == 1) {
              $flashdata["alert_class"] = "success";
              $flashdata["alert_text"] = "Data berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
            else {
              $flashdata["alert_class"] = "warning";
              $flashdata["alert_text"] = "Data tidak berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
          }
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

  public function hapus(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));

    $param['where']['id'] = $id;

    $row = $this->peralatan->get_data($param)->row();
    unlink('foto/'.$row->foto);

    $this->peralatan->delete($param);

    $flashdata["alert_class"] = "success";
    $flashdata["alert_text"] = "Data berhasil dihapus";
    $this->session->set_flashdata($flashdata);

    redirect(strtolower($this->modul));
  }

  public function datagrid()
  {
    $list = $this->vPeralatan->get_datatables($this->input->post(NULL, TRUE));
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rw->nama;
      $row[] = $rw->fasilitas;
      $row[] = $rw->merk;
      $row[] = $rw->kondisi;
      $row[] = $rw->nama_kobu;
      $row[] = $rw->nama_kacab_pembina;
      $row[] = $rw->lppnpi;
      $row[] = $rw->created_at;
      $row[] = "<div class=\"text-center\">
              <a href=\"".site_url(strtolower($this->modul)."/edit/".$rw->id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>
              <button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->id."','".$rw->nama."');\">Delete</button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vPeralatan->count_all(),
      "recordsFiltered" => $this->vPeralatan->count_filtered($this->input->post(NULL, TRUE)),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }

  public function get_data_by_lokasi_alat()
  {
    $lokasi_alat_id = $this->input->post('lokasi_alat_id', TRUE);

    $param['where']['lokasi_alat_id'] = $lokasi_alat_id;
    $result = $this->peralatan->get_data($param);

    if ($result->num_rows() > 0)
    {
      echo json_encode($result->result());
    }
    else
    {
      echo json_encode($lokasi_alat_id);
    }
  }
}