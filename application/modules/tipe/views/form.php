<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->id : '';
    $nama = isset($row) ? $row->nama : '';
    $idmerk = isset($row) ? $row->idmerk : '';
    $btn_val = isset($row) ? 'update' : 'save';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" name="nama" id="nama" value="<?php echo $nama; ?>" class="form-control" placeholder="Nama" autofocus="autofocus" autocomplete="off">
      </div>
      <div class="form-group">
        <label for="nama">Merek</label>
        <select name="idmerk" id="idmerk" class="form-control">
          <option value="">Pilih</option>
          <?php
          foreach ($merk as $valMerk) {
            $sel_merk = $idmerk == $valMerk->id ? 'selected="selected"' : '';
            ?>
            <option value="<?php echo $valMerk->id; ?>" <?php echo $sel_merk; ?>><?php echo $valMerk->nama; ?></option>
            <?php
          }
          ?>
        </select>
      </div>
      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>

<script>
$(document).ready(function(){
  $("#idmerk").select2();
});
</script>