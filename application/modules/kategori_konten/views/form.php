<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->id_kategori : '';
    $nama = isset($row) ? $row->nama_kategori : '';
    $deskripsi = isset($row) ? $row->deskripsi : '';
    $gambar = isset($row) ? $row->gambar_kategori : '';
    $btn_val = isset($row) ? 'update' : 'save';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label for="nama">Nama Kategori Konten</label>
        <input type="text" name="nama" id="nama" value="<?php echo $nama; ?>" class="form-control" placeholder="Nama" autofocus="autofocus" autocomplete="off">
      </div>
      <div class="form-group">
        <label for="berita">Deskripsi</label>
        <textarea name="deskripsi" id="deskripsi" class="ckeditor" placeholder="Deskripsi" cols="30" rows="3"><?php echo $deskripsi; ?></textarea>
      </div>
      <div class="form-group">
        <label for="favicon">Gambar</label>
        <input type="file" name="gambar" id="gambar" class="form-control" value="<?php echo $gambar; ?>">
        <?php if($gambar !="")
        {
          echo "<i style='color:red;'>Lihat gambar Saat ini : </i> <a target='_BLANK' href='".base_url()."foto/$gambar'>$gambar</a>"; 
        }
        ?> 
      </div>
      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>

<script>
  $(document).ready(function(){
    $("#active").select2();
  });
</script>