<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->id_berita : '';
    $judul = isset($row) ? $row->judul : '';
    $id_kategori_berita = isset($row) ? $row->id_kategori : '';
    $headline = isset($row) ? $row->headline : '';
    $slider = isset($row) ? $row->slider : '';
    $statis = isset($row) ? $row->statis : '';
    $berita = isset($row) ? $row->isi_berita : '';
    $gambar = isset($row) ? $row->gambar : '';
    $tag = isset($row) ? $row->tag : 'tidak ada tag';


    $btn_val = isset($row) ? 'update' : 'save';
    $arr_headline = array(1 => 'Aktif', 0 => 'Tidak Aktif');
    $arr_slider = array(1 => 'Aktif', 0 => 'Tidak Aktif');
    $arr_statis = array(1 => 'Aktif', 0 => 'Tidak Aktif');
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label for="judul">Judul</label>
        <input type="text" name="judul" id="judul" value="<?php echo $judul; ?>" class="form-control" placeholder="Judul Berita" autofocus="autofocus" autocomplete="off">
      </div>
      <div class="form-group">
        <label for="kategori_berita">Kategori Berita</label>
        <select name="kategori" id="kategori" class="form-control">
          <option value="0">Pilih Kategori</option>
          <?php
          foreach ($kategori as $valKategori) {
            $sel_kategori = $valKategori->id_kategori == $id_kategori_berita ? 'selected="selected"' : '';
            ?>
            <option value="<?php echo $valKategori->id_kategori; ?>" <?php echo $sel_kategori; ?>><?php echo $valKategori->nama_kategori; ?></option>
            <?php
          }
          ?>
        </select>
      </div>
      <div class="form-group">
        <label for="headline">Headline</label>
        <select name="headline" id="headline" class="form-control" required="required">
          <?php
            foreach ($arr_headline as $kHeadline => $vHeadline) {
            $sel_headline = $kHeadline == $headline ? 'selected="selected"' : '';
            ?>
            <option value="<?php echo $kHeadline; ?>" <?php echo $sel_headline; ?>><?php echo $vHeadline; ?></option>
            <?php
          }
          ?>
        </select>
      </div>
      <div class="form-group">
        <label for="headline">Slider</label>
        <select name="slider" id="slider" class="form-control" required="required">
          <?php
            foreach ($arr_slider as $kSlider => $vSlider) {
            $sel_slider = $kSlider == $slider ? 'selected="selected"' : '';
            ?>
            <option value="<?php echo $kSlider; ?>" <?php echo $sel_slider; ?>><?php echo $vSlider; ?></option>
            <?php
          }
          ?>
        </select>
      </div>
      <div class="form-group" hidden>
        <label for="statis">Statis</label>
        <select name="statis" id="statis" class="form-control" required="required">
          <?php
            foreach ($arr_statis as $kStatis => $vStatis) {
            $sel_statis = $kStatis == $statis ? 'selected="selected"' : '';
            ?>
            <option value="<?php echo $kStatis; ?>" <?php echo $sel_statis; ?>><?php echo $vStatis; ?></option>
            <?php
          }
          ?>
        </select>
      </div>
      <div class="form-group">
        <label for="berita">Berita</label>
        <textarea name="berita" id="berita" class="ckeditor" placeholder="Isi Berita" cols="30" rows="3"><?php echo $berita; ?></textarea>
      </div>
      <div class="form-group">
        <label for="tag">Tag/Label</label>
        <div class="checkbox-scroll">
          <?php 
          $_arrNilai = explode(',', $tag);
          foreach ($tag_konten as $key) {
            $_ck = (array_search($key->tag_seo, $_arrNilai) === false) ? '' : "checked";
          ?>
          <span style="display:block;"><input type=checkbox value="<?php echo $key->tag_seo; ?>" name=tag[] <?php echo $_ck; ?> ><?php echo $key->nama_tag; ?> &nbsp; &nbsp; &nbsp; </span>
          <?php
          }
          ?>
        </div>
      </div>



      <div class="form-group">
        <label for="favicon">Gambar</label>
        <input type="file" name="gambar" id="gambar" class="form-control" value="<?php echo $gambar; ?>">
        <?php if($gambar !="")
        {
          echo "<i style='color:red;'>Lihat gambar Saat ini : </i> <a target='_BLANK' href='".base_url()."foto/$gambar'>$gambar</a>"; 
        }
        ?> 
      </div>

      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>

<script>
  $(document).ready(function(){
    $("#active").select2();
  });
</script>

 <script>
      $(function () {
        $("#berita").wysihtml5();
      });

      CKEDITOR.replace('editor1' ,{
        filebrowserImageBrowseUrl : '<?php echo base_url('asset/kcfinder'); ?>'
      });
  </script>