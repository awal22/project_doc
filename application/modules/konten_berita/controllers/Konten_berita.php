<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Awaluddin <awaluddinbinusep@gmail.com>
 */
class Konten_berita extends MX_Controller
{
  private $modul = 'Konten_berita';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));
    $this->load->library('upload');
    $this->load->model('kategori_konten/Kategori_konten_model', 'kategori');
    $this->load->model('konten_berita/V_berita_model', 'vBerita');
    $this->load->model('tag_konten/Tag_model', 'tag_konten');
    $this->load->model('konten_berita/Konten_model', 'berita');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'index', $data);
  }

  public function add()
  {
    $this->acl->check_create(strtolower($this->modul));

    $kategori = $this->kategori->get_data(NULL);
    $data['kategori'] = $kategori->result();

    $tag = $this->tag_konten->get_data(NULL);
    $data['tag_konten'] = $tag->result();

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'Add';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'form', $data);
  }

  public function edit(int $id = NULL)
  {
    $this->acl->check_update(strtolower($this->modul));

    if ( ! is_null($id)) {
      $param['where']['id_berita'] = $id;
      $row = $this->berita->get_data($param);

      if ($row->num_rows() > 0) {
        $data['row'] = $row->row();
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak ditemukan";
        $this->session->set_flashdata($flashdata);
  
        redirect(strtolower($this->modul));
      }

      $kategori = $this->kategori->get_data(NULL);
      $data['kategori'] = $kategori->result();

      $tag = $this->tag_konten->get_data(NULL);
      $data['tag_konten'] = $tag->result();

      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Edit';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'form', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
			$this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function simpan()
  {

    $id       = $this->input->post('id', TRUE);
    $judul    = $this->input->post('judul',  TRUE);
    $kategori = $this->input->post('kategori', TRUE);
    $headline = $this->input->post('headline', TRUE);
    $slider   = $this->input->post('slider', TRUE);
    $statis   = $this->input->post('statis', TRUE);
    $berita   = $this->input->post('berita');
    $seo      = seo_title($this->input->post('judul', TRUE));
    $save     = $this->input->post('save', TRUE);

    switch ($save) {
      case 'save':

        if (is_array($this->input->post('tag')!==''))
        {
          $tag_seo = $this->input->post('tag');
          $tag = implode(',',$tag_seo);
        }
        else
        {
          $tag = "";
        }

        if($_FILES['gambar']['name'])
        {
          $config['upload_path'] = './foto/';
          $config['allowed_types'] = 'ico|gif|jpg|jpeg|png';
          $config['max_size'] = 200048;

          $this->upload->initialize($config);
          if($this->upload->do_upload('gambar'))
          {
            
            $gbr = $this->upload->data();
            
            
            $foto = $gbr['file_name'];
            }
            else
            {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
          }
        }
        else
        {
          $foto = "";
        }

        $data['id_kategori'] = $kategori;
        $data['judul'] = $judul;
        $data['judul_seo'] = $seo;
        $data['headline'] = $headline;
        $data['slider'] = $slider;
        $data['slider'] = $statis;
        $data['isi_berita'] = $berita;
        $data['hari'] = hari_ini(date('w'));
        $data['tanggal'] = date('Y-m-d');
        $data['jam'] = date('H:i:s');
        $data['gambar'] = $foto;
        $data['dibaca'] = 0;
        $data['tag'] = $tag;

        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->session->userdata('userId');

          $insert = $this->berita->insert($data);

          if ($insert == 1) {
            $flashdata["alert_class"] = "success";
            $flashdata["alert_text"] = "Data berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
          else {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
     
        break;

      case 'update':

        if ($this->input->post('tag')!=='')
        {
          $tag_seo = $this->input->post('tag');
          $tag=implode(',',$tag_seo);
        }
        else
        {
          $tag = "";
        }

        $config['upload_path'] = './foto/';
        $config['allowed_types'] = 'ico|gif|jpg|jpeg|png';
        $config['max_size'] = 200048;

        $this->upload->initialize($config);
        if($this->upload->do_upload('gambar'))
        {
          $gbr = $this->upload->data();

          $foto_update = $gbr['file_name'];
          $param['data']['id_kategori'] = $kategori;
          $param['data']['judul'] = $judul;
          $param['data']['judul_seo'] = $seo;
          $param['data']['headline'] = $headline;
          $param['data']['slider'] = $slider;
          $param['data']['statis'] = $statis;
          $param['data']['isi_berita'] = $berita;
          $param['data']['hari'] = hari_ini(date('w'));
          $param['data']['tanggal'] = date('Y-m-d');
          $param['data']['jam'] = date('H:i:s');
          $param['data']['gambar'] = $foto_update;
          $param['data']['tag'] = $tag;

          $param['data']['updated_at'] = date('Y-m-d H:i:s');
          $param['data']['updated_by'] = $this->session->userdata('userId');
          $param['where']['id_berita'] = $id;
        }
        else
        {
          $param['data']['id_kategori'] = $kategori;
          $param['data']['judul'] = $judul;
          $param['data']['judul_seo'] = $seo;
          $param['data']['headline'] = $headline;
          $param['data']['slider'] = $slider;
          $param['data']['statis'] = $statis;
          $param['data']['isi_berita'] = $berita;
          $param['data']['hari'] = hari_ini(date('w'));
          $param['data']['tanggal'] = date('Y-m-d');
          $param['data']['jam'] = date('H:i:s');
          $param['data']['tag'] = $tag;

          $param['data']['updated_at'] = date('Y-m-d H:i:s');
          $param['data']['updated_by'] = $this->session->userdata('userId');
          $param['where']['id_berita'] = $id;
        }
        
          $update = $this->berita->update($param);

          if ($update == 1) {
            $flashdata["alert_class"] = "success";
            $flashdata["alert_text"] = "Data berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
          else {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }

        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

  public function hapus(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));

    $param['where']['id_berita'] = $id;
    $this->berita->delete($param);

    $flashdata["alert_class"] = "success";
    $flashdata["alert_text"] = "Data berhasil dihapus";
    $this->session->set_flashdata($flashdata);

    redirect(strtolower($this->modul));
  }

  public function datagrid()
  {
    $list = $this->vBerita->get_datatables($this->input->post(NULL, TRUE));
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rw->judul;
      $row[] = 'berita/detail/'.$rw->judul_seo;
      $row[] = $rw->nama_kategori;

      $row[] = $rw->created_at;
      $row[] = "<div class=\"text-center\">
              <a href=\"".site_url(strtolower($this->modul)."/edit/".$rw->id_berita)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>
              <button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->id_berita."','".$rw->judul."');\">Delete</button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vBerita->count_all(),
      "recordsFiltered" => $this->vBerita->count_filtered($this->input->post(NULL, TRUE)),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }
}