<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->footer_id : '';
    $nama = isset($row) ? $row->footer_nama : '';
    $category = isset($row) ? $row->footer_kategori : '';
    $arr_category = array(2 => 'Maskapai', 1 => 'Bandara');

    $btn_val = isset($row) ? 'update' : 'save';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label for="category">Kategori</label>
        <select name="category" id="category" class="form-control" required="required">
          <option value="0">Pilih</option>
          <?php
          foreach ($arr_category as $kCategory => $vCategory) {
            $sel_category = $kCategory == $category ? 'selected="selected"' : '';
            ?>
            <option value="<?php echo $kCategory; ?>" <?php echo $sel_category; ?>><?php echo $vCategory; ?></option>
            <?php
          }
          ?>
        </select>
      </div>

      <div class="form-group">
        <label for="nama">Nama Bandara/Maskapai</label>
        <input type="text" name="nama" id="nama" value="<?php echo $nama; ?>" class="form-control" placeholder="Nama" autofocus="autofocus" autocomplete="off">
      </div>
      
      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>

<script>
  $(document).ready(function(){
    $("#category").select2();
  });
</script>