<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Footer_website extends MX_Controller
{
  private $modul = 'Footer_website';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));
    $this->load->model('Footer_website/Footer_model', 'footer');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'index', $data);
  }

  public function add()
  {
    $this->acl->check_create(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'Add';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'form', $data);
  }

  public function edit(int $id = NULL)
  {
    $this->acl->check_update(strtolower($this->modul));

    if ( ! is_null($id)) {
      $param['where']['footer_id'] = $id;
      $row = $this->footer->get_data($param);

      if ($row->num_rows() > 0) {
        $data['row'] = $row->row();
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak ditemukan";
        $this->session->set_flashdata($flashdata);
  
        redirect(strtolower($this->modul));
      }

      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Edit';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'form', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
	  $flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
	  $this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function simpan()
  {
    $id    = $this->input->post('id', TRUE);
    $nama  = $this->input->post('nama');
    $kategori  = $this->input->post('category');
    $save  = $this->input->post('save', TRUE);

    switch ($save) {
      case 'save':
        $data['footer_kategori'] = $kategori;
        $data['footer_nama'] = $nama;

        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->session->userdata('userId');

        $param['where']['footer_nama'] = $nama;
      	$row = $this->footer->get_data($param);

      	if ($row->num_rows() > 0) {
    	  $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data sudah ada didatabase";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
      	}else{

	      	$insert = $this->footer->insert($data);

	        if ($insert == 1) {
	          $flashdata["alert_class"] = "success";
	          $flashdata["alert_text"] = "Data berhasil disimpan";
	          $this->session->set_flashdata($flashdata);
	    
	          redirect(strtolower($this->modul));
	        }
	        else {
	          $flashdata["alert_class"] = "warning";
	          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
	          $this->session->set_flashdata($flashdata);
	    
	          redirect(strtolower($this->modul));
	        }
    	}
        break;

      case 'update':

          $param['data']['footer_kategori'] = $kategori;
          $param['data']['footer_nama'] = $nama;

          $param['data']['updated_at'] = date('Y-m-d H:i:s');
          $param['data']['updated_by'] = $this->session->userdata('userId');
          $param['where']['footer_id'] = $id;

          $update = $this->footer->update($param);

          if ($update == 1) {
            $flashdata["alert_class"] = "success";
            $flashdata["alert_text"] = "Data berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
          else {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
          break;
        
        default:
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
          $this->session->set_flashdata($flashdata);

          redirect(strtolower($this->modul));
          break;
    }
  }

  public function hapus(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));

    $param['where']['footer_id'] = $id;
    $this->footer->delete($param);

    $flashdata["alert_class"] = "success";
    $flashdata["alert_text"] = "Data berhasil dihapus";
    $this->session->set_flashdata($flashdata);

    redirect(strtolower($this->modul));
  }

  public function datagrid()
  {
    $list = $this->footer->get_datatables($this->input->post(NULL, TRUE));
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      //bandara = 1 & maskapai = 2
      if($rw->footer_kategori==1){ $kategori = "Bandara"; }
      if($rw->footer_kategori==2){ $kategori = "Maskapai"; }
      if($rw->footer_kategori==0){ $kategori = "Belum pilih"; }

      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $kategori;
      $row[] = $rw->footer_nama;

      $row[] = $rw->created_at;
      $row[] = "<div class=\"text-center\">
              <a href=\"".site_url(strtolower($this->modul)."/edit/".$rw->footer_id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>
              <button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->footer_id."','".$rw->footer_nama."');\">Delete</button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->footer->count_all(),
      "recordsFiltered" => $this->footer->count_filtered($this->input->post(NULL, TRUE)),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }
}