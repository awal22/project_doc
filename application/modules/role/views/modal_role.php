<form>
  <input type="hidden" name="ur_user">
  <table class="table table-bordered table-hover table-striped">
    <thead>
      <tr>
        <th>Role</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach ($role->result() as $vRole) {
        ?>
        <tr>
          <td><?php echo $vRole->role_name; ?></td>
          <td><input type="checkbox" name="ur_role" id="ur_role"></td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
</form>