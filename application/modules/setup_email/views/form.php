<?php
  if ( ! is_null($this->session->flashdata('alert_class')))
  {
    ?>
    <div id="alert" class="alert alert-<?php echo $this->session->flashdata('alert_class'); ?>" role="alert"><?php echo $this->session->flashdata('alert_text'); ?></div>
    <?php
  }
?>
<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->setup_email_id : '';
    $nama = isset($row) ? $row->setup_email_nama : '';
    $username = isset($row) ? $row->setup_email_username : '';
    $password = isset($row) ? $row->setup_email_password : '';
    $port = isset($row) ? $row->setup_email_port : '';
    $tipe = isset($row) ? $row->setup_email_tipe : '';

    $btn_val = isset($row) ? 'update' : 'save';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label for="nama">Nama Email</label>
        <input type="text" name="nama" id="nama" value="<?php echo $nama; ?>" class="form-control" placeholder="Nama Email" autofocus="autofocus" autocomplete="off">
        <label for="username">Username Email</label>
        <input type="text" name="username" id="username" value="<?php echo $username; ?>" class="form-control" placeholder="Username" autofocus="autofocus" autocomplete="off">
        <label for="password">Password</label>
        <input type="text" name="password" id="password" value="<?php echo $password; ?>" class="form-control" placeholder="Password Email" autofocus="autofocus" autocomplete="off">
        <label for="port">Port</label>
        <input type="text" name="port" id="port" value="<?php echo $port; ?>" class="form-control" placeholder="Port" autofocus="autofocus" autocomplete="off">
        <label for="tipe">Tipe</label>
        <input type="text" name="tipe" id="tipe" value="<?php echo $tipe; ?>" class="form-control" placeholder="Tipe" autofocus="autofocus" autocomplete="off">
      </div>
      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>