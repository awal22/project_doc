<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Awaluddin <awaluddinbinusep@gmail.com>
 */
class Setup_email extends MX_Controller
{
  private $modul = 'Setup_email';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));
    $this->load->model('setup_email/setup_email_model', 'email_setup');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);
    $id = "setup_email_id";
    $param['order_by']['setup_email_id'] = $id;
    $row = $this->email_setup->get_data($param);
    if ($row->num_rows() > 0) {
      $data['row'] = $row->row();
    }

    $this->template->load('templates/admin', 'form', $data);
  }

  public function simpan()
  {
    
    $id = $this->input->post('id', TRUE);
    $nama = $this->input->post('nama', TRUE);
    $username = $this->input->post('username', TRUE);
    $password = $this->input->post('password', TRUE);
    $port = $this->input->post('port', TRUE);
    $tipe = $this->input->post('tipe', TRUE);
    
    $save = $this->input->post('save', TRUE);

    switch ($save) {
      case 'save':
        $data['setup_email_nama'] = $nama;
        $data['setup_email_username'] = $username;
        $data['setup_email_password'] = $password;
        $data['setup_email_port'] = $port;
        $data['setup_email_tipe'] = $tipe;
        $data['setup_email_created_at'] = date('Y-m-d H:i:s');
        $data['setup_email_created_by'] = $this->session->userdata('userId');
        $insert = $this->email_setup->insert($data);

        if ($insert == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else 
        {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;

      case 'update':
    
        $param['data']['setup_email_nama'] = $nama;
        $param['data']['setup_email_username'] = $username;
        $param['data']['setup_email_password'] = $password;
        $param['data']['setup_email_port'] = $port;
        $param['data']['setup_email_tipe'] = $tipe;
        
        $param['data']['setup_email_update_at'] = date('Y-m-d H:i:s');
        $param['data']['setup_email_update_by'] = $this->session->userdata('userId');
        $param['where']['setup_email_id'] = $id;
        $update = $this->email_setup->update($param);

        if ($update == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));   
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

}