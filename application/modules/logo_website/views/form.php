<?php
  if ( ! is_null($this->session->flashdata('alert_class')))
  {
    ?>
    <div id="alert" class="alert alert-<?php echo $this->session->flashdata('alert_class'); ?>" role="alert"><?php echo $this->session->flashdata('alert_text'); ?></div>
    <?php
  }
?>
<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->id_logowebsite : '';
    $nama = isset($row) ? $row->nama_website : '';
    $gambar = isset($row) ? $row->gambar : '';

    $btn_val = isset($row) ? 'update' : 'save';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label for="nama">Nama Website</label>
        <input type="text" name="nama_web" id="nama_web" value="<?php echo $nama; ?>" class="form-control" placeholder="Nama Website" autofocus="autofocus" autocomplete="off">
        <label for="favicon">Logo Website</label>
        <input type="file" name="gambar" id="gambar" class="form-control" value="<?php echo $gambar; ?>">
        <label>Logo Website Aktif Saat ini : <?php echo "<img src='".base_url()."foto/$gambar'>" ?>
      </div>
      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>