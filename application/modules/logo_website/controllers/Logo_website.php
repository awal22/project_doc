<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Logo_website extends MX_Controller
{
  private $modul = 'Logo_website';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));
    $this->load->library('upload');
    $this->load->model('logo_website/logo_web_model', 'logo');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);
    $id = "id_logowebsite";
    $param['order_by']['id_logowebsite'] = $id;
    $row = $this->logo->get_data($param);
    if ($row->num_rows() > 0) {
      $data['row'] = $row->row();
    }

    $this->template->load('templates/admin', 'form', $data);
  }

  public function simpan()
  {
    if($_FILES['gambar']['name'])
    {
      $fileName = "file_1_".time();
      $config['file_name'] = $fileName;
      $config['upload_path'] = './foto/';
      $config['allowed_types'] = 'ico|gif|jpg|png';
      $config['max_size'] = 2048;

      $this->upload->initialize($config);
      if($this->upload->do_upload('gambar'))
      {
        $gbr = $this->upload->data();
        //Compress Image
        $config['image_library']='gd2';
        $config['source_image']='./foto/'.$gbr['file_name'];
        $config['create_thumb']= FALSE;
        $config['maintain_ratio']= FALSE;
        $config['quality']= '100%';
        $config['width']= 300;
        $config['height']= 55;
        $config['new_image']= './foto/'.$gbr['file_name'];
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
        $foto = $gbr['file_name'];
        }
        else
        {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
    }
    else
    {
      $foto = "";
    }


    $id = $this->input->post('id', TRUE);
    $nama = $this->input->post('nama_web', TRUE);
    $save = $this->input->post('save', TRUE);

    switch ($save) {
      case 'save':
        $data['nama_website'] = $nama;
        $data['gambar'] = $foto;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->session->userdata('userId');
        $insert = $this->logo->insert($data);

        if ($insert == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else 
        {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
        
      if($_FILES['gambar']['name'])
        {
          $fileName = "file_1_".time();
          $config['file_name'] = $fileName;
          $config['upload_path'] = './foto/';
          $config['allowed_types'] = 'ico|gif|jpg|png';
          $config['max_size'] = 2048;

          $this->upload->initialize($config);
          if($this->upload->do_upload())
          {
            $gbr = $this->upload->data();
            //Compress Image
            $config['image_library']='gd2';
            $config['source_image']='./foto/'.$gbr['file_name'];
            $config['create_thumb']= FALSE;
            $config['maintain_ratio']= FALSE;
            $config['quality']= '100%';
            $config['width']= 300;
            $config['height']= 55;
            $config['new_image']= './foto/'.$gbr['file_name'];
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            $foto = $gbr['file_name'];
            }
            else
            {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
          }
        }
        else
        {
          $foto = "";
        }

      case 'update':
        $param['data']['nama_website'] = $nama;
        $param['data']['gambar'] = $foto;
       
        $param['data']['updated_at'] = date('Y-m-d H:i:s');
        $param['data']['updated_by'] = $this->session->userdata('userId');
        $param['where']['id_logowebsite'] = $id;
        $update = $this->logo->update($param);

        if ($update == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));   
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }
}