<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->ijin_id : '';
    $user_name = $this->session->userdata('userId');
    $ijin_jenis = isset($row) ? $row->ijin_jenis : '';
    $ijin_hari = isset($row) ? $row->ijin_hari : '';
    $keterangan = isset($row) ? $row->ijin_ket : '';
    $tanggal = isset($row) ? $row->ijin_tanggal : '';
    $btn_val = isset($row->ijin_id) ? 'update' : 'save';

    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <input type="hidden" name="user_nama" value="<?php echo $user_name; ?>">

      <div class="row">
      <div class="col-md-4">
        <div class="form-group">
            <label for="tanggal_ijin">Tanggal Ijin</label>
            <input type="text" name="tanggal" id="tanggal" value="<?php echo $tanggal; ?>" class="form-control" placeholder="Tanggal Ijin" autofocus="autofocus" autocomplete="off">
          </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
            <label for="tanggal_ijin">Jumlah Hari Ijin</label>
            <input type="text" name="hari_ijin" id="hari_ijin" value="<?php echo $ijin_hari; ?>" class="form-control" placeholder="Jumlah Hari Ijin" autofocus="autofocus" autocomplete="off" readonly>
          </div>
      </div>

      <div class="col-md-9">
        <div class="form-group">
          <label for="nama">Jenis Ijin</label>
          <select name="jenis" id="jenis" class="form-control">  
            <option value="0" <?php echo $selected = $ijin_jenis == "0" ? "selected=\"selected\"" : "";?>>Pilih Jenis</option>
            <option value="1" <?php echo $selected = $ijin_jenis == "1" ? "selected=\"selected\"" : "";?>>Ijin Cuti</option>
            <option value="2" <?php echo $selected = $ijin_jenis == "2" ? "selected=\"selected\"" : "";?>>Ijin Sakit</option>
            <option value="3" <?php echo $selected = $ijin_jenis == "3" ? "selected=\"selected\"" : "";?>>Ijin Menikah</option>
            <option value="4" <?php echo $selected = $ijin_jenis == "4" ? "selected=\"selected\"" : "";?>>Ijin Darurat</option>
          </select>
        </div>
      
        <div class="form-group">
          <label for="keterangan">Keterangan Cuti</label>
          <textarea name="keterangan" id="keterangan" cols="30" rows="10" placeholder="Keterangan Cuti..." class="form-control"><?php echo $keterangan; ?></textarea>
        </div>
      
    <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
  </form>
  </div>   
</div>

<script>
  $(document).ready(function () {
    /*
    $("#tanggal").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      orientation: "bottom auto",
      todayHighlight: true,
      todayBtn: "linked"
    });
    */

    $('input[name="tanggal"]').daterangepicker({
      opens: 'left'
    }, function(start, end, label) {
      console.log("A new date selection was made: " + start.format('DD') + ' to ' + end.format('YYYY-MM-DD'));
      var tglMulai = start.format('DD');
      var tglSelesai = end.format('DD');
      var hari = tglSelesai - tglMulai+1;
      console.log("hari cuti anda " + hari + " hari");
      $("#hari_ijin").val(hari);
    });
  });
  </script>