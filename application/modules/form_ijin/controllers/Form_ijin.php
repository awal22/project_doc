<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Awaluddin <awaluddinbinusep@gmail.com>
 */
class Form_ijin extends MX_Controller
{
  private $modul = 'Form_ijin';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));

    $this->load->model('form_ijin/Ijin_model', 'ijin');
    $this->load->model('form_ijin/V_ijin_model', 'ijinModel');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'index', $data);
  }

  public function add()
  {
    $this->acl->check_create(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'Add';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'form', $data);
  }

  public function edit(int $id = NULL)
  {
    $this->acl->check_update(strtolower($this->modul));

    if ( ! is_null($id)) {
      $param['where']['ijin_id'] = $id;
      $row = $this->ijin->get_data($param);

      if ($row->num_rows() > 0) {
        $data['row'] = $row->row();
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak ditemukan";
        $this->session->set_flashdata($flashdata);
  
        redirect(strtolower($this->modul));
      }
  
      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Edit';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'form', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
			$this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function simpan()
  {
    $save = $this->input->post('save', TRUE);

    switch ($save) {
      case 'save':
        $insert = $this->ijin->insert($this->input->post(NULL, TRUE));

        if ($insert == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;

      case 'update':
        $update = $this->ijin->update($this->input->post(NULL, TRUE));

        if ($update == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

  public function hapus(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));

    $this->ijin->delete($id);

    $flashdata["alert_class"] = "success";
    $flashdata["alert_text"] = "Data berhasil dihapus";
    $this->session->set_flashdata($flashdata);

    redirect(strtolower($this->modul));
  }

  public function datagrid()
  {
    $list = $this->ijinModel->get_datatables($this->input->post(NULL, TRUE));
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {

      if($rw->ijin_status == '1'){
        $tombol = 'disabled';
        $edit = "<button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" disabled>Edit</button>";
      }else{
        $tombol = 'enabled';
        $edit = "<a href=\"".site_url(strtolower($this->modul)."/edit/".$rw->ijin_id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>";
      }
      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rw->user_name;
      $row[] = $rw->ijin_tanggal;
      $row[] = $rw->ijin_hari;
      $row[] = $rw->status_ijin;
      $row[] = $rw->jabatan_nama;
      $row[] = $rw->ijin_created_at;
      $row[] = "<div class=\"text-center\">
              $edit
              <button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->ijin_id."','".$rw->user_name."');\" $tombol>Delete</button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->ijinModel->count_all(),
      "recordsFiltered" => $this->ijinModel->count_filtered($this->input->post(NULL, TRUE)),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }

  
}
