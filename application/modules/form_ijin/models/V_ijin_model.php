<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */

class V_ijin_model extends CI_Model
{
  private $table = 'v_form_ijin';
  private $column_order = array(null, 'user_name', 'status_ijin', 'ijin_tanggal', 'jabatan_nama', 'ijin_created_at');
  private $column_search = array('user_name', 'status_ijin', 'ijin_tanggal', 'jabatan_nama', 'ijin_created_at');
  private $order = array('ijin_id' => 'desc');

  public function get_data(Array $param = NULL)
  {
    if ( ! is_null($param)) {
      if (array_key_exists('select', $param)) {
        $this->db->select($param['select']);
      }

      if (array_key_exists('distinct', $param)) {
        $this->db->distinct();
      }

      if (array_key_exists('where', $param)) {
        if (is_array($param['where'])) {
          foreach ($param['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($param['where']);
        }
      }

      if (array_key_exists('or_where', $param)) {
        if (is_array($param['or_where'])) {
          foreach ($param['or_where'] as $key => $value) {
            $this->db->or_where($key, $value);
          }
        }
        else {
          $this->db->or_where($param['or_where']);
        }
      }

      if (array_key_exists('like', $param)) {
        if (is_array($param['like'])) {
          foreach ($param['like'] as $key => $value) {
            $this->db->like($key, $value);
          }
        }
        else {
          $this->db->like($param['like']);
        }
      }

      if (array_key_exists('or_like', $param)) {
        if (is_array($param['or_like'])) {
          foreach ($param['or_like'] as $key => $value) {
            $this->db->or_like($key, $value);
          }
        }
        else {
          $this->db->or_like($param['or_like']);
        }
      }
    }

    $this->db->from($this->table);

    return $this->db->get();
  }

  public function insert($data)
  {
    $this->db->trans_begin();
    
    $dataInsert['ijin_nama'] = $data['user_nama'];
    $dataInsert['ijin_jenis'] = $data['jenis'];
    $dataInsert['ijin_ket'] = $data['keterangan'];
    $dataInsert['ijin_tanggal'] = $data['tanggal'];
    $dataInsert['created_at'] = date('Y-m-d H:i:s');
    $dataInsert['created_by'] = $this->session->userdata('userId');
    $this->db->insert($this->table, $dataInsert);

    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE) {
      return '0';
    }
    else {
      return '1';
    }
  }

  public function update($data)
  {
    $this->db->trans_begin();
    
    $param['user_name'] = $data['user_name'];
    $data['user_password'] == '' ? '' : $param['user_password'] = password_hash($user_password, PASSWORD_DEFAULT);
    $param['user_email'] = $data['user_email'];
    $param['user_jabatan'] = $data['jabatan'];
    $param['active'] = $data['active'];
    $param['updated_at'] = date('Y-m-d H:i:s');
    $param['updated_by'] = $this->session->userdata('userId');
    $this->db->where('id', $data['id']);
    $this->db->update($this->table, $param);
    
    // delete dulu trus insert baru
    //var_dump($data['id']);exit;
    $this->db->where('ur_user', $data['id']);
    $this->db->delete('user_roles');

    foreach ($data['ur_role'] as $key => $value) {
      $param2['ur_user'] = $data['id'];
      $param2['ur_role'] = $value;
      $param2['updated_at'] = date('Y-m-d H:i:s');
      $param2['updated_by'] = $this->session->userdata('userId');
      $this->db->insert('user_roles', $param2);
    }

    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE) {
      return '0';
    }
    else {
      return '1';
    }
  }

  public function delete($data)
  {
    $this->db->trans_begin();

    $this->db->where('ur_role', $data);
    $this->db->delete('user_roles');
    
    $this->db->where('id', $data);
    $this->db->delete('users');

    $this->db->trans_complete();
  }

  private function _get_datatables_query($post)
  {
    $id_pegawai = $this->session->userdata('userId');
    $this->db->select("*");
    $this->db->where('ijin_nama', $id_pegawai);
    $this->db->from($this->table);

    $i = 0;

    foreach ($this->column_search as $item) // loop column
    {
      if($post['search']['value']) // if datatable send POST for search
      {
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $post['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $post['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
        {
          $this->db->group_end(); //close bracket
        }
      }
      $i++;
    }

    if(isset($post['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$post['order']['0']['column']], $post['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables($post)
  {
    $this->_get_datatables_query($post);
    if($post['length'] != -1)
    {
      $this->db->limit($post['length'], $post['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered($post)
  {
    $this->_get_datatables_query($post);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
}
