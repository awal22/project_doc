<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class User extends MX_Controller
{
  private $modul = 'User';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));

    $this->load->model('user/User_model', 'user');
    $this->load->model('user/V_user_role_model', 'userRole');
    $this->load->model('role/Role_model', 'role');
    $this->load->model('jabatan/Jabatan_model', 'jabatan');
    $this->load->model('user/V_user_model', 'userModel');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'index', $data);
  }

  public function add()
  {
    $this->acl->check_create(strtolower($this->modul));

    $qryJabatan = $this->jabatan->get_data();
    $data['qryJabatan'] = $qryJabatan->result();

    $role = $this->role->get_data(NULL);
    $data['role'] = $role->result();

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'Add';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'form', $data);
  }

  public function edit(int $id = NULL)
  {
    $this->acl->check_update(strtolower($this->modul));

    if ( ! is_null($id)) {
      $param['where']['id'] = $id;
      $row = $this->user->get_data($param);

      if ($row->num_rows() > 0) {
        $data['row'] = $row->row();
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak ditemukan";
        $this->session->set_flashdata($flashdata);
  
        redirect(strtolower($this->modul));
      }

      $qryJabatan = $this->jabatan->get_data();
      $data['qryJabatan'] = $qryJabatan->result();

      $role = $this->role->get_data(NULL);
      $data['role'] = $role->result();
  
      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Edit';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'form', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
			$this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function simpan()
  {
    $save = $this->input->post('save', TRUE);

    switch ($save) {
      case 'save':
        $insert = $this->user->insert($this->input->post(NULL, TRUE));

        if ($insert == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;

      case 'update':
        $update = $this->user->update($this->input->post(NULL, TRUE));

        if ($update == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

  public function hapus(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));

    $this->user->delete($id);

    $flashdata["alert_class"] = "success";
    $flashdata["alert_text"] = "Data berhasil dihapus";
    $this->session->set_flashdata($flashdata);

    redirect(strtolower($this->modul));
  }

  public function datagrid()
  {
    $list = $this->userModel->get_datatables($this->input->post(NULL, TRUE));
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $label = $rw->active == 'Aktif' ? 'success' : 'warning';

      $btnRole = '<button type="button" class="btn btn-primary btn-flat btn-xs" onclick="modal_role(\''.$rw->user_id.'\');">Change Password</button>';

      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rw->user_name;
      $row[] = $rw->user_email;
      $row[] = $rw->jabatan_nama;
      $row[] = '<span class="label label-'.$label.' center-block">'.$rw->active.'</span>';
      $row[] = $rw->created_at;
      $row[] = "<div class=\"text-center\">
              <a href=\"".site_url(strtolower($this->modul)."/edit/".$rw->user_id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>
              <button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->user_id."','".$rw->user_name."');\">Delete</button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->userModel->count_all(),
      "recordsFiltered" => $this->userModel->count_filtered($this->input->post(NULL, TRUE)),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }

  public function check_email()
  {
    $user_email = $this->input->post('user_email', TRUE);
    $param['where']['user_email'] = $user_email;
    $user = $this->userModel->get_data($param);

    if ($user->num_rows() > 0) {
      echo 'duplicate';
    }
  }
}
