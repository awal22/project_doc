<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->id : '';
    $user_name = isset($row) ? $row->user_name : '';
    $user_email = isset($row) ? $row->user_email : '';
    $user_jabatan = isset($row) ? $row->user_jabatan : '';
    $user_active = isset($row) ? $row->active : '';
    $btn_val = isset($row) ? 'update' : 'save';

    $arr_active = array(1 => 'Aktif', 0 => 'Tidak Aktif');

    $req_password = isset($row) ? '' : 'required="required"';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      
      <div class="form-group">
        <label for="user_name">User Name</label>
        <input type="text" name="user_name" id="user_name" value="<?php echo $user_name; ?>" class="form-control" placeholder="User Name" autofocus="autofocus" autocomplete="off" required="required">
      </div>
      
      <div class="form-group">
        <label for="user_password">Password</label>
        <input type="password" name="user_password" id="user_password" class="form-control" placeholder="Password" <?php echo $req_password; ?>>
      </div>
      
      <div class="form-group" id="div-email">
        <label for="user_email">User Email</label>
        <input type="email" name="user_email" id="user_email" value="<?php echo $user_email; ?>" class="form-control" placeholder="User Email" autocomplete="off" required="required">
        <span id="helpBlockEmail" class="help-block"></span>
      </div>

      <div class="form-group">
        <label for="nama">Jabatan</label>
        <select name="jabatan" id="jabatan" class="form-control">
          <option value="">Pilih Jabatan</option>
          <?php
          foreach ($qryJabatan as $rowJabatan) {
            $sel_jabatan_id = $user_jabatan == $rowJabatan->id ? 'selected="selected"' : '';
            ?>
            <option value="<?php echo $rowJabatan->id; ?>" <?php echo $sel_jabatan_id; ?>><?php echo $rowJabatan->nama; ?></option>
            <?php
          }
          ?>
        </select>
      </div>
      
      <div class="form-group">
        <label for="active">Active</label>
        <select name="active" id="active" class="form-control" required="required">
          <option value="" selected="selected">Pilih</option>
          <?php
          foreach ($arr_active as $kActive => $vActive) {
            $selActive = $kActive == $user_active ? 'selected="selected"' : '';
            ?>
            <option value="<?php echo $kActive; ?>" <?php echo $selActive; ?>><?php echo $vActive; ?></option>
            <?php
          }
          ?>
        </select>
      </div>

      <div class="form-group">
        <!-- <label for="">Role</label> -->
        <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th>Role</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($role as $vRole) {
              $pUserRole['where']['ur_user'] = $id;
              $pUserRole['where']['ur_role'] = $vRole->id;
              $userRole = $this->userRole->get_data($pUserRole);

              $userRoleId = $userRole->num_rows() > 0 ? $userRole->row()->ur_role : 0;

              $checked = $vRole->id == $userRoleId ? 'checked="checked"' : '';
              ?>
              <tr>
                <td><?php echo $vRole->role_name; ?></td>
                <td><input type="checkbox" name="ur_role[]" id="ur_role" value="<?php echo $vRole->id; ?>" <?php echo $checked; ?>></td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
      </div>

      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $("#jabatan").select2();

  <?php
  if ($btn_val == 'save') {
    ?>
      $("#user_email").blur(function(){
        var email = $(this).val();
        $.ajax({
          url: '<?php echo site_url($modul.'/check_email'); ?>',
          data: {user_email: email},
          method: 'POST',
          success: function(response) {
            if (response == 'duplicate') {
              $("#user_email").focus();
              $("#div-email").addClass('has-warning');
              $("#helpBlockEmail").html('Email sudah terdaftar');
            }
            else {
              $("#div-email").removeClass('has-warning');
              $("#helpBlockEmail").html('');
            }
          }
        });
      });
    <?php
  }
  ?>
});
</script>