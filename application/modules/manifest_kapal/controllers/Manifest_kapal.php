<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Awaluddin <awaluddinbinusep@gmail.com>
 */
class Manifest_kapal extends MX_Controller
{
  private $modul = 'Manifest_kapal';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));
    $this->load->model('manifest_kapal/manifest_kapal_model', 'manifest_model');
    $this->load->model('manifest_kapal/v_manifest_kapal_model', 'v_manifest_model');
    $this->load->model('kapal/kapal_model', 'kapal');
    $this->load->model('user/user_model', 'user');
    $this->load->model('manifest_kapal/manifest_detail_model', 'mdk_model');
    $this->load->model('manifest_kapal/v_crew_manifest_model', 'v_crew_manifest');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'index', $data);
  }

  public function add()
  {
    $this->acl->check_create(strtolower($this->modul));

    $qryKapal = $this->kapal->get_data();
    $data['kapal'] = $qryKapal->result();

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'Add';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'form', $data);
  }

  public function edit(int $id = NULL)
  {
    $this->acl->check_update(strtolower($this->modul));

    if ( ! is_null($id)) {
      $param['where']['manifest_kapal_id'] = $id;
      $row = $this->manifest_model->get_data($param);

      if ($row->num_rows() > 0) {
        $data['row'] = $row->row();
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak ditemukan";
        $this->session->set_flashdata($flashdata);
  
        redirect(strtolower($this->modul));
      }

      $qryKapal = $this->kapal->get_data();
      $data['kapal'] = $qryKapal->result();
  
      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Edit';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'form', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
			$this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function add_crew(int $id = NULL)
  {
    $this->acl->check_create(strtolower($this->modul));

    if ( ! is_null($id)) {
      $data['id'] = $id;

      $qryKapal = $this->kapal->get_data();
      $data['kapal'] = $qryKapal->result();

      $param['where']['user_jabatan'] = 157;
      //$row = $this->user->get_data($param);
      $qryUserModel = $this->user->get_data($param);
      $data['crew'] = $qryUserModel->result();
  
      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Add Data Crew';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'index_add_crew', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
			$this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function simpan()
  {
    
    $id = $this->input->post('id', TRUE);
    $nama = $this->input->post('nama', TRUE);
    $tanggal = $this->input->post('tanggal', TRUE);
    $keterangan = $this->input->post('keterangan', TRUE);
    
    $save = $this->input->post('save', TRUE);

    switch ($save) {
      case 'save':
        $data['manifest_kapal_nama'] = $nama;
        $data['manifest_kapal_tanggal'] = $tanggal;
        $data['manifest_kapal_ket'] = $keterangan;
        $data['manifest_kapal_created_at'] = date('Y-m-d H:i:s');
        $data['manifest_kapal_created_by'] = $this->session->userdata('userId');
        $insert = $this->manifest_model->insert($data);

        if ($insert == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else 
        {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;

      case 'update':
    
        $param['data']['manifest_kapal_nama'] = $nama;
        $param['data']['manifest_kapal_tanggal'] = $tanggal;
        $param['data']['manifest_kapal_ket'] = $keterangan;
        
        $param['data']['manifest_kapal_updated_at'] = date('Y-m-d H:i:s');
        $param['data']['manifest_kapal_updated_by'] = $this->session->userdata('userId');
        $param['where']['manifest_kapal_id'] = $id;
        $update = $this->manifest_model->update($param);

        if ($update == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));   
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

  public function simpan_data_crew()
  {
    
    $id = $this->input->post('idm', TRUE);
    $nama = $this->input->post('nama', TRUE);
    
    $data['mdk_manifest_id'] = $id;
    $data['mdk_pegawai'] = $nama;
    $data['mdk_created_at'] = date('Y-m-d H:i:s');
    $data['mdk_created_by'] = $this->session->userdata('userId');
    $insert = $this->mdk_model->insert($data);

    if ($insert) {
      $flashdata["msg"] = "success";
      //$flashdataa["alert_class"] = "success";
      //$flashdataa["alert_text"] = "Data berhasil disimpan";
      //$this->session->set_flashdata($flashdataa);
      
      //redirect(strtolower($this->modul));
    }
    else 
    {
      $flashdata["msg"] = "warning";
      //$flashdataa["alert_class"] = "success";
      //$flashdataa["alert_text"] = "Data berhasil disimpan";
      //$this->session->set_flashdata($flashdataa);

      //redirect(strtolower($this->modul));
    }
    //var_dump($flashdata);exit;
    echo json_encode($flashdata);

    
  }

  public function hapus(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));

    $param['where']['manifest_kapal_id'] = $id;
    $this->manifest_model->delete($param);

    $flashdata["alert_class"] = "success";
    $flashdata["alert_text"] = "Data berhasil dihapus";
    $this->session->set_flashdata($flashdata);

    redirect(strtolower($this->modul));
  }

  public function hapus_crew(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));
   
    $param['where']['mdk_id'] = $id;
    $this->mdk_model->delete($param);

    $flashdata["msg"] = "success";

    echo json_encode($flashdata);
  }

  public function datagrid()
  {
    $list = $this->v_manifest_model->get_datatables($this->input->post(NULL, TRUE));
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rw->kapal_nama;
      $row[] = $rw->manifest_kapal_created_at;
      $row[] = "<div class=\"text-center\">
              <a href=\"".site_url(strtolower($this->modul)."/edit/".$rw->manifest_kapal_id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>
              <a href=\"".site_url(strtolower($this->modul)."/add_crew/".$rw->manifest_kapal_id)."\" class=\"btn btn-primary btn-flat btn-xs\">Add Crew Kapal</a>
              <button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->manifest_kapal_id."','".$rw->kapal_nama."');\">Delete</button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->v_manifest_model->count_all(),
      "recordsFiltered" => $this->v_manifest_model->count_filtered($this->input->post(NULL, TRUE)),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }

  public function manifest_grid()
  {
    $idm = $this->input->post('id_manifest');
    //var_dump($ids);exit;
    $paramss['from'] = 'v_crew_manifest_kapal';
		$paramss['select'] = '*';
    $paramss['where']['manifest_kapal_id'] = $idm;
		$paramss['column_order'] = array(null, 'user_name', 'kapal_nama', 'manifest_kapal_tanggal');
		$paramss['column_search'] = array('user_name','kapal_nama','manifest_kapal_tanggal');
		$paramss['order'] = array('mdk_id' => 'desc');
		$paramss['post'] = $this->input->post(NULL, TRUE);
    
    
    
    //$param['post'] = $this->input->post(NULL, TRUE);
    $list = $this->v_crew_manifest->get_datatables($paramss);
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rwm) {
      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rwm->user_name;
      $row[] = $rwm->kapal_nama;
      $row[] = $rwm->manifest_kapal_tanggal;
      $row[] = "<div class=\"text-center\">
              <button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItemCrew('".$rwm->mdk_id."','".$rwm->user_name."');\">Delete</button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->v_crew_manifest->count_all($paramss['from']),
      "recordsFiltered" => $this->v_crew_manifest->count_filtered($paramss),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
    
  }

}