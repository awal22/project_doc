<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Awaluddin <awaluddinbinusep@gmail.com>
 */

class V_crew_manifest_model extends CI_Model
{
  //private $table = 'v_crew_manifest_kapal';
  

  public function get_data(Array $param = NULL)
  {
    if ( ! is_null($param)) {
      if (array_key_exists('select', $param)) {
        $this->db->select($param['select']);
      }

      if (array_key_exists('order_by', $param)) {
        foreach ($param['order_by'] as $key => $value) {
          $this->db->order_by($key, $value);
        }
      }

      if (array_key_exists('distinct', $param)) {
        $this->db->distinct();
      }

      if (array_key_exists('where', $param)) {
        if (is_array($param['where'])) {
          foreach ($param['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($param['where']);
        }
      }

      if (array_key_exists('or_where', $param)) {
        if (is_array($param['or_where'])) {
          foreach ($param['or_where'] as $key => $value) {
            $this->db->or_where($key, $value);
          }
        }
        else {
          $this->db->or_where($param['or_where']);
        }
      }

      if (array_key_exists('like', $param)) {
        if (is_array($param['like'])) {
          foreach ($param['like'] as $key => $value) {
            $this->db->like($key, $value);
          }
        }
        else {
          $this->db->like($param['like']);
        }
      }

      if (array_key_exists('or_like', $param)) {
        if (is_array($param['or_like'])) {
          foreach ($param['or_like'] as $key => $value) {
            $this->db->or_like($key, $value);
          }
        }
        else {
          $this->db->or_like($param['or_like']);
        }
      }
    }

    $this->db->from($this->table);

    return $this->db->get();
  }

  public function insert($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->affected_rows();
  }

  public function update(Array $param = NULL)
  {
    if (array_key_exists('where', $param)) {
      if (is_array($param['where'])) {
        foreach ($param['where'] as $key => $value) {
          $this->db->where($key, $value);
        }
      }
      else {
        $this->db->where($param['where']);
      }
    }

    $this->db->update($this->table, $param['data']);
    return $this->db->affected_rows();
  }

  public function delete(Array $param = NULL)
  {
    if ( ! is_null($param)) {
      if (array_key_exists('where', $param)) {
        if (is_array($param['where'])) {
          foreach ($param['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($param['where']);
        }
      }
    }

    $this->db->delete($this->table);
  }

  private function _get_datatables_query(Array $param)
	{
		$this->db->from($param['from']);
		
		if (array_key_exists('select', $param)) {
			$this->db->select($param['select']);
		}
		
		if (array_key_exists('where', $param)) {
			if (is_array($param['where'])) {
				foreach ($param['where'] as $key => $value) {
					$this->db->where($key, $value);
				}
			} else {
				$this->db->where($param['where']);
			}
		}
  
		if (array_key_exists('or_where', $param)) {
			if (is_array($param['or_where'])) {
				foreach ($param['or_where'] as $key => $value) {
					$this->db->or_where($key, $value);
				}
			} else {
				$this->db->or_where($param['or_where']);
			}
		}
  
		if (array_key_exists('where_in', $param)) {
			if (is_array($param['where_in'])) {
				foreach ($param['where_in'] as $key => $value) {
					$this->db->where_in($key, $value);
				}
			} else {
				$this->db->where_in($param['where_in']);
			}
		}
  
		if (array_key_exists('like', $param)) {
			if (is_array($param['like'])) {
				foreach ($param['like'] as $key => $value) {
					$this->db->like($key, $value);
				}
			} else {
				$this->db->like($param['like']);
			}
		}
  
		if (array_key_exists('or_like', $param)) {
			if (is_array($param['or_like'])) {
				foreach ($param['or_like'] as $key => $value) {
					$this->db->or_like($key, $value);
				}
			} else {
				$this->db->or_like($param['or_like']);
			}
		}
  
	  $i = 0;
  
	  foreach ($param['column_search'] as $item) // loop column
	  {
			if($param['post']['search']['value']) { // if datatable send POST for search
				if($i===0) { // first loop
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $param['post']['search']['value']);
				} else {
					$this->db->or_like($item, $param['post']['search']['value']);
				}
		
				if(count($param['column_search']) - 1 == $i) { //last loop
					$this->db->group_end(); //close bracket
				}
			}
			$i++;
	  }
  
	  if(isset($param['post']['order'])) { // here order processing
			$this->db->order_by($param['column_order'][$param['post']['order']['0']['column']], $param['post']['order']['0']['dir']);
	  } else if(isset($param['order'])) {
			$order = $param['order'];
			$this->db->order_by(key($order), $order[key($order)]);
	  }
	}
	
	function get_datatables(Array $param)
	{
	  $this->load->database();
  
	  $this->_get_datatables_query($param);
	  
	  if ($param['post']['length'] != -1) {
			$this->db->limit($param['post']['length'], $param['post']['start']);
	  }
  
	  if (array_key_exists('compiled', $param)) {
			echo $this->db->get_compiled_select();
			exit;
	  }
  
	  $query = $this->db->get();
	  
	  return $query->result();
  
	  $this->db->close();
	}

	function count_filtered(Array $param)
	{
	  $this->load->database();
  
	  $this->_get_datatables_query($param);
	  $query = $this->db->get();
	  return $query->num_rows();
  
	  $this->db->close();
	}
  
	public function count_all($table)
	{
	  $this->load->database();
  
	  $this->db->from($table);
	  return $this->db->count_all_results();
  
	  $this->db->close();
	}
}