

<div class="row">
  <div class="col-md-12">
   
    <div id="sukses" class="alert alert-success" role="alert"style="display: none;">Data Berhasil disimpan</div>
    <div id="gagal" class="alert alert-warning" role="alert"style="display: none;">Data gagal disimpan</div>
    <div id="sukses_hapus" class="alert alert-success" role="alert"style="display: none;">Data Berhasil dihapus</div>

      
    <div class="box box-solid box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Input Data Crew Kapal :</h3>
      </div>
      <div class="box-body">
        <form id="form-input-crew" class="form-horizontal">
          <div class="form-group">
            <label for="nama" class="col-sm-2 control-label">Pilih Nama Crew Kapal :</label>
            <input type="hidden" name="idm" id="idm" value="<?php echo $id; ?>">
            <div class="col-sm-8">
              <select name="nama" id="nama" class="form-control">
                <option value="" selected="selected">Pilih Nama Crew Kapal</option>
                <?php foreach($crew as $k ) : ?>
                  <option value="<?php echo $k->id; ?>"><?php echo $k->user_name; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        
          <div class="form-group">
            <label for="LastName" class="col-sm-2 control-label"></label>
            <div class="col-sm-8">
              <button type="button" id="btn-filter" class="btn btn-primary">Tambah Data</button>
              <input  type="reset" id="btn-reset" class="btn btn-default" value="Reset" onClick="window.location.href=window.location.href">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="box box-solid box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Data Crew Pada Manifest Kapal</h3>
      </div>
      <div class="box-body table-responsive">
     

        <table class="table table-bordered table-striped table-hover" id="manifest">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Crew</th>
              <th>Kapal</th>
              <th>Tgl Berlayar</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
    
 

</div>

<script>

$(document).ready(function(){
  
  $("#nama").select2();

  var table_manifest = $('#manifest').DataTable({

    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": "<?php echo site_url('manifest_kapal/manifest_grid')?>",
      "type": "POST",
      "data": function ( data ) {
        data.id_manifest = $('#idm').val();
      }
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      {
        "targets": [ 0 ], //first column
        "orderable": false, //set not orderable
      },
      {
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },

    ],

  });

  $('#btn-filter').click(function(){ //button filter event click
    table_manifest.ajax.reload();  //just reload table
    // window.location.href=window.location.href
    $.ajax({
      type: 'POST',
      url: "<?php echo site_url('manifest_kapal/simpan_data_crew');?>",
      data: $('#form-input-crew').serialize(),
      //cache: false,
      dataType: 'json',
      success: function(resp) {
        // alert(data); 
        if (resp.msg == 'success'){
          
          $("#nama").select2("destroy");

          $('#form-input-crew')[0].reset();
          var alert_berhasil = document.getElementById("sukses");
          alert_berhasil.style.display = "block";
          $("#sukses").fadeTo(2000, 500).slideUp(500, function(){
              $("#sukses").slideUp(500);
          });
        }else{
          var alert_gagal = document.getElementById("gagal");
          alert_gagal.style.display = "block";
          $("#gagal").fadeTo(2000, 500).slideUp(500, function(){
              $("#gagal").slideUp(500);
          });

          $('#form-input-crew')[0].reset();
          $('#alert_gagal').toggle('slow');
        }
        $("#nama").select2();
  
      }
    });
  });

});

function deleteItemCrew(i,a)
/*
{
  if (confirm("Anda yakin menghapus data "+a+"?"))
  {
    window.location = "<?php echo site_url($modul.'/hapus_crew/'); ?>"+i;
  }
  return false;
}
*/
{
  
var table_m = $('#manifest').DataTable( {
  ajax: "data.json",
  searching: false,
  paging: false,
  retrieve: true
});
  table_m.ajax.reload();  //just reload table
$.ajax({
    type: 'POST',
    url: "<?php echo site_url('manifest_kapal/hapus_crew/');?>"+i,
    //data: $('#form-input-crew').serialize(),
    //cache: false,
    dataType: 'json',
    success: function(resp) {
      table_m.ajax.reload();
      var alert_berhasil_hapus = document.getElementById("sukses_hapus");
      alert_berhasil_hapus.style.display = "block";
      $("#sukses_hapus").fadeTo(2000, 500).slideUp(500, function(){
          $("#sukses_hapus").slideUp(500);
      });
    }
    
  });
}
</script>