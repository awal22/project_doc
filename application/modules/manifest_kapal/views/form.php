<?php
  if ( ! is_null($this->session->flashdata('alert_class')))
  {
    ?>
    <div id="alert" class="alert alert-<?php echo $this->session->flashdata('alert_class'); ?>" role="alert"><?php echo $this->session->flashdata('alert_text'); ?></div>
    <?php
  }
?>
<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->manifest_kapal_id : '';
    $nama = isset($row) ? $row->manifest_kapal_nama : '';
    $keterangan = isset($row) ? $row->manifest_kapal_ket : '';
    $tanggal = isset($row) ? $row->manifest_kapal_tanggal : '';

    $btn_val = isset($row) ? 'update' : 'save';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <div class="col-md-6">
        <div class="form-group">
            <label for="tanggal">Tanggal Ijin</label>
            <input type="text" name="tanggal" id="tanggal" value="<?php echo $tanggal; ?>" class="form-control" placeholder="Tanggal" autofocus="autofocus" autocomplete="off">
          </div>

          <div class="form-group">
            <label for="nama">Nama Kapal</label>
            <select name="nama" id="nama" class="form-control">
              <option value="">Pilih Nama Kapal</option>
              <?php
              foreach ($kapal as $vKapal) {
                $NamaKapal = $nama == $vKapal->kapal_id ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vKapal->kapal_id; ?>" <?php echo $NamaKapal; ?>><?php echo $vKapal->kapal_nama; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
      </div>
      <div class="col-md-8">
          <div class="form-group">
            <label for="keterangan">Keterangan</label>
            <textarea name="keterangan" id="keterangan" cols="30" rows="10" placeholder="Keterangan" class="form-control"><?php echo $keterangan; ?></textarea>
          </div>

          <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
      </div>

    </form>

  </div>
</div>

<script>
  $(document).ready(function () {
    $("#nama").select2();
    $("#tanggal").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      orientation: "bottom auto",
      todayHighlight: true,
      todayBtn: "linked"
    });
  });
  </script>