<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
  function get_data_peralatan(){
    $query = $this->db->query("SELECT tahun_installasi,SUM(tahun_installasi) AS jumlah FROM peralatan GROUP BY tahun_installasi");
      
    if($query->num_rows() > 0){
      foreach($query->result() as $data){
        $hasil[] = $data;
      }
      return $hasil;
    }
  }

  public function get_kondisi_fasilitas(Array $param = NULL)
  {
    $this->db->from('v_kondisi_fasilitas');

    if (array_key_exists('where', $param)) {
      if (is_array($param['where'])) {
        foreach ($param['where'] as $key => $value) {
          $this->db->where($key, $value);
        }
      }
      else {
        $this->db->where($param['where']);
      }
    }

    return $this->db->get();
  }

  //untuk mengecek jumlah data sesuai parameter
	function check_jumlah_data($table, $parameter){
		$sql = "SELECT COUNT(*) as jml FROM ".$table." WHERE ".$parameter;
        
		//echo $sql;exit;
		
        $qry = $this->db->query($sql);
        return $qry->row();
	}
}