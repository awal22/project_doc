<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */

class Dashboard extends MX_Controller
{
  private $modul = 'Dashboard';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));
    $this->load->model('dashboard/Dashboard_model', 'dashboard');
    $this->load->model('peralatan/Peralatan_model', 'peralatan');
    $this->load->model('peralatan/V_peralatan_model', 'vPeralatan');
    $this->load->model('peralatan/Fasilitas_model', 'fasilitas');
    $this->load->model('kobu/Kobu_model', 'kobu');
    $this->load->model('kacab_pembina/Kacab_pembina_model', 'kacabPembina');
    $this->load->model('lppnpi/Lppnpi_model', 'lppnpi');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $kobu = $this->kobu->get_data(NULL);

    // var_dump($kobu->result());die();
    $data['kobu'] = $kobu->result();

    $kacabPembina = $this->kacabPembina->get_data(NULL);
    $data['kacabPembina'] = $kacabPembina;

    $lppnpi = $this->lppnpi->get_data(NULL);
    // var_dump($lppnpi->result());die();
    $data['lppnpi'] = $lppnpi->result();

    $fasilitas = $this->fasilitas->get_data();
    $data['fasilitas'] = $fasilitas;

    $pJPBT['select'] = 'tahun_installasi, count(tahun_installasi) as jumlah';
    $pJPBT['group_by'] = 'tahun_installasi';
    $jmlPeralatanByTahun = $this->peralatan->get_data($pJPBT);
    $data['jmlPeralatanByTahun'] = $jmlPeralatanByTahun->result();

    // $pComm['where']['fasilitas'] = 'Communication';
    // $rComm = $this->dashboard->get_kondisi_fasilitas($pComm);
    // $resComm = array();
    // foreach ($rComm as $rowComm) {
    // }

    $data['page_header'] = 'Dashboard';

    $query =  $this->db->query("SELECT fasilitas ,sum(fasilitas) as jumlah FROM `v_peralatan` GROUP BY fasilitas"); 
    $row1 = $query->result();
    
    if($this->input->post('kobu'))
    {
        $this->db->where('kobu_id', $this->input->post('kobu'));
    }
    if($this->input->post('lppnpi'))
    {
        $this->db->like('lppnpi_id', $this->input->post('lppnpi'));
    }
    if($this->input->post('fasilitas'))
    {
        $this->db->like('fasilitas', $this->input->post('fasilitas'));
    }
    if($this->input->post('kondisi'))
    {
        $this->db->like('kondisi', $this->input->post('kondisi'));
    }
    $this->db->from("v_kondisi_fasilitas");
    $query2 = $this->db->get();
  
    $results = array();

    foreach ($query2->result_array() as $row)
    {
        if(!isset($results[$row['fasilitas']]))
        $results[$row['fasilitas']] = array('name' => $row['fasilitas'],'jml' => $row['jumlah_fasilitas'], 'label' => array(), 'data' => array());
        $results[$row['fasilitas']]['label'][] = $row['kondisi'] ;
        $results[$row['fasilitas']]['data'][] = $row['jml_kondisi'] ;
    }
    //var_dump(array_values($results));die();
    //Rekey arrays so they aren't associative

    //dokumen kapal
    $params_arsip_kapal_expired = "status_dokumen_kapal = 'expired'";
    $qry_cek_expired = $this->dashboard->check_jumlah_data("v_arsip_kapal",$params_arsip_kapal_expired);
    $data_kapal_expired = $qry_cek_expired->jml;

    $params_arsip_kapal_unexpired = "status_dokumen_kapal = 'unexpired'";
    $qry_cek_unexpired = $this->dashboard->check_jumlah_data("v_arsip_kapal",$params_arsip_kapal_unexpired);
    $data_kapal_unexpired = $qry_cek_unexpired->jml;
    $totDataArsipKapal = $data_kapal_expired + $data_kapal_unexpired;

    //dokumen crew
    $params_arsip_crew_expired = "status_dokumen = 'expired'";
    $qry_cek_expired_crew = $this->dashboard->check_jumlah_data("v_arsip_crew",$params_arsip_crew_expired);
    $data_crew_expired = $qry_cek_expired_crew->jml;

    $params_arsip_crew_unexpired = "status_dokumen = 'unexpired'";
    $qry_cek_unexpired_crew = $this->dashboard->check_jumlah_data("v_arsip_crew",$params_arsip_crew_unexpired);
    $data_crew_unexpired = $qry_cek_unexpired_crew->jml;
    $totDataArsipCrew = $data_crew_expired + $data_crew_unexpired;

    $results = array_values($results);   
    $data['lingkaran'] = $row1;
    $data['results'] = $results;
    
    $data['kapal_expired'] = $data_kapal_expired;
    $data['kapal_unexpired'] = $data_kapal_unexpired;
    $data['totDataArsipKapal'] = $totDataArsipKapal;
    $data['persenDataArsipKapal'] = ($data_kapal_expired/$totDataArsipKapal)*100;

    $data['crew_expired'] = $data_crew_expired;
    $data['crew_unexpired'] = $data_crew_unexpired;
    $data['totDataArsipCrew'] = $totDataArsipCrew;
    $data['persenDataArsipCrew'] = round(($data_crew_expired/$totDataArsipCrew)*100,1);

    $data['chart_data'] = json_encode($results);
    $this->template->load('templates/admin', 'index', $data);
  }

  public function peralatan_grid()
  {
    $list = $this->vPeralatan->get_datatables($this->input->post(NULL, TRUE));
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $no++;
      $row = array();
      $row[] = $rw->nama;
      $row[] = $rw->fasilitas;
      $row[] = $rw->tahun_installasi;
      $row[] = $rw->nama_kobu;
      $row[] = $rw->nama_kacab_pembina;
      $row[] = $rw->lppnpi;
      $row[] = $rw->kondisi;

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vPeralatan->count_all(),
      "recordsFiltered" => $this->vPeralatan->count_filtered($this->input->post(NULL, TRUE)),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }

  public function test()
  {
    // var_dump($_POST);die();
    if($this->input->post('kobu'))
    {
      $this->db->where('kobu_id', $this->input->post('kobu'));
    }
    if($this->input->post('lppnpi'))
    {
      $this->db->like('lppnpi_id', $this->input->post('lppnpi'));
    }
    if($this->input->post('fasilitas'))
    {
      $this->db->like('fasilitas', $this->input->post('fasilitas'));
    }
    if($this->input->post('kondisi'))
    {
      $this->db->like('kondisi', $this->input->post('kondisi'));
    }

    $this->db->from("v_kondisi_fasilitas"); 
    $query2 = $this->db->get();
    //  echo $this->db->last_query();exit();
    //var_dump($query2->result_array());die();
    $results = array();

    foreach ($query2->result_array() as $row)
    {
      if(!isset($results[$row['fasilitas']]))
      $results[$row['fasilitas']] = array('name' => $row['fasilitas'],'jml' => $row['jumlah_fasilitas'], 'label' => array(), 'data' => array());
      $results[$row['fasilitas']]['label'][] = $row['kondisi'] ;
      $results[$row['fasilitas']]['data'][] = $row['jml_kondisi'] ;
    }
  
    //Rekey arrays so they aren't associative
    $results = array_values($results);   
    // $data['lingkaran'] = $row1;
    // $data['results'] = $results;
    echo  json_encode($results);
  }
}
