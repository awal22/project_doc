<!---
<div class="row">
  <div class="col-md-12">
    <div class="box box-solid box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Filter :</h3>
      </div>
      <div class="box-body">
        <form id="form-filter" class="form-horizontal">
          <div class="form-group">
            <label for="kobu" class="col-sm-2 control-label">KOBU :</label>
            <div class="col-sm-8">
              <select name="kobu" id="kobu" class="form-control">
                <option value="" selected="selected">Pilih KOBU</option>
                <?php foreach($kobu as $k ) : ?>
                  <option value="<?php echo $k->id; ?>"><?php echo $k->nama; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="lppnpi" class="col-sm-2 control-label">UPNP :</label>
            <div class="col-sm-8">
              <select name="lppnpi" id="lppnpi" class="form-control">
                <option value="" selected="selected">Pilih Unit Pangkep</option>
                <?php foreach($lppnpi as $l ) : ?>
                  <option value="<?php echo $l->id; ?>"><?php echo $l->nama; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>

                    
          <div class="form-group">
            <label for="fasilitas" class="col-sm-2 control-label">Fasilitas :</label>
            <div class="col-sm-8">
              <select name="fasilitas" id="fasilitas" class="form-control">
                <option value="" selected="selected">Pilih Fasilitas</option>
                <option value="Communication">Communication</option>
                <option value="Navigation">Navigation</option>
                <option value="Surveillance">Surveillance</option>
                <option value="ATC Automation">ATC Automation</option>
              </select>
            </div>
          </div>
                  

          <div class="form-group">
            <label for="lppnpi" class="col-sm-2 control-label">Kondisi :</label>
              <div class="col-sm-8">
              <select name="kondisi" id="kondisi" class="form-control">
                <option value="" selected="selected">Pilih Kondisi</option>
                <option value="Normal">Normal</option>
                <option value="Intermittent">Intermittent</option>
                <option value="U/S">U/S</option>
              </select>
            </div>
          </div>
                  
          <div class="form-group">
            <label for="LastName" class="col-sm-2 control-label"></label>
            <div class="col-sm-8">
              <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
              <input  type="reset" id="btn-reset" class="btn btn-default" value="Reset" onClick="window.location.href=window.location.href">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="box box-solid box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Peralatan</h3>
      </div>
      <div class="box-body table-responsive">
     

        <table class="table table-bordered table-striped table-hover" id="prlt">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Fasilitas</th>
              <th>Tahun Instalasi</th>
              <th>KOBU</th>
              <th>Kacab Pembina</th>
              <th>LPPNPI</th>
              <th>Kondisi</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
    

  <?php
  for ($i=0; $i  < count($results) ; $i++) {
    ?>
      <div class="col-md-3">
        <div class="box box-primary">
          <div class="box-body">
            <canvas id="pie-chart<?php echo $i ?>"></canvas>
          </div>
        </div>
      </div>
    <?php
  }
  ?>



      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-body">
            <canvas id="pie-chart"></canvas>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-body">
            <canvas id="pie-chart-dok-crew"></canvas>
          </div>
        </div>
      </div>
   

</div>

<script>
$(function(){

  //get the pie chart canvas
  var cData = JSON.parse(`<?php echo $chart_data; ?>`);
    
  var ctx= [];
  for (var i = 0; i < cData.length; i++) {

    ctx[i] = $("#pie-chart"+i+"");
    //pie chart data
    var data = {
      labels: cData[i].label,
      datasets: [
        {
          // label: "Users Count",
          data: cData[i].data,
          backgroundColor: [
            "#66882D",
            "#ffea00",
            "#ff0000",
          ],
          borderColor: [
            "#66882D",
            "#ffea00",
            "#ff0000",
          ],
          borderWidth: [1, 1, 1]
        }
      ]
    };

    //options
    var options = {
      responsive: true,
      title: {
        display: true,
        position: "top",
        text: [cData[i].name,cData[i].jml+" units"],
        fontSize: 18,
        fontColor: "#111"
      },
      
      legend: {
        display: true,
        position: "right",
        labels: {
          fontColor: "#333",
          fontSize: 16
        }
      }
    };

  
    //create Pie Chart class object
    var chart1 = new Chart(ctx[i], {
      type: "pie",
      data: data,
      options: options
    });
  }
});


$(function(){

  //get the pie chart canvas
  var cData = JSON.parse(`[{"name":"Dokumen Arsip Kapal","jml":"<?php echo $totDataArsipKapal; ?>","label":["Expired","Unexpired"],"data":["<?php echo $kapal_expired; ?>","<?php echo $kapal_unexpired; ?>"]}]`);
    
  var ctx= [];
  for (var i = 0; i < cData.length; i++) {

    ctx[i] = $("#pie-chart");
    //pie chart data
    var data = {
      labels: cData[i].label,
      datasets: [
        {
          // label: "Users Count",
          data: cData[i].data,
          backgroundColor: [
            "#66882D",
            "#ffea00",
            "#ff0000",
          ],
          borderColor: [
            "#66882D",
            "#ffea00",
            "#ff0000",
          ],
          borderWidth: [1, 1, 1]
        }
      ]
    };

    //options
    var options = {
      responsive: true,
      title: {
        display: true,
        position: "top",
        text: [cData[i].name,cData[i].jml+" Dokumen"],
        fontSize: 18,
        fontColor: "#111"
      },
      
      legend: {
        display: true,
        position: "right",
        labels: {
          fontColor: "#333",
          fontSize: 16
        }
      }
    };

  
    //create Pie Chart class object
    var chart1 = new Chart(ctx[i], {
      type: "pie",
      data: data,
      options: options
    });
  }
});

$(function(){

//get the pie chart canvas
var cData = JSON.parse(`[{"name":"Dokumen Arsip Crew","jml":"<?php echo $totDataArsipCrew; ?>","label":["Expired","Unexpired"],"data":["<?php echo $crew_expired; ?>","<?php echo $crew_unexpired; ?>"]}]`);
  
var ctx= [];
for (var i = 0; i < cData.length; i++) {

  ctx[i] = $("#pie-chart-dok-crew");
  //pie chart data
  var data = {
    labels: cData[i].label,
    datasets: [
      {
        // label: "Users Count",
        data: cData[i].data,
        backgroundColor: [
          "#66882D",
          "#ffea00",
          "#ff0000",
        ],
        borderColor: [
          "#66882D",
          "#ffea00",
          "#ff0000",
        ],
        borderWidth: [1, 1, 1]
      }
    ]
  };

  //options
  var options = {
    responsive: true,
    title: {
      display: true,
      position: "top",
      text: [cData[i].name,cData[i].jml+" Dokumen"],
      fontSize: 18,
      fontColor: "#111"
    },
    
    legend: {
      display: true,
      position: "right",
      labels: {
        fontColor: "#333",
        fontSize: 16
      }
    }
  };


  //create Pie Chart class object
  var chart1 = new Chart(ctx[i], {
    type: "pie",
    data: data,
    options: options
  });
}
});

$(document).ready(function(){
  $("#kobu,#prlt_tahun,#lppnpi,#kondisi,#fasilitas").select2();

  var table_peralatan = $('#prlt').DataTable({

    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": "<?php echo site_url('dashboard/peralatan_grid')?>",
      "type": "POST",
      "data": function ( data ) {
        data.kobu = $('#kobu').val();
        data.kondisi = $('#kondisi').val();
        data.lppnpi = $('#lppnpi').val();
        data.fasilitas = $('#fasilitas').val();
      }
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      {
        "targets": [ 0 ], //first column
        "orderable": false, //set not orderable
      },
      {
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },

    ],

  });

  $('#btn-filter').click(function(){ //button filter event click
    table_peralatan.ajax.reload();  //just reload table
    // window.location.href=window.location.href
    $.ajax({
      type: 'POST',
      url: "<?php echo site_url('dashboard/test');?>",
      data: {
        kobu : $('#kobu').val(),
        kondisi : $('#kondisi').val(),
        lppnpi : $('#lppnpi').val(),
        fasilitas : $('#fasilitas').val()
      },
      cache: false,
      success: function(data) {
        // alert(data);
        vData = JSON.parse(data);

        var vctx= [];
        for (var i = 0; i < vData.length; i++) {
          vctx[i] = $("#pie-chart"+i+"");
          
          // pie chart data
          var vdata = {
            labels: vData[i].label,
            datasets: [
              {
              // label: "Users Count",
                data: vData[i].data,
                backgroundColor: [
                  "#DEB887",
                  "#A9A9A9",
                  "#DC143C",
                  "#F4A460",
                  "#2E8B57",
                  "#1D7A46",
                  "#CDA776",
                ],
                borderColor: [
                  "#CDA776",
                  "#989898",
                  "#CB252B",
                  "#E39371",
                  "#1D7A46",
                  "#F4A460",
                  "#CDA776",
                ],
                borderWidth: [1, 1, 1, 1, 1,1,1]
              }
            ]
          };

          //options
          var voptions = {
            responsive: true,
            title: {
              display: true,
              position: "top",
              text: [vData[i].name,vData[i].jml+" units"],
              fontSize: 18,
              fontColor: "#111"
            },
          
            legend: {
              display: true,
              position: "right",
              labels: {
                fontColor: "#333",
                fontSize: 16
              }
            }
          };


          //create Pie Chart class object
          var chart2 = new Chart(vctx[i], {
            type: "pie",
            data: vdata,
            options: voptions
          });

          chart2.data = vdata;
          chart2.update();
          // vctx[i].clearRect(0, 0, 0,0);
        }
  
      }
    });
  });
});
</script>
--->

<script>
            
  $(function($) {

      $(".knob").knob({
          change : function (value) {
              //console.log("change : " + value);
              
          },
          release : function (value) {
              //console.log(this.$.attr('value'));
              console.log("release : " + value);
          },
          cancel : function () {
              console.log("cancel : ", this);
          },
          /*format : function (value) {
              return value + '%';
          },*/
          draw : function () {
            $(this.i).val(this.cv + '%');
            $(this.i).css('font-size', '25px');
              // "tron" case
              if(this.$.data('skin') == 'tron') {

                  this.cursorExt = 0.3;

                  var a = this.arc(this.cv)  // Arc
                      , pa                   // Previous arc
                      , r = 1;

                  this.g.lineWidth = this.lineWidth;

                  if (this.o.displayPrevious) {
                      pa = this.arc(this.v);
                      this.g.beginPath();
                      this.g.strokeStyle = this.pColor;
                      this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, pa.s, pa.e, pa.d);
                      this.g.stroke();
                  }

                  this.g.beginPath();
                  this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
                  this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, a.s, a.e, a.d);
                  this.g.stroke();

                  this.g.lineWidth = 2;
                  this.g.beginPath();
                  this.g.strokeStyle = this.o.fgColor;
                  this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                  this.g.stroke();

                  return false;
              }
          }
      });

    
      // Example of infinite knob, iPod click wheel
      
  });

</script>
<style>
    
    p{font-size: 30px; line-height: 30px}
    div.demo{text-align: center; width: 280px; float: left}
    div.demo > p{font-size: 20px}
</style>
  
<div class="row">
  <div class="col-md-12">
    <div class="box box-solid box-primary">
    <div class="box box-primary"> 
      <div class="row">
        <div class="col-md-2">
        </div>   
        <div class="col-md-4">
               
                <div class="demo" style="color:#222;">
                    <h4>Recent Expired Document Kapal
                      <input class="knob" data-width="200" data-displayPrevious=true data-fgColor="#233090" data-readonly="true" data-skin="tron" data-thickness=".2" value="<?=$persenDataArsipKapal?>">  
                    </h4>
                </div>
                
            </div>
      
        <div class="col-md-6">
            <div class="demo" style="color:#222;">
                <h4>Recent Expired Document Crew
                  <input class="knob" data-width="200" style="font: 20px;" data-displayPrevious=true data-fgColor="#233090" data-readonly="true" data-skin="tron" data-thickness=".2" value="<?=$persenDataArsipCrew?>">
                </h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
        

