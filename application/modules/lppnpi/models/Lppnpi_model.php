<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */

class Lppnpi_model extends CI_Model
{
  private $table = 'lppnpi';

  public function get_data(Array $param = NULL)
  {
    if ( ! is_null($param)) {
      if (array_key_exists('select', $param)) {
        $this->db->select($param['select']);
      }

      if (array_key_exists('order_by', $param)) {
        foreach ($param['order_by'] as $key => $value) {
          $this->db->order_by($key, $value);
        }
      }

      if (array_key_exists('distinct', $param)) {
        $this->db->distinct();
      }

      if (array_key_exists('where', $param)) {
        if (is_array($param['where'])) {
          foreach ($param['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($param['where']);
        }
      }

      if (array_key_exists('or_where', $param)) {
        if (is_array($param['or_where'])) {
          foreach ($param['or_where'] as $key => $value) {
            $this->db->or_where($key, $value);
          }
        }
        else {
          $this->db->or_where($param['or_where']);
        }
      }

      if (array_key_exists('like', $param)) {
        if (is_array($param['like'])) {
          foreach ($param['like'] as $key => $value) {
            $this->db->like($key, $value);
          }
        }
        else {
          $this->db->like($param['like']);
        }
      }

      if (array_key_exists('or_like', $param)) {
        if (is_array($param['or_like'])) {
          foreach ($param['or_like'] as $key => $value) {
            $this->db->or_like($key, $value);
          }
        }
        else {
          $this->db->or_like($param['or_like']);
        }
      }
    }

    $this->db->from($this->table);

    return $this->db->get();
  }

  public function insert($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->affected_rows();
  }

  public function update(Array $param = NULL)
  {
    if (array_key_exists('where', $param)) {
      if (is_array($param['where'])) {
        foreach ($param['where'] as $key => $value) {
          $this->db->where($key, $value);
        }
      }
      else {
        $this->db->where($param['where']);
      }
    }

    $this->db->update($this->table, $param['data']);
    return $this->db->affected_rows();
  }

  public function delete(Array $param = NULL)
  {
    if ( ! is_null($param)) {
      if (array_key_exists('where', $param)) {
        if (is_array($param['where'])) {
          foreach ($param['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($param['where']);
        }
      }
    }

    $this->db->delete($this->table);
  }
}