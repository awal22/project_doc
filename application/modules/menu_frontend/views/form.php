<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->id : '';
    $navigasi = isset($row) ? $row->navigasi_menu : '';
    $category = isset($row) ? $row->id_kategori : '';
    $content = isset($row) ? $row->id_konten : '';
    $judul_link = isset($row) ? $row->judul_link : '';
    $menu_url = isset($row) ? $row->link : '';
    $menu_order = isset($row) ? $row->order_menu : '';
    if($category!==0 && $content==0 && $judul_link==""){
    	$active_menu = 1;	
    }

    if($content!==0 && $category==0 && $judul_link==""){
    	$active_menu = 2;
    }

    if($judul_link!==""){
    	$active_menu = 3;
    }

    $btn_val = isset($row) ? 'update' : 'save';

    $arr_active = array(3 => 'External Link', 2 => 'Konten', 1 => 'Kategori');
    $arr_navigasi = array(2 => 'Menu Bawah', 1 => 'Menu Atas');

    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <div class="row">
        <div class="col-md-8">
           <div class="form-group">
            <label for="active">Order Menu</label>
            <input type="text" name="order_menu" id="order_menu" value="<?php echo $menu_order; ?>" class="form-control" placeholder="order menu" autofocus="autofocus" autocomplete="off">
          </div>

          <div class="form-group">
            <label for="active">Navigasi Menu</label>
            <select name="navigasi" id="navigasi" class="form-control" required="required">
              <option value="">Pilih</option>
              <?php
              foreach ($arr_navigasi as $kNavigasi => $vNavigasi) {
                $sel_navigasi = $kNavigasi == $navigasi ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $kNavigasi; ?>" <?php echo $sel_navigasi; ?>><?php echo $vNavigasi; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-8">
          <div class="form-group">
            <label for="active">Pilih Menu</label>
            <select name="pilih_menu" id="pilih_menu" class="form-control" required="required">
              <option value="">Pilih</option>
              <?php
              foreach ($arr_active as $kActive => $vActive) {
                $sel_active = $kActive == $active_menu ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $kActive; ?>" <?php echo $sel_active; ?>><?php echo $vActive; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-8" id="kategori">
          <div class="form-group">
            <label for="active">Category Menu</label>
            <select name="kategori_menu" id="kategori_menu" class="form-control">
              <option value="">Pilih</option>
              <?php
              foreach ($arr_category as $kCategory) {
                $sel_category = $kCategory->id_kategori == $category ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $kCategory->id_kategori; ?>" <?php echo $sel_category; ?>><?php echo $kCategory->nama_kategori; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-8" id="konten">
          <div class="form-group">
            <label for="active">Content Menu</label>
            <select name="konten_menu" id="konten_menu" class="form-control">
              <option value="">Pilih</option>
              <?php
              foreach ($arr_content as $kContent) {
                $sel_content = $kContent->id_berita== $content ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $kContent->id_berita; ?>" <?php echo $sel_content; ?>><?php echo $kContent->judul; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>
        <div class="col-md-8" id="judul_link">
          <div class="form-group">
            <label for="menu_title">Judul Link</label>
            <input type="text" name="judul_link" id="judul_link" value="<?php echo $judul_link; ?>" class="form-control" placeholder="Title" autofocus="autofocus" autocomplete="off">
          </div>
        </div>
        <div class="col-md-8" id="external_link">
          <div class="form-group">
            <label for="menu_url">External Link</label>
            <input type="text" name="menu_url" id="menu_url" value="<?php echo $menu_url; ?>" class="form-control" placeholder="URL" autocomplete="off">
          </div>
        </div>
      </div>

      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>

<script>
  $(document).ready(function(){
    $("#menu_parent").select2();
    $("#active").select2();
  
    //check all
    $("#chk-all-read").click(function () {
      $(".view").prop('checked', $(this).prop('checked'));
    });
    $("#chk-all-create").click(function () {
      $(".insert").prop('checked', $(this).prop('checked'));
    });
    $("#chk-all-update").click(function () {
      $(".update").prop('checked', $(this).prop('checked'));
    });
    $("#chk-all-delete").click(function () {
      $(".delete").prop('checked', $(this).prop('checked'));
    });
  });
</script>
<script type="text/javascript">
  function load() {
    $('#kategori').hide();
    $('#konten').hide();
    $('#external_link').hide();
    $('#judul_link').hide();

    var pilihan = $("#pilih_menu").val();
    
    if(pilihan==1){
      $('#kategori').show();
    }else{
       $('#kategori').hide();
    }

    if(pilihan==2){
      $('#konten').show();
    }else{
      $('#konten').hide();
    }

    if(pilihan==3){
      $('#external_link').show();
      $('#judul_link').show();
    }else{
      $('#external_link').hide();
      $('#judul_link').hide();
    }
    
  }

  window.onload = load;
  $('#pilih_menu').click(function(){
    var pilihan = $("#pilih_menu").val();
    
    if(pilihan==1){
      $('#kategori').show();
    }else{
       $('#kategori').hide();
    }

    if(pilihan==2){
      $('#konten').show();
    }else{
      $('#konten').hide();
    }

    if(pilihan==3){
      $('#external_link').show();
      $('#judul_link').show();
    }else{
      $('#external_link').hide();
      $('#judul_link').hide();
    }

  });
</script>




