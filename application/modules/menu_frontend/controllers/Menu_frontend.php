<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Menu_frontend extends MX_Controller
{
  private $modul = 'Menu_frontend';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));

    $this->load->model('menu_frontend/Menu_frontend_model', 'frontend');
    $this->load->model('menu_frontend/V_menu_frontend_model', 'vMenuFrontend');
    $this->load->model('kategori_konten/Kategori_konten_model', 'kategori');
    $this->load->model('konten_berita/konten_model', 'konten');

  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));
    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'index', $data);
  }

  public function add()
  {
    $this->acl->check_create(strtolower($this->modul));

    $category = $this->kategori->get_data(NULL);
    $data['arr_category'] = $category->result();

    $content = $this->konten->get_data(NULL);
    $data['arr_content'] = $content->result();

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'Add';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'form', $data);
  }

  public function edit(int $id = NULL)
  {
    $this->acl->check_update(strtolower($this->modul));

    if ( ! is_null($id)) {
      $param['where']['id'] = $id;
      $row = $this->frontend->get_data($param);

      if ($row->num_rows() > 0) {
        $data['row'] = $row->row();
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak ditemukan";
        $this->session->set_flashdata($flashdata);
  
        redirect(strtolower($this->modul));
      }

      $category = $this->kategori->get_data(NULL);
      $data['arr_category'] = $category->result();
      
      $content = $this->konten->get_data(NULL);
      $data['arr_content'] = $content->result();
  
      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Edit';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'form', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
			$this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function simpan()
  {
    $id = $this->input->post('id', TRUE);
    $id_kategori = $this->input->post('kategori_menu', TRUE);
    $id_konten = $this->input->post('konten_menu', TRUE);
    $judul_link = $this->input->post('judul_link', TRUE);
    $menu_url = $this->input->post('menu_url', TRUE);
    $navigasi = $this->input->post('navigasi', TRUE);
    $menu_order = $this->input->post('order_menu', TRUE);
    $save = $this->input->post('save', TRUE);

    switch ($save) {
      case 'save':
        $data['id_kategori'] = $id_kategori;
        $data['id_konten'] = $id_konten;
        $data['judul_link'] = $judul_link;
        $data['link'] = $menu_url;
        $data['navigasi_menu'] = $navigasi;
        $data['order_menu'] = $menu_order;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->session->userdata('userId');

        $insert = $this->frontend->insert($data);

        if ($insert == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;

      case 'update':
        $param['data']['id_kategori'] = $id_kategori;
        $param['data']['id_konten'] = $id_konten;
        $param['data']['judul_link'] = $judul_link;
        $param['data']['link'] = $menu_url;
        $param['data']['order_menu'] = $menu_order;
        $param['data']['updated_at'] = date('Y-m-d H:i:s');
        $param['data']['updated_by'] = $this->session->userdata('userId');
        $param['where']['id'] = $id;

        $update = $this->frontend->update($param);

        if ($update == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

  public function hapus(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));

    $param['where']['id'] = $id;
    $this->frontend->delete($param);

    $flashdata["alert_class"] = "success";
    $flashdata["alert_text"] = "Data berhasil dihapus";
    $this->session->set_flashdata($flashdata);

    redirect(strtolower($this->modul));
  }

  public function datagrid()
  {
    $list = $this->vMenuFrontend->get_datatables($this->input->post(NULL, TRUE));
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rw->order_menu;
      $row[] = $rw->nama_kategori;
      $row[] = $rw->judul;
      $row[] = $rw->judul_link;
      $row[] = $rw->link;
      $row[] = "<div class=\"text-center\">
              <a href=\"".site_url(strtolower($this->modul)."/edit/".$rw->id)."\" class=\"btn btn-primary btn-flat btn-xs\" title=\"Edit\"><i class=\"fa fa-edit\"></i></a>
              <button type=\"button\" title=\"Delete\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->id."','".$rw->id."');\"><i class=\"fa fa-trash\"></i></button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vMenuFrontend->count_all(),
      "recordsFiltered" => $this->vMenuFrontend->count_filtered($this->input->post(NULL, TRUE)),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }
}