<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Kalibrasi extends MX_Controller
{
  private $modul = 'Kalibrasi';
  private $table = 'kalibrasi';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));

    $this->load->model('Master_model', 'master');
    $this->load->model('kalibrasi/Kalibrasi_model', 'kalibrasi');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'index', $data);
  }

  public function add()
  {
    $this->acl->check_create(strtolower($this->modul));

    $pPeralatan['table'] = 'peralatan';
    $pPeralatan['select'] = 'id, nama';
    $peralatan = $this->master->get_data($pPeralatan);
    $data['peralatan'] = $peralatan->result();

    $data['arrJenis'] = $this->kalibrasi->jenis_kalibrasi();

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'Add';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'form', $data);
  }

  public function edit(int $id = NULL)
  {
    $this->acl->check_update(strtolower($this->modul));

    if ( ! is_null($id)) {
      $param['table'] = 'kalibrasi';
      $param['where']['id'] = $id;
      $row = $this->master->get_data($param);

      if ($row->num_rows() > 0) {
        $data['row'] = $row->row();
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak ditemukan";
        $this->session->set_flashdata($flashdata);
  
        redirect(strtolower($this->modul));
      }

      $pPeralatan['table'] = 'peralatan';
      $pPeralatan['select'] = 'id, nama';
      $peralatan = $this->master->get_data($pPeralatan);
      $data['peralatan'] = $peralatan->result();
  
      $data['jenis'] = $this->kalibrasi->jenis_kalibrasi();
      
      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Edit';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'form', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
			$this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function simpan()
  {
    $id = $this->input->post('id', TRUE);
    $jenis = $this->input->post('jenis', TRUE);
    $peralatan_id = $this->input->post('peralatan_id', TRUE);
    $no_sertifikat = $this->input->post('no_sertifikat', TRUE);
    $tgl_terbit = date('Y-m-d', strtotime($this->input->post('tgl_terbit', TRUE)));
    $bukti = isset($_FILES['bukti']) ? $_FILES['bukti']['name'] : '';
    $save = $this->input->post('save', TRUE);

    $image_name = url_title($no_sertifikat, 'dash', TRUE);
    $file_ext = pathinfo($bukti, PATHINFO_EXTENSION);

    $param['table'] = $this->table;
    $param['data']['jenis'] = $jenis;
    $param['data']['peralatan_id'] = $peralatan_id;
    $param['data']['no_sertifikat'] = $no_sertifikat;
    $param['data']['tgl_terbit'] = $tgl_terbit;
    $file_ext == "" ? "" : $data['bukti'] = $image_name.".".$file_ext;
    $param['affected_rows'] = TRUE;

    switch ($save) {
      case 'save':

        $param['data']['created_at'] = date('Y-m-d H:i:s');
        $param['data']['created_by'] = $this->session->userdata('userId');
        
        // upload bukti
        $config['file_name'] = $image_name;
        $config['upload_path'] = './bukti/kalibrasi/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        
        $this->load->library('upload', $config);

        if ($file_ext == "") {
          $insert = $this->master->insert($param);

          if ($insert == 1) {
            $flashdata["alert_class"] = "success";
            $flashdata["alert_text"] = "Data berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
          else {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
        }
        else {
          if ( ! $this->upload->do_upload('bukti'))
          {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
          }
          else
          {
            $insert = $this->master->insert($param);
  
            if ($insert == 1) {
              $flashdata["alert_class"] = "success";
              $flashdata["alert_text"] = "Data berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
            else {
              $flashdata["alert_class"] = "warning";
              $flashdata["alert_text"] = "Data tidak berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
          }
        }
        break;

      case 'update':
        $param['data']['updated_at'] = date('Y-m-d H:i:s');
        $param['data']['updated_by'] = $this->session->userdata('userId');
        $param['where']['id'] = $id;

        if ($file_ext == "") {
          $update = $this->master->update($param);
    
          if ($update == 1) {
            $flashdata["alert_class"] = "success";
            $flashdata["alert_text"] = "Data berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
          else {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
        }
        else {
          $param['where']['id'] = $id;
          $row = $this->master->get_data($param)->row();
          if ($row->bukti != '') {
            unlink('bukti/kalibrasi/'.$row->bukti);
          }

          // upload bukti
          $config['file_name'] = $image_name;
          $config['upload_path'] = './bukti/kalibrasi/';
          $config['allowed_types'] = 'gif|jpg|png|jpeg';
          $config['max_size'] = 2048;
          $config['max_width'] = 1024;
          $config['max_height'] = 768;
          
          $this->load->library('upload', $config);
          
          if ( ! $this->upload->do_upload('bukti'))
          {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
          }
          else
          {
            $update = $this->master->update($param);
    
            if ($update == 1) {
              $flashdata["alert_class"] = "success";
              $flashdata["alert_text"] = "Data berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
            else {
              $flashdata["alert_class"] = "warning";
              $flashdata["alert_text"] = "Data tidak berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
          }
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

  public function hapus(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));
    
    $param['table'] = $this->table;
    $param['where']['id'] = $id;

    $row = $this->master->get_data($param)->row();
    unlink('bukti/kalibrasi/'.$row->bukti);

    $this->master->delete($param);

    $flashdata["alert_class"] = "success";
    $flashdata["alert_text"] = "Data berhasil dihapus";
    $this->session->set_flashdata($flashdata);

    redirect(strtolower($this->modul));
  }

  public function datagrid()
  {
    $param['table'] = "v_kalibrasi";
    $param['column_order'] = array(null, 'jenis', 'nama_peralatan', 'no_sertifikat', 'tgl_terbit');
    $param['column_search'] = array('jenis', 'nama_peralatan', 'no_sertifikat', 'tgl_terbit');
    $param['order'] = array('id' => 'desc');
    $param['post'] = $this->input->post(NULL, TRUE);
    
    $list = $this->master->get_datatables($param);
    $data = array();
    $no = $this->input->post('start', TRUE);
    foreach ($list as $rw) {
      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rw->jenis;
      $row[] = $rw->nama_peralatan;
      $row[] = $rw->no_sertifikat;
      $row[] = date('d-m-Y', strtotime($rw->tgl_terbit));
      $row[] = "<div class=\"text-center\">
              <a href=\"".site_url(strtolower($this->modul)."/edit/".$rw->id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>
              <button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->id."','".$rw->no_sertifikat."');\">Delete</button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->master->count_all($this->table),
      "recordsFiltered" => $this->master->count_filtered($param),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }
}