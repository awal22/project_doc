<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */

class Kalibrasi_model extends CI_Model
{
  public function jenis_kalibrasi()
  {
    return array('FlightCom', 'Periodik', 'Spesial');
  }
}