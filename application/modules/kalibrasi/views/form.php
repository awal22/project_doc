<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->id : '';
    $jenis = isset($row) ? $row->jenis : '';
    $peralatan_id = isset($row) ? $row->peralatan_id : '';
    $no_sertifikat = isset($row) ? $row->no_sertifikat : '';
    $tgl_terbit = isset($row) ? $row->tgl_terbit : '';
    $bukti = isset($row) ? $row->bukti : '';
    $btn_val = isset($row) ? 'update' : 'save';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $id; ?>">

      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="jenis">Jenis</label>
            <select name="jenis" id="jenis" class="form-control">
              <option value="">Pilih Jenis</option>
              <?php
              foreach ($arrJenis as $vJenis) {
                $selJenis = $vJenis == $jenis ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vJenis; ?>" <?php echo $selJenis; ?>><?php echo $vJenis; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="peralatan_id">Peralatan</label>
            <select name="peralatan_id" id="peralatan_id" class="form-control">
              <option value="">Pilih Peralatan</option>
              <?php
              foreach ($peralatan as $vPeralatan) {
                $selPeralatan = $peralatan_id == $vPeralatan->id ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vPeralatan->id; ?>" <?php echo $selPeralatan; ?>><?php echo $vPeralatan->nama; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="no_sertifikat">NO Sertifikat</label>
            <input type="text" name="no_sertifikat" id="no_sertifikat" value="<?php echo $no_sertifikat; ?>" class="form-control" placeholder="NO Sertifikat" autofocus="autofocus" autocomplete="off">
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="tgl_terbit">Tanggal Terbit</label>
            <input type="text" name="tgl_terbit" id="tgl_terbit" value="<?php echo $tgl_terbit; ?>" class="form-control" placeholder="Tanggal Terbit" autofocus="autofocus" autocomplete="off">
          </div>
        </div>
      </div>

      <div class="form-group">
        <label for="bukti">Bukti</label>
        <input type="file" name="bukti" id="bukti">
      </div>

      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>

<script>
$(document).ready(function () {
  $("#tgl_terbit").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    orientation: "bottom auto",
    todayHighlight: true,
    todayBtn: "linked"
  });
});
</script>