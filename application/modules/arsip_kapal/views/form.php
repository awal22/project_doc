<?php
  if ( ! is_null($this->session->flashdata('alert_class')))
  {
    ?>
    <div id="alert" class="alert alert-<?php echo $this->session->flashdata('alert_class'); ?>" role="alert"><?php echo $this->session->flashdata('alert_text'); ?></div>
    <?php
  }
?>
<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->arsip_kapal_id : '';
    $nomor = isset($row) ? $row->arsip_kapal_nomor : '';
    $nama_kapal = isset($row) ? $row->arsip_kapal_nama : '';
    $deskripsi = isset($row) ? $row->arsip_kapal_deskripsi : '';
    $expired = isset($row) ? $row->arsip_kapal_expired : '';
    $file = isset($row) ? $row->arsip_kapal_file : '';

    $btn_val = isset($row) ? 'update' : 'save';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
   
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="no_amandemen">Nomor Arsip</label>
            <input type="text" name="nomor" id="nomor" value="<?php echo $nomor; ?>" class="form-control" placeholder="Nomor Arsip" autofocus="autofocus" autocomplete="off">
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="nama">Nama Kapal</label>
            <select name="nama" id="nama" class="form-control">
              <option value="">Pilih Nama Kapal</option>
              <?php
              foreach ($kapal as $vKapal) {
                $NamaKapal = $nama_kapal == $vKapal->kapal_id ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vKapal->kapal_id; ?>" <?php echo $NamaKapal; ?>><?php echo $vKapal->kapal_nama; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="tanggal_expired">Tanggal Expired</label>
            <input type="text" name="expired" id="expired" value="<?php echo $expired; ?>" class="form-control" placeholder="Tanggal Expired" autofocus="autofocus" autocomplete="off">
          </div>
        </div>
      </div>

      <div class="form-group">
        <label for="deskripsi">Deskripsi Arsip</label>
        <textarea name="deskripsi" id="deskripsi" cols="30" rows="10" placeholder="Deskripsi Arsip..." class="form-control"><?php echo $deskripsi; ?></textarea>
      </div>

      <div class="form-group">
        <label for="bukti">Arsip File</label>
        <input type="file" name="arsip_file" id="arsip_file" value="<?php echo $file; ?>">
        <label>Arsip File Aktif Saat ini : <?php echo "<a href='".base_url()."arsip_file/$file' target='_blank'>$file</a>" ?>
      </div>

      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

    </div>
  </div>

  <script>
  $(document).ready(function () {
    $("#nama").select2();
    $("#expired").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      orientation: "bottom auto",
      todayHighlight: true,
      todayBtn: "linked"
    });
  });
  </script>