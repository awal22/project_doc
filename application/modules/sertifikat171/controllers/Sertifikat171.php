<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Sertifikat171 extends MX_Controller
{
  private $modul = 'Sertifikat171';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));

    $this->load->model('kobu/kobu_model', 'kobu');
    $this->load->model('kacab_pembina/kacab_pembina_model', 'kacabPembina');
    $this->load->model('lppnpi/lppnpi_model', 'lppnpi');
    $this->load->model('sertifikat171/Sertifikat171_model', 'sertifikat171');
    $this->load->model('sertifikat171/V_sertifikat171_model', 'vSertifikat171');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'index', $data);
  }

  public function add()
  {
    $this->acl->check_create(strtolower($this->modul));

    $kobu = $this->kobu->get_data();
    $data['kobu'] = $kobu->result();

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'Add';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'form', $data);
  }

  public function edit(int $id = NULL)
  {
    $this->acl->check_update(strtolower($this->modul));

    if ( ! is_null($id)) {
      $param['where']['id'] = $id;
      $row = $this->vSertifikat171->get_data($param);

      if ($row->num_rows() > 0) {
        $data['row'] = $row->row();
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak ditemukan";
        $this->session->set_flashdata($flashdata);
  
        redirect(strtolower($this->modul));
      }

      $kobu = $this->kobu->get_data();
      $data['kobu'] = $kobu->result();
  
      $kacabPembina = $this->kacabPembina->get_data();
      $data['kacabPembina'] = $kacabPembina->result();
  
      $lppnpi = $this->lppnpi->get_data();
      $data['lppnpi'] = $lppnpi->result();
  
      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Edit';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'form', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
			$this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function simpan()
  {
    $id = $this->input->post('id', TRUE);
    $no_amandemen = $this->input->post('no_amandemen', TRUE);
    $no = $this->input->post('no', TRUE);
    $lppnpi_id = $this->input->post('lppnpi_id', TRUE);
    $tanggal_terbit = date('Y-m-d', strtotime($this->input->post('tanggal_terbit', TRUE)));
    $temuan = $this->input->post('temuan', TRUE);
    $bukti = isset($_FILES['bukti']) ? $_FILES['bukti']['name'] : '';
    $save = $this->input->post('save', TRUE);

    $image_name = url_title($no_amandemen, 'dash', TRUE);
    $file_ext = pathinfo($bukti, PATHINFO_EXTENSION);

    switch ($save) {
      case 'save':
        $data['no_amandemen'] = $no_amandemen;
        $data['no'] = $no;
        $data['lppnpi_id'] = $lppnpi_id;
        $data['tanggal_terbit'] = $tanggal_terbit;
        $data['temuan'] = $temuan;
        $file_ext == "" ? "" : $data['bukti'] = $image_name.".".$file_ext;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->session->userdata('userId');
        
        // upload bukti
        $config['file_name'] = $image_name;
        $config['upload_path'] = './bukti/sertifikat171/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        
        $this->load->library('upload', $config);

        if ($file_ext == "") {
          $insert = $this->sertifikat171->insert($data);

          if ($insert == 1) {
            $flashdata["alert_class"] = "success";
            $flashdata["alert_text"] = "Data berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
          else {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
        }
        else {
          if ( ! $this->upload->do_upload('bukti'))
          {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
          }
          else
          {
            $insert = $this->sertifikat171->insert($data);
  
            if ($insert == 1) {
              $flashdata["alert_class"] = "success";
              $flashdata["alert_text"] = "Data berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
            else {
              $flashdata["alert_class"] = "warning";
              $flashdata["alert_text"] = "Data tidak berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
          }
        }
        break;

      case 'update':
        $param['data']['no_amandemen'] = $no_amandemen;
        $param['data']['no'] = $no;
        $param['data']['lppnpi_id'] = $lppnpi_id;
        $param['data']['tanggal_terbit'] = $tanggal_terbit;
        $param['data']['temuan'] = $temuan;
        $file_ext == "" ? "" : $param['data']['bukti'] = $image_name.".".$file_ext;
        $param['data']['updated_at'] = date('Y-m-d H:i:s');
        $param['data']['updated_by'] = $this->session->userdata('userId');
        $param['where']['id'] = $id;

        if ($file_ext == "") {
          $update = $this->sertifikat171->update($param);
    
          if ($update == 1) {
            $flashdata["alert_class"] = "success";
            $flashdata["alert_text"] = "Data berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
          else {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
        }
        else {
          $param['where']['id'] = $id;
          $row = $this->sertifikat171->get_data($param)->row();
          if ($row->bukti != '') {
            unlink('bukti/sertifikat171/'.$row->bukti);
          }

          // upload bukti
          $config['file_name'] = $image_name;
          $config['upload_path'] = './bukti/sertifikat171/';
          $config['allowed_types'] = 'gif|jpg|png|jpeg';
          $config['max_size'] = 2048;
          $config['max_width'] = 1024;
          $config['max_height'] = 768;
          
          $this->load->library('upload', $config);
          
          if ( ! $this->upload->do_upload('bukti'))
          {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
          }
          else
          {
            $update = $this->sertifikat171->update($param);
    
            if ($update == 1) {
              $flashdata["alert_class"] = "success";
              $flashdata["alert_text"] = "Data berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
            else {
              $flashdata["alert_class"] = "warning";
              $flashdata["alert_text"] = "Data tidak berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
          }
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

  public function hapus(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));
    
    $param['where']['id'] = $id;

    $row = $this->sertifikat171->get_data($param)->row();
    unlink('bukti/sertifikat171/'.$row->bukti);

    $this->sertifikat171->delete($param);

    $flashdata["alert_class"] = "success";
    $flashdata["alert_text"] = "Data berhasil dihapus";
    $this->session->set_flashdata($flashdata);

    redirect(strtolower($this->modul));
  }

  public function datagrid()
  {
    $list = $this->vSertifikat171->get_datatables($this->input->post(NULL, TRUE));
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rw->no_amandemen;
      $row[] = $rw->no;
      $row[] = $rw->tanggal_terbit;
      $row[] = $rw->nama_lppnpi;
      $row[] = $rw->nama_kacab_pembina;
      $row[] = $rw->nama_kobu;
      $row[] = "<div class=\"text-center\">
              <a href=\"".site_url(strtolower($this->modul)."/edit/".$rw->id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>
              <button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->id."','".$rw->no_amandemen."');\">Delete</button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vSertifikat171->count_all(),
      "recordsFiltered" => $this->vSertifikat171->count_filtered($this->input->post(NULL, TRUE)),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }
}