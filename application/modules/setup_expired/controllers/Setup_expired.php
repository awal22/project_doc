<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Awaluddin <awaluddinbinusep@gmail.com>
 */
class Setup_expired extends MX_Controller
{
  private $modul = 'Setup_expired';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));
    $this->load->model('setup_expired/setup_expired_model', 'expired_setup');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);
    $id = "setup_expired_id";
    $param['order_by']['setup_expired_id'] = $id;
    $row = $this->expired_setup->get_data($param);
    if ($row->num_rows() > 0) {
      $data['row'] = $row->row();
    }

    $this->template->load('templates/admin', 'form', $data);
  }

  public function simpan()
  {
    
    $id = $this->input->post('id', TRUE);
    $hari = $this->input->post('hari', TRUE);
    
    $save = $this->input->post('save', TRUE);

    switch ($save) {
      case 'save':
        $data['setup_expired_hari'] = $hari;
        $data['setup_expired_created_at'] = date('Y-m-d H:i:s');
        $data['setup_expired_created_by'] = $this->session->userdata('userId');
        $insert = $this->expired_setup->insert($data);

        if ($insert == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        else 
        {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;

      case 'update':
    
        $param['data']['setup_expired_hari'] = $hari;
        
        $param['data']['setup_expired_update_at'] = date('Y-m-d H:i:s');
        $param['data']['setup_expired_update_by'] = $this->session->userdata('userId');
        $param['where']['setup_expired_id'] = $id;
        $update = $this->expired_setup->update($param);

        if ($update == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_text"] = "Data berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));   
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_text"] = "Data tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
    
          redirect(strtolower($this->modul));
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

}