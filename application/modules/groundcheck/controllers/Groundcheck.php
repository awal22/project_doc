<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Groundcheck extends MX_Controller
{
  private $modul = 'Groundcheck';

  function __construct()
  {
    parent::__construct();
    $this->acl->check_auth();
    $this->template->set('title', humanize($this->modul));

    $this->load->model('groundcheck/Groundcheck_model', 'GroundCheck');
    $this->load->model('groundcheck/V_groundcheck_model', 'vGroundCheck');
    $this->load->model('peralatan/V_peralatan_model', 'vPeralatan');
    $this->load->model('provinsi/Provinsi_model', 'provinsi');
    $this->load->model('lokasi_alat/Lokasi_alat_model', 'lokasiAlat');
  }

  public function index()
  {
    $this->acl->check_read(strtolower($this->modul));

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'List';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'index', $data);
  }

  public function add()
  {
    $this->acl->check_create(strtolower($this->modul));

    $provinsi = $this->provinsi->get_data();
    $data['provinsi'] = $provinsi->result();

    $arr_jenis_gc = $this->GroundCheck->jenis_gc();
    $data['arr_jenis_gc'] = $arr_jenis_gc;

    $data['page_header'] = humanize($this->modul);
    $data['opt_desc'] = 'Add';
    $data['modul'] = strtolower($this->modul);

    $this->template->load('templates/admin', 'form', $data);
  }

  public function edit(int $id = NULL)
  {
    $this->acl->check_update(strtolower($this->modul));

    if ( ! is_null($id)) {
      $param['where']['id'] = $id;
      $row = $this->vGroundCheck->get_data($param);

      if ($row->num_rows() > 0) {
        $data['row'] = $row->row();
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Data tidak ditemukan";
        $this->session->set_flashdata($flashdata);
  
        redirect(strtolower($this->modul));
      }
  
      $provinsi = $this->provinsi->get_data();
      $data['provinsi'] = $provinsi->result();

      $lokasi_alat = $this->lokasi_alat->get_data();
      $data['lokasi_alat'] = $lokasi_alat->result();

      $data['page_header'] = humanize($this->modul);
      $data['opt_desc'] = 'Edit';
      $data['modul'] = strtolower($this->modul);
  
      $this->template->load('templates/admin', 'form', $data);        
    }
    else {
      $flashdata["alert_class"] = "warning";
			$flashdata["alert_text"] = "Anda harus pilih 1 (satu) data";
			$this->session->set_flashdata($flashdata);

      redirect(strtolower($this->modul));
    }
  }

  public function simpan()
  {
    $id = $this->input->post('id', TRUE);
    $peralatan_id = $this->input->post('peralatan_id', TRUE);
    $tgl_gc = $this->input->post('tgl_gc', TRUE);
    $jenis_gc = $this->input->post('jenis_gc', TRUE);
    $tgl_expire = date('Y-m-d', strtotime($this->input->post('tgl_expire', TRUE)));
    $bukti = isset($_FILES['bukti']) ? $_FILES['bukti']['name'] : '';
    $save = $this->input->post('save', TRUE);

    $image_name = url_title($tgl_gc."_".$jenis_gc."_".$peralatan_id, 'dash', TRUE);
    $file_ext = pathinfo($bukti, PATHINFO_EXTENSION);

    switch ($save) {
      case 'save':
        $data['peralatan_id'] = $peralatan_id;
        $data['tgl_gc'] = $tgl_gc;
        $data['jenis_gc'] = $jenis_gc;
        $data['tgl_expire'] = $tgl_expire;
        $file_ext == "" ? "" : $data['bukti'] = $image_name.".".$file_ext;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->session->userdata('userId');
        
        // upload bukti
        $config['file_name'] = $image_name;
        $config['upload_path'] = './bukti/groundcheck/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2048;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        
        $this->load->library('upload', $config);

        if ($file_ext == "") {
          $insert = $this->Isr->insert($data);

          if ($insert == 1) {
            $flashdata["alert_class"] = "success";
            $flashdata["alert_text"] = "Data berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
          else {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
        }
        else {
          if ( ! $this->upload->do_upload('bukti'))
          {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
          }
          else
          {
            $insert = $this->GroundCheck->insert($data);
  
            if ($insert == 1) {
              $flashdata["alert_class"] = "success";
              $flashdata["alert_text"] = "Data berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
            else {
              $flashdata["alert_class"] = "warning";
              $flashdata["alert_text"] = "Data tidak berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
          }
        }
        break;

      case 'update':
        $param['data']['peralatan_id'] = $peralatan_id;
        $param['data']['tgl_gc'] = $tgl_gc;
        $param['data']['jenis_gc'] = $jenis_gc;
        $param['data']['tgl_expire'] = $tgl_expire;
        $file_ext == "" ? "" : $param['data']['bukti'] = $image_name.".".$file_ext;
        $param['data']['updated_at'] = date('Y-m-d H:i:s');
        $param['data']['updated_by'] = $this->session->userdata('userId');
        $param['where']['id'] = $id;

        if ($file_ext == "") {
          $update = $this->GroundCheck->update($param);
    
          if ($update == 1) {
            $flashdata["alert_class"] = "success";
            $flashdata["alert_text"] = "Data berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
          else {
            $flashdata["alert_class"] = "warning";
            $flashdata["alert_text"] = "Data tidak berhasil disimpan";
            $this->session->set_flashdata($flashdata);
      
            redirect(strtolower($this->modul));
          }
        }
        else {
          $param['where']['id'] = $id;
          $row = $this->GroundCheck->get_data($param)->row();
          if ($row->bukti != '') {
            unlink('bukti/groundcheck/'.$row->bukti);
          }

          // upload bukti
          $config['file_name'] = $image_name;
          $config['upload_path'] = './bukti/groundcheck/';
          $config['allowed_types'] = 'gif|jpg|png|jpeg';
          $config['max_size'] = 2048;
          $config['max_width'] = 1024;
          $config['max_height'] = 768;
          
          $this->load->library('upload', $config);
          
          if ( ! $this->upload->do_upload('bukti'))
          {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
          }
          else
          {
            $update = $this->Isr->update($param);
    
            if ($update == 1) {
              $flashdata["alert_class"] = "success";
              $flashdata["alert_text"] = "Data berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
            else {
              $flashdata["alert_class"] = "warning";
              $flashdata["alert_text"] = "Data tidak berhasil disimpan";
              $this->session->set_flashdata($flashdata);
        
              redirect(strtolower($this->modul));
            }
          }
        }
        break;
      
      default:
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_text"] = "Anda tidak boleh mengakses langsung halaman ini";
        $this->session->set_flashdata($flashdata);

        redirect(strtolower($this->modul));
        break;
    }
  }

  public function hapus(int $id = NULL)
  {
    $this->acl->check_delete(strtolower($this->modul));
    
    $param['where']['id'] = $id;

    $row = $this->GroundCheck->get_data($param)->row();
    unlink('bukti/groundcheck/'.$row->bukti);

    $this->Isr->delete($param);

    $flashdata["alert_class"] = "success";
    $flashdata["alert_text"] = "Data berhasil dihapus";
    $this->session->set_flashdata($flashdata);

    redirect(strtolower($this->modul));
  }

  public function datagrid()
  {
    $list = $this->vGroundCheck->get_datatables($this->input->post(NULL, TRUE));
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rw->tgl_gc;
      $row[] = $rw->jenis_gc;
      $row[] = $rw->nama_provinsi;
      $row[] = $rw->nama_peralatan;
      $row[] = $rw->tgl_expire;
      $row[] = "<div class=\"text-center\">
              <a href=\"".site_url(strtolower($this->modul)."/edit/".$rw->id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>
              <button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->id."','".$rw->nama_peralatan."');\">Delete</button></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vGroundCheck->count_all(),
      "recordsFiltered" => $this->vGroundCheck->count_filtered($this->input->post(NULL, TRUE)),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }
}