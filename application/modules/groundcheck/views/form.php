<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Form <?php echo $page_header; ?></h3>
    <div class="box-tools pull-right">
      <span class="label label-primary">
        <a href="<?php echo site_url($modul); ?>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-list"></i> List</a>
      </span>
    </div>
  </div>

  <div class="box-body">

    <?php
    $id = isset($row) ? $row->id : '';
    $tgl_gc = isset($row) ? $row->tgl_gc : '';
    $jenis_gc = isset($row) ? $row->jenis_gc : '';
    $tgl_expire = isset($row) ? $row->tgl_expire : '';
    $peralatan_id = isset($row) ? $row->no_peralatan_id : '';
    $provinsi_id = isset($row) ? $row->provinsi_id : '';
    $kobu_id = isset($row) ? $row->kobu_id : '';

    $bukti = isset($row) ? $row->bukti : '';
    $btn_val = isset($row) ? 'update' : 'save';
    ?>

    <form action="<?php echo site_url($modul.'/simpan'); ?>" method="post" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $id; ?>">

      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="tgl_gc">Tanggal Ground Check</label>
            <input type="text" name="tgl_gc" id="tgl_gc" value="<?php echo $tgl_gc; ?>" class="form-control" placeholder="Tanggal Ground Check" autofocus="autofocus" autocomplete="off">
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="jenis_gc">Jenis</label>
            <select name="jenis_gc" id="jenis_gc" class="form-control">
              <option value="">Jenis</option>
              <?php
              foreach ($arr_jenis_gc as $val_jenis_gc) {
                $sel_jenis_gc = $val_jenis_gc == $jenis_gc ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $val_jenis_gc; ?>" <?php echo $sel_jenis_gc; ?>><?php echo $val_jenis_gc; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="tanggal_terbit">Tanggal Expire</label>
            <input type="text" name="tgl_expire" id="tgl_expire" value="<?php echo $tgl_expire; ?>" class="form-control" placeholder="Tanggal Expire" autofocus="autofocus" autocomplete="off">
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="provinsi">Provinsi</label>
            <select name="provinsi" id="provinsi" class="form-control">
              <option value="">Pilih Provinsi</option>
              <?php
              foreach ($provinsi as $vProvinsi) {
                $selKobu = $kobu_id == $vProvinsi->id ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vProvinsi->id; ?>" <?php echo $selKobu; ?>><?php echo $vProvinsi->nama; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="lokasi_alat">Lokasi Alat</label>
            <select name="lokasi_alat" id="lokasi_alat" class="form-control">
              <option value="">Pilih Lokasi Alat</option>
              <?php
              if (isset($lokasi_alat)) {
                foreach ($lokasi_alat as $vLokasiAlat) {
                  $selLokasiAlat = $lokasi_alat == $vLokasiAlat->id ? 'selected="selected"' : '';
                  ?>
                  <option value="<?php echo $vLokasiAlat->id; ?>" <?php echo $selLokasiAlat; ?>><?php echo $vLokasiAlat->nama; ?></option>
                  <?php
                }
              }
              ?>
            </select>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label for="peralatan_id">Peralatan</label>
            <select name="peralatan_id" id="peralatan_id" class="form-control">
              <option value="">Pilih Peralatan</option>
              <?php
              foreach ($lppnpi as $vLppnpi) {
                $selLppnpi = $peralatan_id == $vLppnpi->id ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $vLppnpi->id; ?>" <?php echo $selLppnpi; ?>><?php echo $vLppnpi->nama; ?></option>
                <?php
              }
              ?>
            </select>
          </div>
        </div>
      </div>
      
      <div class="row"> 
        <div class="col-md-4"> 
          <div class="form-group">
            <label for="bukti">Bukti</label>
            <input type="file" name="bukti" id="bukti">
          </div>
        </div>  
      </div>

      <button type="submit" name="save" id="save" value="<?php echo $btn_val; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    </form>

  </div>
</div>


<script>
$(document).ready(function () {
  $("#tgl_gc").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    orientation: "bottom auto",
    todayHighlight: true,
    todayBtn: "linked"
  });

  $("#tgl_expire").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    orientation: "bottom auto",
    todayHighlight: true,
    todayBtn: "linked"
  });

  $("#provinsi").change(function () {
    var provinsi_id = $("#provinsi").val();
    $.ajax({
      url: '<?php echo site_url('lokasi_alat/get_data_by_provinsi');?>',
      data: {provinsi_id: provinsi_id},
      method: 'POST',
      dataType: 'json',
      success: function (response) {
        $("#lokasi_alat").empty();
        $("#lokasi_alat").append('<option value="">Pilih Lokasi Alat</option');
        $.each(response, function (key, value) {
          $("#lokasi_alat").append('<option value="'+value.id+'">'+value.nama+'</option>');
        })
      }
    });
  });

  $("#lokasi_alat").change(function () {
    var lokasi_alat_id = $("#lokasi_alat").val();
    $.ajax({
      url: '<?php echo site_url('peralatan/get_data_by_lokasi_alat');?>',
      data: {lokasi_alat_id: lokasi_alat_id},
      method: 'POST',
      dataType: 'json',
      success: function (response) {
        $("#peralatan_id").empty();
        $("#peralatan_id").append('<option value="">Pilih Peralatan</option');
        $.each(response, function (key, value) {
          $("#peralatan_id").append('<option value="'+value.id+'">'+value.nama+'</option>');
        })
      }
    });
  });
});
</script>