<?php 

/**
 * @author Awaluddin <awaluddinbinusep@gmail.com>
 */
 
function seo_title($s) 
{
    $c = array (' ');
    $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+','–');
    $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d
    $s = strtolower(str_replace($c, '-', $s)); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
    return $s;
}
function hari_ini(){
    date_default_timezone_set('Asia/Jakarta'); // PHP 6 mengharuskan penyebutan timezone.
    $seminggu = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
    $hari = date("w");
    return $seminggu[$hari];
}
?>