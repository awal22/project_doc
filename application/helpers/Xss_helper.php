<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */

class Xss_helper
{	
	
	function cetak($value)
	{
	  echo htmlentities($value, ENT_QUOTES, 'UTF-8');
	}
}
