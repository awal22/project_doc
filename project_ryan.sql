/*
SQLyog Enterprise v12.5.1 (64 bit)
MySQL - 10.4.17-MariaDB : Database - producod_dbnav3
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`producod_dbnav3` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `producod_dbnav3`;

/*Table structure for table `arsip_crew` */

DROP TABLE IF EXISTS `arsip_crew`;

CREATE TABLE `arsip_crew` (
  `arsip_crew_id` int(11) NOT NULL AUTO_INCREMENT,
  `arsip_crew_nomor` varchar(255) DEFAULT NULL,
  `arsip_crew_nama` int(11) NOT NULL,
  `arsip_crew_deskripsi` varchar(255) DEFAULT NULL,
  `arsip_crew_expired` date DEFAULT NULL,
  `arsip_crew_file` varchar(255) DEFAULT NULL,
  `arsip_crew_created_at` datetime NOT NULL,
  `arsip_crew_created_by` int(11) NOT NULL,
  `arsip_crew_update_at` datetime NOT NULL,
  `arsip_crew_update_by` int(11) NOT NULL,
  PRIMARY KEY (`arsip_crew_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `arsip_crew` */

insert  into `arsip_crew`(`arsip_crew_id`,`arsip_crew_nomor`,`arsip_crew_nama`,`arsip_crew_deskripsi`,`arsip_crew_expired`,`arsip_crew_file`,`arsip_crew_created_at`,`arsip_crew_created_by`,`arsip_crew_update_at`,`arsip_crew_update_by`) values 
(1,'1241244',3,'apa aja dah','2021-03-09','file_1_1615942632.png','2021-03-17 01:57:12',1,'0000-00-00 00:00:00',0),
(2,'1245535',2,'first dokumen','2021-05-13','file_1_1615944357.png','2021-03-17 02:25:57',1,'0000-00-00 00:00:00',0);

/*Table structure for table `arsip_kapal` */

DROP TABLE IF EXISTS `arsip_kapal`;

CREATE TABLE `arsip_kapal` (
  `arsip_kapal_id` int(11) NOT NULL AUTO_INCREMENT,
  `arsip_kapal_nomor` varchar(255) DEFAULT NULL,
  `arsip_kapal_nama` int(11) NOT NULL,
  `arsip_kapal_deskripsi` varchar(255) DEFAULT NULL,
  `arsip_kapal_expired` date DEFAULT NULL,
  `arsip_kapal_file` varchar(255) DEFAULT NULL,
  `arsip_kapal_created_at` datetime NOT NULL,
  `arsip_kapal_created_by` int(11) NOT NULL,
  `arsip_kapal_update_at` datetime NOT NULL,
  `arsip_kapal_update_by` int(11) NOT NULL,
  PRIMARY KEY (`arsip_kapal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `arsip_kapal` */

insert  into `arsip_kapal`(`arsip_kapal_id`,`arsip_kapal_nomor`,`arsip_kapal_nama`,`arsip_kapal_deskripsi`,`arsip_kapal_expired`,`arsip_kapal_file`,`arsip_kapal_created_at`,`arsip_kapal_created_by`,`arsip_kapal_update_at`,`arsip_kapal_update_by`) values 
(1,'23435666',1,'dokumen stnk','2021-04-21','file_1_1615826746.png','2021-03-15 17:45:46',1,'2021-03-15 17:56:09',1),
(2,'124244',1,'dokumen berharga','2021-03-09','file_1_1615827212.pdf','2021-03-15 17:53:32',1,'0000-00-00 00:00:00',0);

/*Table structure for table `article_attachments` */

DROP TABLE IF EXISTS `article_attachments`;

CREATE TABLE `article_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemid` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `titleAttribute` text DEFAULT NULL,
  `hits` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `article_attachments` */

/*Table structure for table `article_categories` */

DROP TABLE IF EXISTS `article_categories`;

CREATE TABLE `article_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `parentid` int(11) NOT NULL DEFAULT 0,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `access` varchar(64) NOT NULL,
  `language` char(7) NOT NULL,
  `theme` varchar(12) NOT NULL DEFAULT 'blog',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `image` text DEFAULT NULL,
  `image_caption` varchar(255) DEFAULT NULL,
  `image_credits` varchar(255) DEFAULT NULL,
  `params` text DEFAULT NULL,
  `metadesc` text DEFAULT NULL,
  `metakey` text DEFAULT NULL,
  `robots` varchar(20) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `copyright` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `article_categories` */

insert  into `article_categories`(`id`,`name`,`alias`,`description`,`parentid`,`state`,`access`,`language`,`theme`,`ordering`,`image`,`image_caption`,`image_credits`,`params`,`metadesc`,`metakey`,`robots`,`author`,`copyright`) values 
(1,'Publik','publik','',0,1,'public','en-GB','blog',0,'','','','{\"categoriesImageWidth\":\"small\",\"categoriesIntroText\":\"No\",\"categoriesFullText\":\"No\",\"categoriesCreatedData\":\"No\",\"categoriesModifiedData\":\"No\",\"categoriesUser\":\"No\",\"categoriesHits\":\"No\",\"categoriesDebug\":\"No\",\"categoryImageWidth\":\"small\",\"categoryIntroText\":\"No\",\"categoryFullText\":\"No\",\"categoryCreatedData\":\"No\",\"categoryModifiedData\":\"No\",\"categoryUser\":\"No\",\"categoryHits\":\"No\",\"categoryDebug\":\"No\",\"itemImageWidth\":\"small\",\"itemIntroText\":\"No\",\"itemFullText\":\"No\",\"itemCreatedData\":\"No\",\"itemModifiedData\":\"No\",\"itemUser\":\"No\",\"itemHits\":\"No\",\"itemDebug\":\"No\"}','','','index, follow','','');

/*Table structure for table `article_items` */

DROP TABLE IF EXISTS `article_items`;

CREATE TABLE `article_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `catid` int(11) NOT NULL DEFAULT 0,
  `userid` int(11) NOT NULL DEFAULT 0,
  `introtext` text DEFAULT NULL,
  `fulltext` text DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `access` varchar(64) NOT NULL,
  `language` char(7) NOT NULL,
  `theme` varchar(12) NOT NULL DEFAULT 'blog',
  `ordering` int(11) NOT NULL DEFAULT 0,
  `hits` int(11) NOT NULL DEFAULT 0,
  `image` text DEFAULT NULL,
  `image_caption` varchar(255) DEFAULT NULL,
  `image_credits` varchar(255) DEFAULT NULL,
  `video` text DEFAULT NULL,
  `video_type` varchar(20) DEFAULT NULL,
  `video_caption` varchar(255) DEFAULT NULL,
  `video_credits` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL DEFAULT 0,
  `modified` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_by` int(11) NOT NULL DEFAULT 0,
  `params` text DEFAULT NULL,
  `metadesc` text DEFAULT NULL,
  `metakey` text DEFAULT NULL,
  `robots` varchar(20) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `copyright` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `article_items` */

insert  into `article_items`(`id`,`title`,`alias`,`catid`,`userid`,`introtext`,`fulltext`,`state`,`access`,`language`,`theme`,`ordering`,`hits`,`image`,`image_caption`,`image_credits`,`video`,`video_type`,`video_caption`,`video_credits`,`created`,`created_by`,`modified`,`modified_by`,`params`,`metadesc`,`metakey`,`robots`,`author`,`copyright`) values 
(1,'DITJEN HUBUD PANTAU INSIDEN JATUHNYA HELIKOPTER DI LOMBOK','ditjen-hubud-pantau-insiden-jatuhnya-helikopter-di-lombok',1,1,'<p>Jakarta, (14/7/2019) - Direktorat Jenderal Perhubungan Udara Kementerian Perhubungan, memantau insiden jatuhnya Helikopter yang dioperasikan oleh PT. Carpediem Air di Desa Kawo, Pujut, Lombok Tengah, Minggu (14/7) sore. &nbsp;Helikopter jatuh tepat di luar pagar Bandara Internasional Lombok diarea pendekatan (Final) runway 31.</p>\r\n','<p>Jakarta, (14/7/2019) - Direktorat Jenderal Perhubungan Udara Kementerian Perhubungan, memantau insiden jatuhnya Helikopter yang dioperasikan oleh PT. Carpediem Air di Desa Kawo, Pujut, Lombok Tengah, Minggu (14/7) sore. &nbsp;Helikopter jatuh tepat di luar pagar Bandara Internasional Lombok diarea pendekatan (Final) runway 31.<br />\r\n<br />\r\nHelikopter dengan rute penerbangan Labuan Bajo - Lombok yang dipiloti Capt. Kustiyadi ini terakhir kali melakukan kontak tower saat mengarah ke runway 31, pada pukul 14.02 WITA dan diberikan ijin mendarat oleh ATC yang bertugas. Sesaat kemudian pada pukul 14.03 WITA pesawat tersebut jatuh.</p>\r\n\r\n<p>&quot;Kami terus memantau situasi atas insiden jatuh helikopter ini, semua pihak berkoordinasi dalam proses evakuasi,&quot; ujar Direktur Jenderal Perhubungan Udara, Polana B Pramesti di Jakarta.&nbsp;<br />\r\n<br />\r\nHelikopter dengan tipe B206L4 Registrasi PKCDV, membawa sebanyak tiga penumpang Warga Negara Asing (WNA) yakni Luka Marie (Jerman), Nicholas Alexander (Inggris) serta Donoso Lillo (Chille).<br />\r\n<br />\r\nPolana menambahkan, respon cepat dilakukan oleh Air Rescue Fire Fighting ( ARFF) yang tiba di lokasi kecelakaan pada pukuk 14.30 WITA. Tim ARFF terdiri dari utility dan ambulance dengan personel 10 orang.<br />\r\n<br />\r\nSelanjutnya, pada pukul 15.03 WIT, 3 penumpang &nbsp; yang mengalami luka &nbsp;dibawa ke RS Praya<br />\r\n<br />\r\n&quot;Saya mengimbau kepada seluruh penyedia layanan jasa transportasi udara untuk senantiasa mengutamakan keselamatan, keamanan penerbangan, sehingga mencegah terjadinya hal-hal yang beresiko,&quot; imbau Polana.&nbsp;<br />\r\n<br />\r\nDitempat terpisah Kepala Kantor Otoritas Bandar Udara Wilayah IV Elfi Amir mengatakan untuk membantu proses evakuasi dan pengamanan tempat kejadian kecelakaan Helikopter tersebut Inspektur Penerbangan Kantor Otoritas Bandar Udara Wilayah IV sudah terjun kelapangan mengkordinasikan dengan Kepolisian setempat dan General Manager AP I Bandara Lombok Praya. Tidak ada dampak signifikan Kecelakaan pesawat ini terhadap operasi penerbangan di Bandara Internasional Lombok Praya, yang sampai saat ini termonitor normal seperti biasanya.</p>\r\n',1,'public','All','blog',0,0,'ditjen-hubud-pantau-insiden-jatuhnya-helikopter-di-lombok.jpg','','','','youtube','','','2019-07-14 20:20:06',1,'2019-07-31 16:56:13',1,'','','','index, follow','',''),
(2,'TURUT MENDAMPINGI MENHUB, DIRJEN PERHUBUNGAN UDARA TINJAU PENYELENGGARAAN HAJI & RUNWAY 3 BANDARA SOEKARNO HATTA','turut-mendampingi-menhub-dirjen-perhubungan-udara-tinjau-penyelenggaraan-haji--runway-3-bandara-soekarno-hatta',1,1,'<p>Jakarta, 21/07/2019 - Hari ini, Direktur Jenderal Perhubungan Udara, Polana B. Pramesti, mendampingi Menteri Perhubungan, Budi Karya Sumadi dalam rangka meninjau penyelenggaraan angkutan haji dan kesiapan run way 3 di Bandar Udara Soekarno Hatta, Cengkareng, Banten. Turut hadir dalam kegiatan tersebut para &nbsp;Direktur dilingkungan Ditjen Hubud, Kepala Otoritas Bandar &nbsp;Udara Wilayah I. Direktur Utama PT Angkasa Pura II dan Direktur Utama Perum LPPNPI. Polana memastikan penyelenggaraan angkutan haji tahun 2019 berlangsung lancar.</p>\r\n','<p>Jakarta, 21/07/2019 - Hari ini, Direktur Jenderal Perhubungan Udara, Polana B. Pramesti, mendampingi Menteri Perhubungan, Budi Karya Sumadi dalam rangka meninjau penyelenggaraan angkutan haji dan kesiapan run way 3 di Bandar Udara Soekarno Hatta, Cengkareng, Banten. Turut hadir dalam kegiatan tersebut para &nbsp;Direktur dilingkungan Ditjen Hubud, Kepala Otoritas Bandar &nbsp;Udara Wilayah I. Direktur Utama PT Angkasa Pura II dan Direktur Utama Perum LPPNPI. Polana memastikan penyelenggaraan angkutan haji tahun 2019 berlangsung lancar.</p>\r\n\r\n<p>&nbsp;&quot;Untuk keberangkatan haji di sepuluh embarkasi kami pastikan berlangsung dengan aman dan lancar, begitu pula untuk kepulangan para jamaah ke tanah air nantinya &nbsp;selamat, aman dan nyaman,&quot;jelas Polana. Kepala Otoritas Bandar Udara Wilayah I, Herson, menambahkan pihaknya telah menerjunkan inspektur penerbangan untuk memastikan keberangkatan calon jamaah haji berjalan dengan selamat, aman dan nyaman dan memenuhi kepatuhan terhadap aturan penerbangan yang berlaku.&nbsp;&quot;Kami melakukan pengawasan dan memastikan keamanan terkait keberangkatan calon jamaah haji dari asrama haji dengan menerjunkan para inspektur penerbangan bidang keamanan untuk memastikan bahwa seluruh segel bagasi calon jamaah haji tidak rusak atau putus sampai bagasi tersebut masuk pesawat&quot; ujar Herson Selain itu , Herson menambahkan bahwa inspektur penerbangan OBU I bersama dengan inspektur penerbangan dari Direktorat Kelaikudaraan dan Pengoperasian Pesawat Udara (DKPPU) melakukan ramp check sebelum pesawat diberangkatkan membawa calon jamaah haji untuk memastikan bahwa pesawat dalam keadaan laik terbang.</p>\r\n\r\n<p>Selanjutnya, Menhub, Dirjen Hubud dan rombongan melanjutkan tinjauan ke proyek pembangunan runway 3 Bandar Udara Soekarno Hatta. Runway 3 memiliki spesifikasi 2500 m x 45 m yang ditargetkan akan beroperasi pada bulan Juli. Targetnya pembangunan run way akan memiliki panjang 3000 m x 60 m dan ditargetkan akan beroperasi penuh pada November 2019. Dalam kesempatan ini pun telah berhasil didaratkan pesawat milik kalibrasi, yang menandakan bahwa run way 3 Bandara Soekarno Hatta telah siap dioperasionalkan.</p>\r\n',1,'public','All','blog',0,0,'turut-mendampingi-menhub-dirjen-perhubungan-udara-tinjau-penyelenggaraan-haji--runway-3-bandara-soekarno-hatta.jpg','','','','youtube','','','2019-07-29 20:50:40',1,'2019-07-31 16:56:58',1,'','','','index, follow','',''),
(3,'BERPERAN AKTIF DALAM KEGIATAN INTERNASIONAL, DIRJEN HUBUD HADIRI THE THIRD ICAO GLOBAL AVIATION COOPERATION SYMPOSIUM 2019 / GASC2019','berperan-aktif-dalam-kegiatan-internasional-dirjen-hubud-hadiri-the-third-icao-global-aviation-cooperation-symposium-2019--gasc2019',1,1,'<p>Jakarta &ndash; Direktur Jenderal Perhubungan Udara, Polana B. Pramesti, menghadiri The Third Edition of Global Aviation Cooperation Symposium (GASC), sebagai Pembicara, di Phuket, Thailand. Hal ini dipandang sebagai upaya aktif Indonesia dalam kegiatan berskala internasional. Selain dapat menunjukan keunggulan Indonesia di bidang penerbangan, hal ini dapat dijadikan peluang untuk meningkatkan potensi kemajuan &nbsp;penerbangan Indonesia.</p>\r\n','<p>Jakarta &ndash; Direktur Jenderal Perhubungan Udara, Polana B. Pramesti, menghadiri The Third Edition of Global Aviation Cooperation Symposium (GASC), sebagai Pembicara, di Phuket, Thailand. Hal ini dipandang sebagai upaya aktif Indonesia dalam kegiatan berskala internasional. Selain dapat menunjukan keunggulan Indonesia di bidang penerbangan, hal ini dapat dijadikan peluang untuk meningkatkan potensi kemajuan &nbsp;penerbangan Indonesia.</p>\r\n\r\n<p>Kegiatan The Third Edition of Global Aviation Cooperation Symposium (GASC) dihadiri oleh Direktur Jenderal Perhubungan Udara, Polana B. Pramesti; National Project Director, Brazil Secretariat of Civil Aviation, Fabiana Todesco; ICAO Airport Cargo Operations Expert, Simon Villeneuve; dan ICAO Regional Director of Eastern and Southern Office, Barry Kashambo. Kegiatan ini dilaksanakan selama 3 hari dengan kombinasi sesi pembicara, 6 panel diskusi, dan workshop di bidang penerbangan sipil.</p>\r\n\r\n<p><br />\r\nUntuk diketahui, ICAO Global Aviation Coopertion Sysmposium (GASC) merupakan sebuah wadah bagi para professional penerbangan untuk bertukar pandangan dan mendiskusikan tantangan serta peluang di bidang penerbangan. Kegiatan ini juga merupakan ajang bertukar pengalaman antar para pemangku kepentingan di bidang penerbangan. &nbsp;<br />\r\n<br />\r\nDirektur Jenderal Perhubungan Udara, selaku pembicara pada hari kedua Panel 3 &nbsp;dengan topik &nbsp;&ldquo;How Experts Support Capacity Building&rdquo; menyampaikan presentasi dengan tema &ldquo;Outcomes Achieved through Cooperation between Indonesia and ICAO on Capacity Building (Sharing Experience)&rdquo;, yang menyampaikan &nbsp;tentang peran, capaian dan harapan Indonesia terhadap peningkatan safety dan security serta peningkatan jaringan kerjasama dan bantuan teknis kepada Negara-negara anggota ICAO di bidang penerbangan sipil<br />\r\n<br />\r\n&ldquo;Indonesia saat ini berupaya untuk memberikan kemampuan terbaiknya di bidang penerbangan, dengan memiliki visi utama 3S+1C yaitu Safety, Security, Service dan Complience. Indonesia dengan senang hati berpartisipasi dalam setiap Program ICAO untuk mempromosikan pengembangan kapasitas setiap Negara Anggota ICAO sebagai dukungan inisiatif &ldquo;No Country Left Behind,&rdquo; jelas Polana pada paparan nya.<br />\r\n<br />\r\nSecara keseluruhan, kegiatan ini memiliki tujuan untuk :</p>\r\n\r\n<ol>\r\n	<li>Menjembatani Kesenjangan Kepatuhan terhadap ICAO SARP;</li>\r\n	<li>Menentukan solusi unik untuk mengembangkan kapasitas melalui kerjasama teknis dan proyek bantuan;</li>\r\n	<li>Mempromosikan peran Program Kerjasama Teknis ICAO dalam membantu Negara dalam mencapai tujuannya; dan</li>\r\n	<li>Memperkuat hubungan kelembagaan dan lintas industri.</li>\r\n</ol>\r\n\r\n<p>Tahun ini, The Third Edition of the Global Aviation Cooperation Symposium (GACS) oleh Technical Cooperation Bureau (TBC) dari International Civil Aviation Organization (ICAO), bekerjasama dengan Civil Aviation Authority Thailand (CAAT) dan Kantor ICAO regional Bangkok, mengangkat tema &ldquo;ICAO: Bridging the SARPs Compliance Gap with Quality and Efficiency&rdquo; (&ldquo;ICAO: Menjembatani Kesenjangan Kepatuhan SARP dengan Kualitas dan Efisiensi&rdquo;).</p>\r\n',1,'public','All','blog',0,0,'berperan-aktif-dalam-kegiatan-internasional-dirjen-hubud-hadiri-the-third-icao-global-aviation-cooperation-symposium-2019--gasc2019.jpg','','','','youtube','','','2019-07-29 20:59:13',1,'2019-07-31 16:57:37',1,'','','','index, follow','',''),
(4,'PERKUAT KERJASAMA SEKTOR TRANSPORTASI UDARA, DITJEN HUBUD SELENGGARAKAN AIR CARGO SECURITY WORKSHOP BERSAMA IATA, INGGRIS DAN AUSTRALIA','perkuat-kerjasama-sektor-transportasi-udara-ditjen-hubud-selenggarakan-air-cargo-security-workshop-bersama-iata-inggris-dan-australia',1,1,'<p>Jakarta (23/07/2019) &ndash; Direktorat Jenderal Perhubungan Udara bekerjasama dengan &nbsp;International Air Transport Association (IATA), Pemerintah Inggris, &nbsp;dan Pemerintah Australia menyelenggarakan Air Cargo Security Workshop di Jakarta guna mempererat &nbsp;kerjasama di bidang transportasi udara terutama &nbsp; dalam meningkatkan sistem keamanan kargo udara.</p>\r\n','<p>Jakarta (23/07/2019) &ndash; Direktorat Jenderal Perhubungan Udara bekerjasama dengan &nbsp;International Air Transport Association (IATA), Pemerintah Inggris, &nbsp;dan Pemerintah Australia menyelenggarakan Air Cargo Security Workshop di Jakarta guna mempererat &nbsp;kerjasama di bidang transportasi udara terutama &nbsp; dalam meningkatkan sistem keamanan kargo udara.<br />\r\n<br />\r\nAir Cargo Security Workshop &nbsp; dilaksanakan selama &nbsp;5 hari sejak tanggal 22 &nbsp;sampai 26 Juli 2019 di Hotel The Westin Jakarta yang akan &nbsp;membahas &nbsp;terkait Standards and Recommended Practices (SARPs) tentang tanggung jawab untuk mengamankan kargo udara dan pos terhadap tindakan melawan hukum &nbsp;sesuai &nbsp;dengan International Civil Aviation Organization (ICAO) Annex 17 tentang Aviation Security dan Aviation Security manual (Doc 8973) secara umum.<br />\r\n<br />\r\nKegiatan tersebut dibuka oleh &nbsp;Direktur Keamanan Penerbangan, Dadun Kohar dan &nbsp;dan dihadiri oleh Aviation Security Liaison Officer (ASLO) British Embassy, Elizabeth Mehmood dan First Secretary (Transport), Risk and Internasional Australian Embassy, David Scott.<br />\r\n<br />\r\nDirektur Jenderal Perhubungan Udara, &nbsp;Polana B Pramesti &nbsp;berharap pelaksanaan workshop tersebut dapat memberikan masukan positif untuk &nbsp;meningkatkan &nbsp;sistem keamanan kargo dan pos &nbsp;yang diangkut menggunakan pesawat udara di Indonesia.<br />\r\n<br />\r\n&ldquo;Selain untuk memperkuat regulasi angkutan kargo menggunakan pesawat udara , hal ini diharapkan juga dapat memperkuat kerjasama Indonesia dengan &nbsp;negara lain &nbsp;di bidang industry penerbangan &nbsp;dan juga Indonesia mampu &nbsp;mengikuti &nbsp;perkembangan teknologi penerbangan terutama dalam meningkatkan &nbsp;sistem keamanan kargo udara. &nbsp;<br />\r\n<br />\r\nSecara khusus, kegiatan ini akan berlangsung selama 5 hari dengan pembahasan:</p>\r\n\r\n<ol>\r\n	<li>Resiko dan ancaman angkutan kargo;</li>\r\n	<li>Tanggung jawab dalam mengamankan angkutan kargo dan pos terhadap tindakan melawan hukum, berdasarkan Annex 17 dan Doc 8973;</li>\r\n	<li>Mengulas regulasi dan standar angkutan kargo di Direktorat Jenderal Perhubungan Udara),</li>\r\n	<li>Pengawasan dan jaminan angkutan kargo;</li>\r\n	<li>Pembahasan peraturan dan masalah yang mempengaruhi agen dan maskapai penerbangan; dan</li>\r\n	<li>Pembahasan hasil dan konsolidasi tindaklanjut.</li>\r\n</ol>\r\n\r\n<p>Direktur Keamanan Penerbangan, Dadun Kohar, menyampaikan bahwa &nbsp;workshop ini &nbsp;dapat &nbsp;menjadi ajang &nbsp;untuk bertukar pengalaman antara Indonesia dengan Pemerintah Inggris dan Australia terkait dengan &nbsp;sistem keamanan kargo udara dan &nbsp;menjawab solusi terkait dengam &nbsp;masalah &ndash; masalah &nbsp;dalam &nbsp;sistem keamanan caro udara. Selain itu juga untuk meningkatkan kompetensi para &nbsp;inspektur penerbangan di bidang keamanan penerbangan.</p>\r\n',1,'public','All','blog',0,0,'perkuat-kerjasama-sektor-transportasi-udara-ditjen-hubud-selenggarakan-air-cargo-security-workshop-bersama-iata-inggris-dan-australia.jpg','','','','youtube','','','2019-07-23 21:05:33',1,'2019-07-31 16:58:07',1,'','','','index, follow','',''),
(5,'TINGKATKAN KESELAMATAN PENERBANGAN, DITJEN HUBUD BERSINERGI DENGAN STAKEHOLDER YANG MEMBERIKAN PELAYANAN INFORMASI METEOROLOGI PENERBANGAN','tingkatkan-keselamatan-penerbangan-ditjen-hubud-bersinergi-dengan-stakeholder-yang-memberikan-pelayanan-informasi-meteorologi-penerbangan',1,1,'<p>Jakarta &ndash; Guna mewujudkan keselamatan penerbangan sipil di Indonesia, Direktorat Jenderal Perhubungan Udara melalui Direktorat Navigasi Penerbangan, hari ini mengadakan Sosialisasi Peraturan Menteri Perhubungan No PM 95 tahun 2018 tentang Peraturan Keselamatan Penerbangan Sipil 174 (CASR 174) mengenai Pelayanan Informasi Meteorologi Penerbangan.</p>\r\n','<p>Jakarta &ndash; Guna mewujudkan keselamatan penerbangan sipil di Indonesia, Direktorat Jenderal Perhubungan Udara melalui Direktorat Navigasi Penerbangan, hari ini mengadakan Sosialisasi Peraturan Menteri Perhubungan No PM 95 tahun 2018 tentang Peraturan Keselamatan Penerbangan Sipil 174 (CASR 174) mengenai Pelayanan Informasi Meteorologi Penerbangan.</p>\r\n\r\n<p>Pelayanan informasi meteorologi penerbangan merupakan salah satu komponen yang penting dalam pelayanan navigasi penerbangan baik di bandar udara maupun di sepanjang jalur penerbangan. Untuk itu perlu sinergitas masing masing stakehokder baik regulator, unit pelayanan informasi meteorologi penerbangan, unit penyelenggaran navigasi penerbangan dan maskapai.<br />\r\n<br />\r\nAcara sosialisasi ini dibuka oleh Direktur Navigasi Penerbangan, Asri Santosa dan dihadiri pula oleh Kepala Pusat Meteorologi Penerbangan BMKG Agus Wahyu Raharjo, Para Kepala Kantor Otoritas Bandar Udara, Para Kepala Stasiun Meteorologi Kelas I dan II serta Perum LPPNPI.<br />\r\n<br />\r\nDalam sambutannya, Asri Santosa menegaskan perlunya sinergitas kepada para stakeholder khususnya BMKG untuk menjamin keselamatan penerbangan. &quot;Harapan saya agar momentum ini dapat menjadi suatu forum dalam menyamakan visi, sharing pengalaman dan pengetahuan dalam mewujudkan pelayanan informasi meteorologi penerbangan sesuai kriteria yang dipersyaratkan,&quot;ujar Asri.<br />\r\n<br />\r\nKepala Pusat Meteorologi Penerbangan BMKG Agus Wahyu Raharjo, menyampaikan bahwa pihaknya telah siap melakukan peningkatan sinergitas memberikan pelayanan informasi secara cepat dan tepat. &quot;Beberapa pelayanan yang diberikan oleh Meteorologi Penerbangan diantaranya adalah peringatan seperti SIGMET dan Informasi Abu Vulkanik, pengamatan, prakiraan dan informasi pendukung lainnya untuk memberikan informasi seakurat mungkin untuk menjamin keselamatan penerbangan,&quot;papar Agus.<br />\r\n<br />\r\nPM 95 tahun 2018 tentang Peraturan Keselamatan Penerbangan Sipil Bagian 174 (Civil Aviation Safety Regulations P Art 174) Tentang Pelayanan Informasi Meteorologi Penerbangan (Aeronautical Meteorological Information Services) dan sejalan dengan Standard and Recommended Practices (SARPs) ICAO Annexes 3 &ndash; Aeronautical Information Meteorological Information mewajibkan Direktorat Jenderal Perhubungan Udara untuk menyampaikan perkembangan peraturan di bidang meteorologi penerbangan kepada Badan Meteorologi, Klimatologi dan Geofisika selaku penyedia pelayanan tersebut sehingga implementasi pelayanan tersebut dalam tataran operasional akan menjadi optimal.<br />\r\n<br />\r\nSejalan dengan perkembangan ketentuan ICAO SARPs dan kebijakan di bidang penerbangan sipil yang dinamis, sampai dengan saat ini CASR 174 (PM 95 th 2018) telah mengalami empat kali perubahan yaitu PM 52 tahun 2010, PM 9 tahun 2015, PM 138 tahun 2015, PM 108 tahun 2016 dan terakhir menjadi PM 95 tahun 2018. Dalam PM 95 tahun 2018 dilakukan reformulasi pengaturan guna compliance dengan perkembangan ICAO Annex 3 - Aeronautical Meteorological Services baik dari sisi personel, sistem kendali mutu, dan perkembangan pengelolaan informasi meteorologi ke depan serta Perubahan Pengaturan diantaranya terkait mekanisme pengawasan dan Inspektur meteorologi penerbangan yang ditetapkan diatur didalam peraturan tersendiri.<br />\r\n<br />\r\nDirektorat Jenderal Perhubungan Udara dan Badan Meteorologi, Klimatologi, dan Geofisika akan melakukan kerja sama dan koordinasi teknis secara rutin guna pemenuhan atas kebutuhan dan operasional pelayanan informasi meteorologi penerbangan sehingga terpenuhinya tingkat kecukupan, keakurasian dan ketepatan informasi.</p>\r\n',1,'public','All','blog',0,0,'tingkatkan-keselamatan-penerbangan-ditjen-hubud-bersinergi-dengan-stakeholder-yang-memberikan-pelayanan-informasi-meteorologi-penerbangan.jpg','','','','youtube','','','2019-07-29 21:10:06',1,'2019-07-31 16:58:25',1,'','','','index, follow','',''),
(6,'JAMIN KEAMANAN PENERBANGAN, BANDARA RADIN INTEN II LAMPUNG MENGGELAR SOSIALISASI KEAMANAN PENERBANGAN','jamin-keamanan-penerbangan-bandara-radin-inten-ii-lampung-menggelar-sosialisasi-keamanan-penerbangan',1,1,'<p>Jakarta - Unit Penyelenggara Bandar Udara &nbsp;(UPBU) Radin Inten II Lampung menyelenggarakan kegiatan Sosialisasi &nbsp;Tentang Keamanan , Keselamatan Penerbangan Penanganan awal dugaan Tindak Pidana Penerbangan sipil yang bertempat di Novotel Bandar Lampung, Kamis (25/07).</p>\r\n','<p>Jakarta - Unit Penyelenggara Bandar Udara &nbsp;(UPBU) Radin Inten II Lampung menyelenggarakan kegiatan Sosialisasi &nbsp;Tentang Keamanan , Keselamatan Penerbangan Penanganan awal dugaan Tindak Pidana Penerbangan sipil yang bertempat di Novotel Bandar Lampung, Kamis (25/07).<br />\r\n<br />\r\nSosialiasi ini diikuti sebanyak 78 peserta dari stakeholder penerbangan di wilayah Bandar Udara Radin Inten II &nbsp;Lampung. Penyelenggaraan sosialiasi &nbsp;dibuka oleh Kepala Bandar Udara Radin Inten II Lampung, Asep Kosasi Samapta, turut hadir dalam kegiatan Kepala BNN Provinsi Lampung Brigjen Pol Ery Nursatari, Kepala Kantor Otoritas Wilayah I Kkelas Utama,Herson, serta Kasubdit PPNS Direktorat Keamanan Penerbangan Rudi Ricardo &nbsp;sebagai narasumber pada kegiatan tersebut.<br />\r\n<br />\r\nDalam kegiatan itu, UPBU Bandar Udara Radin Inten II Lampung mengajak para stakeholder penerbangan berkomitmen memberikan pelayanan yang terbaik di bidang kemanan dan keselamatan penerbangan kepada masyarakat pengguna transportasi udara .<br />\r\n<br />\r\n&ldquo;Tujuan dari terselenggaranya kegiatan ini adalah meningkatkan keselamatan dan keamanan penerbangan dan meningkatkan pengetahuan dan mengedukasi tentang keselamatan dan keamanan penerbangan,&rdquo; kata Kepala UPBU Radin Inten II, Asep Kosasi saat membuka acara.<br />\r\n<br />\r\nAsep berharap, sosialiasi tentang pentingnya keamanan penerbangan dapat didukung penuh oleh &nbsp;Pemda, Bea dan Cukai, maskapai penerbangan, maskapai dan masyarakat sebagai calon pengguna jasa angkutan udara. &nbsp;&ldquo;<br />\r\n<br />\r\nDapat berperan aktif berkolaborasi dan berkoordinasi untuk menjaga keamanan operasional penerbangan dengan tetap memperhitungkan keseimbangan terhadap aspek pelayanan kepada pengguna jasa angkutan udara.<br />\r\n<br />\r\n<br />\r\nDi tempat yang sama, Kepala Otoritas Bandar Udara Wilayah I Kelas Utama, Herson, menjelaskan, aspek kemanan penerbangan &nbsp;merupakan salah satu unsur yang wajib dipenuhi oleh setiap bandara dan menjadi tanggung jawab setiap pelaku dan pengguna jasa pelayanan penerbangan sesuai PM 80 Tahun 2017 Tentang Kemananan Penerbangan Nasioanal untuk mewujudkan prioritas Direktorat Jenderal Perhubungan Udara dalam mewujudkan 3S+1C yaitu Safety, Security, Service, Compliance.<br />\r\n<br />\r\n&ldquo;Sesuai PM 80 Tahun 2017 Tentang Kemananan Penerbangan Nasioanal pembagian tanggung jawab penerbangan Kepala Kantor Otoritas Bandara mempunyai tugas dan tanggung jawab menjamin terlaksana dan terpenuhinya ketentuan keamanan serta menyelesaikan masalah-masalah yang dapat mengganggu operasional penerbangan di wilayah kerjanya,&rdquo; tutup dia.</p>\r\n',1,'public','All','blog',0,0,'jamin-keamanan-penerbangan-bandara-radin-inten-ii-lampung-menggelar-sosialisasi-keamanan-penerbangan.jpg','','','','youtube','','','2019-07-29 21:13:53',1,'2019-07-31 16:58:42',1,'','','','index, follow','',''),
(7,'DITJEN HUBUD MENGGANDENG IATA UNTUK MENINGKATKAN PELAYANAN NAVIGASI PENERBANGAN DI RUANG UDARA INDONESIA','ditjen-hubud-menggandeng-iata-untuk-meningkatkan-pelayanan-navigasi-penerbangan-di-ruang-udara-indonesia',1,1,'<p>Jakarta &ndash; Direktorat Jenderal Perhubungan Udara Kementerian Perhubungan bekerjasama dengan Internasional Air Transport Association (IATA) dalam meningkatkan pelayanan penerbangan di Indonesia. Sinergi kerjasama ditandai dengan penandatangan Nota Kesepahaman atau Memorandum of Understanding (MoU) yang berlangsung di ruang rapat Direktorat Navigasi Penerbangan, Gedung Sainath Tower Lt 12, Kemayoran, Selasa (30/7) hari ini.</p>\r\n','<p>Jakarta &ndash; Direktorat Jenderal Perhubungan Udara Kementerian Perhubungan bekerjasama dengan Internasional Air Transport Association (IATA) dalam meningkatkan pelayanan penerbangan di Indonesia. Sinergi kerjasama ditandai dengan penandatangan Nota Kesepahaman atau Memorandum of Understanding (MoU) yang berlangsung di ruang rapat Direktorat Navigasi Penerbangan, Gedung Sainath Tower Lt 12, Kemayoran, Selasa (30/7) hari ini.<br />\r\n<br />\r\nPenandatanganan MoU dilakukan Direktur Jenderal Perhubungan Udara, Polana B Pramesti yang diwakili oleh Direktur Navigasi Penerbangan, Asri Santosa dan perwakilan IATA, Conrad Clifford selaku Vice President &ndash; Asia Pasific dan disaksikan perwakilan AirNav Indonesia dan pejabat di lingkungan Direktorat Navigasi Penerbangan.<br />\r\n<br />\r\nDalam sambutannya, Asri Santosa mengatakan, adanya kerjasama dengan IATA merupakan suatu kemajuan untuk meningkatkan keselamatan dan pelayanan pada ruang udara di Indonesia. &ldquo;Diharapkan dalam kerjasama ini kedua belah pihak mendapatkan benefit untuk penerbangan,&rdquo; ujar Asri.<br />\r\n<br />\r\nMenurut Asri, alasan dipilihnya IATA sebagai partnership karena merupakan lembaga internasional yang memiliki legalitas dan memiliki pengaruh terhadap penerbangan internasional. Dengan kerjasama ini, setiap regulasi dan masterplan yang diselesaikan dapat sinkron dengan kebijakan penerbangan internasional.<br />\r\n<br />\r\n&ldquo;Aktualisasi ini bisa dilakukan tahun ini, jadi tidak hanya sebatas simbolis, kita harus memiliki strategi program karena memang harus ada yang dibenahi di ruang udara kita, sehingga ruang udara bisa optimal mengingat Indonesia merupakan jalur strategis penerbangan serta Indonesia di mata dunia dapat dinilai menomor satukan keselamatan penerbangan,&rdquo; tuturnya.<br />\r\n<br />\r\nKerjasama ini bertujuan untuk meningkatkan keselamatan, kelancaran dan efisiensi pelayanan navpen, pengembangan sistem pelayanan navigasi penerbangan yg seamless, selamat, dan interoperable selaras dengan kebijakan pelayanan navigasi penerbangan regional Asia-Pasific, serta melakukan mitigasi gap dampak atas penggunaan sistem navigasi penerbangan yang terkini pada masa transisi.<br />\r\n<br />\r\nUntuk diketahui, perjanjian kerjasama ini nantinya akan mencakup pengaturan perencanaan Rencana Navigasi Penerbangan Nasional Indonesia dalam bentuk konsultasi, review dan bantuan teknis dalam penerapan Rencana Navigasi Penerbangan.</p>\r\n',1,'public','All','blog',0,0,'ditjen-hubud-menggandeng-iata-untuk-meningkatkan-pelayanan-navigasi-penerbangan-di-ruang-udara-indonesia.jpg','','','','youtube','','','2019-07-30 18:40:51',1,'2019-07-30 18:40:51',0,'','','','index, follow','','');

/*Table structure for table `auth_assignment` */

DROP TABLE IF EXISTS `auth_assignment`;

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_assignment` */

insert  into `auth_assignment`(`item_name`,`user_id`,`created_at`) values 
('Admin','2',1543391661),
('Admin','4',1550547307),
('Admin','5',1552025662),
('Admin','6',1552025652),
('Admin Permission ','4',1543076982),
('ADMINISTRATOR','1',1563953264),
('Administrator ','4',1543076982),
('karyawan','3',1543434521),
('staff','7',1552028010);

/*Table structure for table `auth_item` */

DROP TABLE IF EXISTS `auth_item`;

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_item` */

insert  into `auth_item`(`name`,`type`,`description`,`rule_name`,`data`,`created_at`,`updated_at`) values 
('/*',2,NULL,NULL,NULL,1566352998,1566352998),
('/admin/*',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/assignment/*',2,NULL,NULL,NULL,1566352992,1566352992),
('/admin/assignment/assign',2,NULL,NULL,NULL,1566352992,1566352992),
('/admin/assignment/index',2,NULL,NULL,NULL,1566352992,1566352992),
('/admin/assignment/revoke',2,NULL,NULL,NULL,1566352992,1566352992),
('/admin/assignment/view',2,NULL,NULL,NULL,1566352992,1566352992),
('/admin/default/*',2,NULL,NULL,NULL,1566352992,1566352992),
('/admin/default/index',2,NULL,NULL,NULL,1566352992,1566352992),
('/admin/menu/*',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/menu/create',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/menu/delete',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/menu/index',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/menu/update',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/menu/view',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/permission/*',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/permission/assign',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/permission/create',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/permission/delete',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/permission/index',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/permission/remove',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/permission/update',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/permission/view',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/role/*',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/role/assign',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/role/create',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/role/delete',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/role/index',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/role/remove',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/role/update',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/role/view',2,NULL,NULL,NULL,1566352993,1566352993),
('/admin/route/*',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/route/assign',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/route/create',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/route/index',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/route/refresh',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/route/remove',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/rule/*',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/rule/create',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/rule/delete',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/rule/index',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/rule/update',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/rule/view',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/user/*',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/user/activate',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/user/change-password',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/user/delete',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/user/index',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/user/login',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/user/logout',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/user/request-password-reset',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/user/reset-password',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/user/signup',2,NULL,NULL,NULL,1566352994,1566352994),
('/admin/user/view',2,NULL,NULL,NULL,1566352994,1566352994),
('/articles/*',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/attachments/*',2,NULL,NULL,NULL,1566352994,1566352994),
('/articles/attachments/create',2,NULL,NULL,NULL,1566352994,1566352994),
('/articles/attachments/delete',2,NULL,NULL,NULL,1566352994,1566352994),
('/articles/attachments/deletemultiple',2,NULL,NULL,NULL,1566352994,1566352994),
('/articles/attachments/index',2,NULL,NULL,NULL,1566352994,1566352994),
('/articles/attachments/update',2,NULL,NULL,NULL,1566352994,1566352994),
('/articles/attachments/view',2,NULL,NULL,NULL,1566352994,1566352994),
('/articles/categories/*',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/categories/activemultiple',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/categories/changestate',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/categories/create',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/categories/deactivemultiple',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/categories/delete',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/categories/deleteimage',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/categories/deletemultiple',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/categories/index',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/categories/update',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/categories/view',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/default/*',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/default/index',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/items/*',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/items/activemultiple',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/items/changestate',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/items/create',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/items/deactivemultiple',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/items/delete',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/items/deleteimage',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/items/deletemultiple',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/items/index',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/items/update',2,NULL,NULL,NULL,1566352995,1566352995),
('/articles/items/view',2,NULL,NULL,NULL,1566352995,1566352995),
('/ckeditor/*',2,NULL,NULL,NULL,1566352996,1566352996),
('/ckeditor/default/*',2,NULL,NULL,NULL,1566352996,1566352996),
('/ckeditor/default/file-browse',2,NULL,NULL,NULL,1566352996,1566352996),
('/ckeditor/default/file-upload',2,NULL,NULL,NULL,1566352996,1566352996),
('/ckeditor/default/image-browse',2,NULL,NULL,NULL,1566352996,1566352996),
('/ckeditor/default/image-upload',2,NULL,NULL,NULL,1566352996,1566352996),
('/dashboard/*',2,NULL,NULL,NULL,1566352996,1566352996),
('/dashboard/index',2,NULL,NULL,NULL,1566352996,1566352996),
('/datecontrol/*',2,NULL,NULL,NULL,1566352996,1566352996),
('/datecontrol/parse/*',2,NULL,NULL,NULL,1566352995,1566352995),
('/datecontrol/parse/convert',2,NULL,NULL,NULL,1566352995,1566352995),
('/debug/*',2,NULL,NULL,NULL,1566352996,1566352996),
('/debug/default/*',2,NULL,NULL,NULL,1566352996,1566352996),
('/debug/default/db-explain',2,NULL,NULL,NULL,1566352996,1566352996),
('/debug/default/download-mail',2,NULL,NULL,NULL,1566352996,1566352996),
('/debug/default/index',2,NULL,NULL,NULL,1566352996,1566352996),
('/debug/default/toolbar',2,NULL,NULL,NULL,1566352996,1566352996),
('/debug/default/view',2,NULL,NULL,NULL,1566352996,1566352996),
('/debug/user/*',2,NULL,NULL,NULL,1566352996,1566352996),
('/debug/user/reset-identity',2,NULL,NULL,NULL,1566352996,1566352996),
('/debug/user/set-identity',2,NULL,NULL,NULL,1566352996,1566352996),
('/fungsi/*',2,NULL,NULL,NULL,1566352996,1566352996),
('/fungsi/create',2,NULL,NULL,NULL,1566352996,1566352996),
('/fungsi/delete',2,NULL,NULL,NULL,1566352996,1566352996),
('/fungsi/index',2,NULL,NULL,NULL,1566352996,1566352996),
('/fungsi/update',2,NULL,NULL,NULL,1566352996,1566352996),
('/fungsi/view',2,NULL,NULL,NULL,1566352996,1566352996),
('/gii/*',2,NULL,NULL,NULL,1566352996,1566352996),
('/gii/default/*',2,NULL,NULL,NULL,1566352996,1566352996),
('/gii/default/action',2,NULL,NULL,NULL,1566352996,1566352996),
('/gii/default/diff',2,NULL,NULL,NULL,1566352996,1566352996),
('/gii/default/index',2,NULL,NULL,NULL,1566352996,1566352996),
('/gii/default/preview',2,NULL,NULL,NULL,1566352996,1566352996),
('/gii/default/view',2,NULL,NULL,NULL,1566352996,1566352996),
('/gridview/*',2,NULL,NULL,NULL,1566352995,1566352995),
('/gridview/export/*',2,NULL,NULL,NULL,1566352995,1566352995),
('/gridview/export/download',2,NULL,NULL,NULL,1566352995,1566352995),
('/groundcheck/*',2,NULL,NULL,NULL,1566352996,1566352996),
('/groundcheck/create',2,NULL,NULL,NULL,1566352996,1566352996),
('/groundcheck/delete',2,NULL,NULL,NULL,1566352996,1566352996),
('/groundcheck/index',2,NULL,NULL,NULL,1566352996,1566352996),
('/groundcheck/update',2,NULL,NULL,NULL,1566352996,1566352996),
('/groundcheck/view',2,NULL,NULL,NULL,1566352996,1566352996),
('/isr/*',2,NULL,NULL,NULL,1566352996,1566352996),
('/isr/create',2,NULL,NULL,NULL,1566352996,1566352996),
('/isr/delete',2,NULL,NULL,NULL,1566352996,1566352996),
('/isr/index',2,NULL,NULL,NULL,1566352996,1566352996),
('/isr/update',2,NULL,NULL,NULL,1566352996,1566352996),
('/isr/view',2,NULL,NULL,NULL,1566352996,1566352996),
('/kacab-pembina/*',2,NULL,NULL,NULL,1566352997,1566352997),
('/kacab-pembina/create',2,NULL,NULL,NULL,1566352997,1566352997),
('/kacab-pembina/delete',2,NULL,NULL,NULL,1566352997,1566352997),
('/kacab-pembina/index',2,NULL,NULL,NULL,1566352996,1566352996),
('/kacab-pembina/update',2,NULL,NULL,NULL,1566352997,1566352997),
('/kacab-pembina/view',2,NULL,NULL,NULL,1566352996,1566352996),
('/kalibrasi/*',2,NULL,NULL,NULL,1566352997,1566352997),
('/kalibrasi/create',2,NULL,NULL,NULL,1566352997,1566352997),
('/kalibrasi/delete',2,NULL,NULL,NULL,1566352997,1566352997),
('/kalibrasi/index',2,NULL,NULL,NULL,1566352997,1566352997),
('/kalibrasi/update',2,NULL,NULL,NULL,1566352997,1566352997),
('/kalibrasi/view',2,NULL,NULL,NULL,1566352997,1566352997),
('/kobu/*',2,NULL,NULL,NULL,1566352997,1566352997),
('/kobu/create',2,NULL,NULL,NULL,1566352997,1566352997),
('/kobu/delete',2,NULL,NULL,NULL,1566352997,1566352997),
('/kobu/index',2,NULL,NULL,NULL,1566352997,1566352997),
('/kobu/update',2,NULL,NULL,NULL,1566352997,1566352997),
('/kobu/view',2,NULL,NULL,NULL,1566352997,1566352997),
('/lokasi-alat/*',2,NULL,NULL,NULL,1566352997,1566352997),
('/lokasi-alat/create',2,NULL,NULL,NULL,1566352997,1566352997),
('/lokasi-alat/delete',2,NULL,NULL,NULL,1566352997,1566352997),
('/lokasi-alat/index',2,NULL,NULL,NULL,1566352997,1566352997),
('/lokasi-alat/update',2,NULL,NULL,NULL,1566352997,1566352997),
('/lokasi-alat/view',2,NULL,NULL,NULL,1566352997,1566352997),
('/lppnpi/*',2,NULL,NULL,NULL,1566352997,1566352997),
('/lppnpi/create',2,NULL,NULL,NULL,1566352997,1566352997),
('/lppnpi/delete',2,NULL,NULL,NULL,1566352997,1566352997),
('/lppnpi/index',2,NULL,NULL,NULL,1566352997,1566352997),
('/lppnpi/update',2,NULL,NULL,NULL,1566352997,1566352997),
('/lppnpi/view',2,NULL,NULL,NULL,1566352997,1566352997),
('/markdown/*',2,NULL,NULL,NULL,1566352995,1566352995),
('/markdown/parse/*',2,NULL,NULL,NULL,1566352995,1566352995),
('/markdown/parse/download',2,NULL,NULL,NULL,1566352995,1566352995),
('/markdown/parse/preview',2,NULL,NULL,NULL,1566352995,1566352995),
('/merk/*',2,NULL,NULL,NULL,1566352997,1566352997),
('/merk/create',2,NULL,NULL,NULL,1566352997,1566352997),
('/merk/delete',2,NULL,NULL,NULL,1566352997,1566352997),
('/merk/index',2,NULL,NULL,NULL,1566352997,1566352997),
('/merk/update',2,NULL,NULL,NULL,1566352997,1566352997),
('/merk/view',2,NULL,NULL,NULL,1566352997,1566352997),
('/news/*',2,NULL,NULL,NULL,1566352998,1566352998),
('/news/view',2,NULL,NULL,NULL,1566352997,1566352997),
('/pelayanan/*',2,NULL,NULL,NULL,1566352998,1566352998),
('/pelayanan/create',2,NULL,NULL,NULL,1566352998,1566352998),
('/pelayanan/delete',2,NULL,NULL,NULL,1566352998,1566352998),
('/pelayanan/index',2,NULL,NULL,NULL,1566352998,1566352998),
('/pelayanan/update',2,NULL,NULL,NULL,1566352998,1566352998),
('/pelayanan/view',2,NULL,NULL,NULL,1566352998,1566352998),
('/peralatan/*',2,NULL,NULL,NULL,1566352998,1566352998),
('/peralatan/create',2,NULL,NULL,NULL,1566352998,1566352998),
('/peralatan/delete',2,NULL,NULL,NULL,1566352998,1566352998),
('/peralatan/index',2,NULL,NULL,NULL,1566352998,1566352998),
('/peralatan/update',2,NULL,NULL,NULL,1566352998,1566352998),
('/peralatan/view',2,NULL,NULL,NULL,1566352998,1566352998),
('/sertifikat171/*',2,NULL,NULL,NULL,1566352998,1566352998),
('/sertifikat171/create',2,NULL,NULL,NULL,1566352998,1566352998),
('/sertifikat171/delete',2,NULL,NULL,NULL,1566352998,1566352998),
('/sertifikat171/index',2,NULL,NULL,NULL,1566352998,1566352998),
('/sertifikat171/update',2,NULL,NULL,NULL,1566352998,1566352998),
('/sertifikat171/view',2,NULL,NULL,NULL,1566352998,1566352998),
('/site/*',2,NULL,NULL,NULL,1566352998,1566352998),
('/site/about',2,NULL,NULL,NULL,1566352998,1566352998),
('/site/error',2,NULL,NULL,NULL,1566352998,1566352998),
('/site/index',2,NULL,NULL,NULL,1566352998,1566352998),
('/tipe/*',2,NULL,NULL,NULL,1566352998,1566352998),
('/tipe/create',2,NULL,NULL,NULL,1566352998,1566352998),
('/tipe/delete',2,NULL,NULL,NULL,1566352998,1566352998),
('/tipe/index',2,NULL,NULL,NULL,1566352998,1566352998),
('/tipe/update',2,NULL,NULL,NULL,1566352998,1566352998),
('/tipe/view',2,NULL,NULL,NULL,1566352998,1566352998),
('/user/*',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/admin/*',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/admin/assignments',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/admin/block',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/admin/confirm',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/admin/create',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/admin/delete',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/admin/index',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/admin/info',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/admin/resend-password',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/admin/switch',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/admin/update',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/admin/update-profile',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/profile/*',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/profile/index',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/profile/show',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/recovery/*',2,NULL,NULL,NULL,1566352991,1566352991),
('/user/recovery/request',2,NULL,NULL,NULL,1566352991,1566352991),
('/user/recovery/reset',2,NULL,NULL,NULL,1566352991,1566352991),
('/user/registration/*',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/registration/confirm',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/registration/connect',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/registration/register',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/registration/resend',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/security/*',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/security/auth',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/security/login',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/security/logout',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/settings/*',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/settings/account',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/settings/confirm',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/settings/delete',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/settings/disconnect',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/settings/networks',2,NULL,NULL,NULL,1566352992,1566352992),
('/user/settings/profile',2,NULL,NULL,NULL,1566352992,1566352992),
('Admin Permission',2,NULL,NULL,NULL,1566353025,1566353025),
('Administrator',1,NULL,NULL,NULL,1566352956,1566352956);

/*Table structure for table `auth_item_child` */

DROP TABLE IF EXISTS `auth_item_child`;

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_item_child` */

insert  into `auth_item_child`(`parent`,`child`) values 
('admin','articles-create-categories'),
('admin','articles-create-items'),
('admin','articles-create-tags'),
('admin','articles-delete-all-items'),
('admin','articles-delete-categories'),
('admin','articles-delete-tags'),
('admin','articles-index-all-items'),
('admin','articles-index-categories'),
('admin','articles-index-tags'),
('admin','articles-publish-all-items'),
('admin','articles-publish-categories'),
('admin','articles-update-all-items'),
('admin','articles-update-categories'),
('admin','articles-update-tags'),
('admin','articles-view-categories'),
('admin','articles-view-items'),
('Admin Permission','/*'),
('ADMINISTRATOR','admin'),
('Administrator','Admin Permission'),
('ADMINISTRATOR','article manager'),
('ADMINISTRATOR','basic user'),
('ADMINISTRATOR','rbac manager'),
('ADMINISTRATOR','user manager'),
('article manager','/articles/*'),
('article manager','/ckeditor/*'),
('author','articles-create-items'),
('author','articles-index-his-items'),
('author','articles-update-his-items'),
('author','articles-view-categories'),
('author','articles-view-items'),
('basic user','/dashboard/*'),
('basic user','/user/profile/*'),
('basic user','/user/security/logout'),
('editor','articles-create-categories'),
('editor','articles-create-items'),
('editor','articles-delete-his-items'),
('editor','articles-index-all-items'),
('editor','articles-index-categories'),
('editor','articles-publish-all-items'),
('editor','articles-update-all-items'),
('editor','articles-update-categories'),
('editor','articles-view-categories'),
('editor','articles-view-items'),
('publisher','articles-create-items'),
('publisher','articles-index-his-items'),
('publisher','articles-publish-his-items'),
('publisher','articles-update-his-items'),
('publisher','articles-view-categories'),
('publisher','articles-view-items'),
('rbac manager','/admin/*'),
('user manager','/user/admin/*');

/*Table structure for table `auth_rule` */

DROP TABLE IF EXISTS `auth_rule`;

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_rule` */

/*Table structure for table `form_ijin` */

DROP TABLE IF EXISTS `form_ijin`;

CREATE TABLE `form_ijin` (
  `ijin_id` int(11) NOT NULL AUTO_INCREMENT,
  `ijin_nama` int(11) NOT NULL,
  `ijin_jenis` int(11) NOT NULL,
  `ijin_ket` varchar(255) DEFAULT NULL,
  `ijin_hari` int(11) NOT NULL DEFAULT 0,
  `ijin_tanggal` date DEFAULT NULL,
  `ijin_status` int(11) NOT NULL DEFAULT 0 COMMENT '0=pengajuan, 1=disetujui',
  `ijin_created_at` datetime DEFAULT NULL,
  `ijin_created_by` int(11) NOT NULL,
  `ijin_update_at` datetime DEFAULT NULL,
  `ijin_update_by` int(11) NOT NULL,
  PRIMARY KEY (`ijin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `form_ijin` */

insert  into `form_ijin`(`ijin_id`,`ijin_nama`,`ijin_jenis`,`ijin_ket`,`ijin_hari`,`ijin_tanggal`,`ijin_status`,`ijin_created_at`,`ijin_created_by`,`ijin_update_at`,`ijin_update_by`) values 
(2,2,1,'coba saja',0,'2021-03-31',1,'2021-03-19 16:26:14',2,NULL,0),
(3,3,3,'mau nikah',0,'2021-06-08',0,'2021-03-19 16:35:37',3,NULL,0),
(4,2,1,'berenang ketepian',0,'2021-05-12',0,'2021-03-19 16:59:53',2,NULL,0);

/*Table structure for table `fungsi` */

DROP TABLE IF EXISTS `fungsi`;

CREATE TABLE `fungsi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_UNIQUE` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Data for the table `fungsi` */

insert  into `fungsi`(`id`,`nama`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,'AFTN TELEPRINTER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(2,'SSB','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(3,'HOMING','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(4,'VOICE RECORDING','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(5,'VHF A/G ADC','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(6,'VHF ER ACC USMG JAKARTA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(7,'VHF ER ACC UJOG JAKARTA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(8,'VHF ER APP SEMARANG','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(9,'VHF A/G ADC PORTABLE','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(10,'VHF A/G APP','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(11,'VHF ADC TRANSCEIVER PORTABLE','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(12,'VHF APP TRANSCEIVER PORTABLE','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(13,'ATIS TRANSMITTER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(14,'ATIS REPRODUCER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(15,'AMSC','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(16,'VSCS0','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(17,'VHF ER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(18,'VHF ER ACC UBDN JAKARTA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(19,'VHF ER ACC UTPN JAKARTA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(20,'VHF ER APP Lower East (LE) JAKARTA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(21,'VHF ER ACC FSS IV/GP JAKARTA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(22,'DVOR','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(23,'DME','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(24,'LOCATOR','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(25,'VHF A/G PORTABLE','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(26,'VHF ER ACC GP JAKARTA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(27,'VHF A/G','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0);

/*Table structure for table `groundcheck` */

DROP TABLE IF EXISTS `groundcheck`;

CREATE TABLE `groundcheck` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_gc` date NOT NULL,
  `jenis_gc` enum('Periodik','Spesial') NOT NULL,
  `peralatan_id` int(11) NOT NULL,
  `tgl_expire` date DEFAULT NULL,
  `bukti` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_groundcheck_peralatan1` (`peralatan_id`),
  CONSTRAINT `fk_groundcheck_peralatan1` FOREIGN KEY (`peralatan_id`) REFERENCES `peralatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `groundcheck` */

/*Table structure for table `isr` */

DROP TABLE IF EXISTS `isr`;

CREATE TABLE `isr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lppnpi_id` int(11) DEFAULT NULL,
  `no_rekomendasi` varchar(45) NOT NULL,
  `no_isr` varchar(45) NOT NULL,
  `masa_berlaku` date NOT NULL,
  `bukti` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_isr_lppnpi1` (`lppnpi_id`),
  CONSTRAINT `fk_isr_lppnpi1` FOREIGN KEY (`lppnpi_id`) REFERENCES `lppnpi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `isr` */

/*Table structure for table `kacab_pembina` */

DROP TABLE IF EXISTS `kacab_pembina`;

CREATE TABLE `kacab_pembina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kobu_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_UNIQUE` (`nama`),
  KEY `fk_provinsi_kobu1` (`kobu_id`),
  CONSTRAINT `fk_provinsi_kobu1` FOREIGN KEY (`kobu_id`) REFERENCES `kobu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `kacab_pembina` */

insert  into `kacab_pembina`(`id`,`nama`,`kobu_id`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,'kacab Pembina 1',1,'2019-10-23 01:34:37',1,'0000-00-00 00:00:00',0);

/*Table structure for table `kalibrasi` */

DROP TABLE IF EXISTS `kalibrasi`;

CREATE TABLE `kalibrasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` enum('FlightCom','Periodik','Spesial') NOT NULL,
  `peralatan_id` int(11) NOT NULL,
  `no_sertifikat` varchar(45) NOT NULL,
  `tgl_terbit` date NOT NULL,
  `bukti` varchar(45) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_kalibrasi_peralatan1` (`peralatan_id`),
  CONSTRAINT `fk_kalibrasi_peralatan1` FOREIGN KEY (`peralatan_id`) REFERENCES `peralatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kalibrasi` */

/*Table structure for table `kobu` */

DROP TABLE IF EXISTS `kobu`;

CREATE TABLE `kobu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_UNIQUE` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `kobu` */

insert  into `kobu`(`id`,`nama`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,'Kobu I','0000-00-00 00:00:00',0,'2019-08-18 21:31:28',1),
(2,'Kobu II','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(3,'Kobu III','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(4,'Kobu IV','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(5,'Kobu V','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(6,'Kobu VI','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(7,'Kobu VII','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(8,'Kobu VIII','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(9,'Kobu IX','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(10,'Kobu X','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0);

/*Table structure for table `lokasi_alat` */

DROP TABLE IF EXISTS `lokasi_alat`;

CREATE TABLE `lokasi_alat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_UNIQUE` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=213 DEFAULT CHARSET=latin1;

/*Data for the table `lokasi_alat` */

insert  into `lokasi_alat`(`id`,`nama`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,'ADC ROOM','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(2,'AFIS','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(3,'AIR MANIS','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(4,'AMBAIPUA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(5,'APP','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(6,'APP ROOM','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(7,'ATC/COM/METEO/BO','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(8,'ATS Building Kantor Pusat','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(9,'BABELAN BEKASI','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(10,'BALIKPAPAN','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(11,'BANDAR UDARA JOS ORMO IMSULA - MOA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(12,'BANDARA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(13,'BANDARA BADAK BONTANG','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(14,'BASE OPS','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(15,'Bengkulu','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(16,'BIAK','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(17,'BLORA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(18,'BRIEFING OFFICE','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(19,'BSH','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(20,'BUKIT KHAYANGAN','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(21,'CIGUGUR','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(22,'Cirebon (CA)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(23,'Cirebon (CRB)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(24,'COM','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(25,'COM ROOM','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(26,'COMM / GD. TOWER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(27,'COMM CENTRE','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(28,'Console ADC','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(29,'Console TMA & APP','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(30,'Dadap','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(31,'DATA TIDAK ADA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(32,'DESA KEMUNING','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(33,'DONGGALA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(34,'Dumai (DUM)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(35,'FSS','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(36,'FUNKE','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(37,'G. LINTEUNG','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(38,'GD 710/720','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(39,'GD. 710','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(40,'GD. 720','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(41,'GD. DVOR/DME','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(42,'GD. KANTOR BANDARA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(43,'GD. NAVIGASI','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(44,'GD. OPERASIONAL','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(45,'GD. PEMANCAR','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(46,'GD. TOWER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(47,'GD. TOWER LT 2','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(48,'GD. TOWER LT 4','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(49,'GD. TOWER LT DASAR','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(50,'Gd. TX PAI','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(51,'Gd.710','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(52,'Gd.710/720','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(53,'Gd.710/GD 720/Padang & Sidikalang','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(54,'GEDUNG 612','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(55,'GEDUNG DVOR','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(56,'GEDUNG DVOR R/W 25','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(57,'GEDUNG DVOR R/W 33','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(58,'GEDUNG DVOR/DME','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(59,'GEDUNG FSO','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(60,'GEDUNG GENSET UPBU','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(61,'GEDUNG GLIDE SLOPE','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(62,'GEDUNG LOC,GS,MM,OM','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(63,'GEDUNG LOCALIZER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(64,'GEDUNG MIDDLE MARKER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(65,'GEDUNG NDB','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(66,'GEDUNG OPERASI','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(67,'GEDUNG OPERASI (RADAR HEAD)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(68,'GEDUNG OPERASIONAL','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(69,'GEDUNG OPS','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(70,'GEDUNG OPSTEK','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(71,'GEDUNG OUTER MARKER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(72,'GEDUNG PEMANCAR/ RADAR','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(73,'GEDUNG RADAR','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(74,'GEDUNG SITE RADAR','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(75,'GEDUNG TELNAV','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(76,'GEDUNG TOWER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(78,'GEDUNG TOWER LT. 3','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(79,'GEDUNG TOWER LT. 5','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(80,'GEDUNG TOWER LT.3','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(81,'GEDUNG TX STATION','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(82,'GEDUNG VHF ER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(83,'GEDUNG VOR/DME','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(84,'GUDANG','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(85,'Gunung Linteung (GNLG)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(86,'GUNUNG LINTEUNG B.ACEH','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(87,'GUNUNG NONA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(88,'GUNUNG PANGANTEN','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(89,'GUNUNG T. PERAHU','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(90,'HANAN R/W','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(91,'HANAN TOWER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(92,'Indramayu (IMU)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(93,'Jambi (DJB)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(94,'JANGLI SEMARANG','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(95,'JATSC/CKG','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(96,'JUWANGI/ PURWODADI','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(97,'Kamal','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(98,'Ketapang','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(99,'KIARA CONDONG','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(100,'KINTAMANI','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(101,'KINTAMANI I','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(102,'KINTAMANI II','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(103,'KM 14','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(104,'KM 7 PARIT BARU','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(105,'KOMPEN','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(106,'KUALA NAMU','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(107,'Kutabumi','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(108,'Kutajaya','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(109,'MAKAWEIMBENG','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(110,'MAKAWEMBENG','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(111,'MALINO','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(112,'MER/MER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(113,'MER/TER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(114,'Meulaboh (MEU)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(115,'OLILIT','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(116,'Padang (PDG)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(117,'PADANG BULAN','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(118,'Palembang (PLB)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(119,'PALU','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(120,'PANCUR BATU','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(121,'Pangandaran (PGDR)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(122,'PANGKALAN BENTENG','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(123,'Pangkalpinang (PKP)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(124,'Pasar Kemis (CKG)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(125,'Pekanbaru (PKU)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(126,'PERPANJANGAN R/W 18','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(127,'PERPANJANGAN R/W 29','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(128,'PERPANJANGAN R/W 34','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(129,'POKSI TEL-NAV / GD. TOWER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(130,'POLONIA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(131,'Pontianak (PNK)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(132,'Purwakarta (PW)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(133,'R. CONTROL ATC','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(134,'R/W 09','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(135,'R/W 09/27','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(136,'R/W 10','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(137,'R/W 12','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(138,'R/W 12/30','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(139,'R/W 26','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(140,'R/W 28','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(141,'R/W 30','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(142,'R/W SIDE','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(143,'RADAR BUILDING','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(144,'RADAR HEAD','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(145,'RCC ROOM','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(146,'RUANG AFIS','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(147,'RUANG AMSC','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(148,'RUANG ATC','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(149,'RUANG BO','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(150,'RUANG COMM','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(151,'RUANG KOMPEN','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(152,'RUANG NDB','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(153,'RUANG PERALATAN','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(154,'RUANG RADIO','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(155,'RUANG TELNAV','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(156,'RUANG TELNAV & TOWER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(157,'RUANGAN NDB','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(158,'RUNWAY 04','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(159,'RUNWAY 05','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(160,'RUNWAY 07L','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(161,'RUNWAY 07R','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(162,'RUNWAY 11','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(163,'RUNWAY 15','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(164,'RUNWAY 18','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(165,'RUNWAY 22','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(166,'RUNWAY 23','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(167,'RUNWAY 25L','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(168,'RUNWAY 25R','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(169,'RUNWAY 29','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(170,'RUNWAY 33','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(171,'RUNWAY 36','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(172,'SAUMLAKI','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(173,'SEJAJAR R/W SEBELAH BARAT','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(174,'Semarang (SMG)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(175,'SHELTER DVOR/DME','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(176,'SHELTER NDB','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(177,'SIBIRU - BIRU','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(178,'Sibiru-biru (SBR)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(179,'SIDIKALANG','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(180,'Sidikalang (SDKL)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(181,'SISI KANAN R/W','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(182,'SISI KIRI R/W','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(183,'SMP. TOWER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(184,'STASIUN RADAR & VHF ER KM 16','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(185,'Tangkuban Perahu (TKB)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(186,'TANJUNG KARANG','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(187,'Tanjung Karawang (DKI)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(188,'Tanjung Pandan (TPD)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(189,'Tanjungpinang (TNJ)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(190,'TELAGA KODOK','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(191,'TELNAV','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(192,'TER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(193,'TER/TER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(194,'TIDAK ADA FASILITAS','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(195,'TIDORE','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(196,'TOWER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(197,'TOWER BARU','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(198,'TOWER KNO','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(199,'TOWER LAMA','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(200,'TOWER LT 5','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(201,'TOWER LT.1','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(202,'TOWER LT.2','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(203,'TOWER LT.3','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(204,'Tower MATSC','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(205,'TX STATION KNO','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(206,'UJUNG R/W 18','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(207,'UNIT ADC','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(208,'UNIT APP','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(209,'UNIT FSS','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(210,'Wonosari (WNSR)','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(211,'X2 MER/X2 TER','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(212,'ZITTO SYSTEM','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0);

/*Table structure for table `lppnpi` */

DROP TABLE IF EXISTS `lppnpi`;

CREATE TABLE `lppnpi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `provinsi_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_UNIQUE` (`nama`),
  KEY `fk_cabang_provinsi1` (`provinsi_id`),
  CONSTRAINT `fk_cabang_provinsi1` FOREIGN KEY (`provinsi_id`) REFERENCES `kacab_pembina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `lppnpi` */

insert  into `lppnpi`(`id`,`nama`,`provinsi_id`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,'Lppnpi 1',1,'2019-10-23 01:34:50',1,'0000-00-00 00:00:00',0);

/*Table structure for table `m_jabatan` */

DROP TABLE IF EXISTS `m_jabatan`;

CREATE TABLE `m_jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_UNIQUE` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=latin1;

/*Data for the table `m_jabatan` */

insert  into `m_jabatan`(`id`,`nama`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,' ALINCO \r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(2,' MOTOROLA \r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(3,'4RF\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(4,'ACAMS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(5,'ADVANTECH\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(6,'ADVENTECH\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(7,'ALCATEL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(8,'ALINCCO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(9,'ALINCO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(10,'ALPHA OMEGA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(11,'AMS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(12,'AMS/ASII\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(13,'AN/URN-5\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(14,'AODR\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(15,'ARINC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(16,'ARTYSIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(17,'ASII\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(18,'ASII AMS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(19,'ASII/SELEX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(20,'ASSMAN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(21,'ATALIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(22,'ATIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(23,'ATIS UHER\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(24,'AVIBIT\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(25,'AWA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(26,'AWA/INTERSCAN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(27,'BAE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(28,'BAOFENG\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(29,'BASELT\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(30,'BECKER\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(31,'BODET\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(32,'CAIXING\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(33,'CARDION\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(34,'CHINA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(35,'CODAN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(36,'COMPAQ\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(37,'COMSOFT\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(38,'COMTECH\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(39,'CPU\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(40,'DAVIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(41,'DITTEL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(42,'EIZO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(43,'ELDIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(44,'ELSA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(45,'ENLIGHT\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(46,'ERA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(47,'EVENTIDE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(48,'FERNAU\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(49,'FREE WAVE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(50,'FREQUENTIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(51,'FSG90-11\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(52,'FUNKE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(53,'GANESHA AVIONICS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(54,'GAREX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(55,'GAREX \r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(56,'HAGENUX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(57,'HANJIN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(58,'HANJIN \r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(59,'HARRIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(60,'HP\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(61,'HUGHES\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(62,'ICOM\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(63,'IDS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(64,'INALIX/DELL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(65,'INDRA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(66,'INPRIMA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(67,'INTERCOM\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(68,'INTERSCAN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(69,'INTERSOFT\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(70,'INTI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(71,'INTUS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(72,'I-TECH\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(73,'JACOTRON\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(74,'JITO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(75,'JOTRON\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(76,'JRC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(77,'KAMBIUM\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(78,'KARNA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(79,'KENWOOD\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(80,'KRAME\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(81,'LAELA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(82,'LEONARDO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(83,'LES\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(84,'LG\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(85,'MARCONI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(86,'MARCONI (OTE)\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(87,'MCRS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(88,'MDK\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(89,'MDK2\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(90,'MDR PL3200\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(91,'MESOTECH INTERNATIONAL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(92,'MICROSTEP\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(93,'MOPIENS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(94,'MOTOROLA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(95,'MULTISUN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(96,'NARDEUX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(97,'NAUTEL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(98,'NAUTEL \r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(99,'NCH\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(100,'NEC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(101,'NEPTUNO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(102,'NORMARC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(103,'NRPL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(104,'OTE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(105,'OTE SELEX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(106,'PAE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(107,'PAS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(108,'PCE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(109,'PELORUS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(110,'PELORUS LDB-102\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(111,'RICOCHET\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(112,'ROHDE & SCHWARZ\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(113,'SAC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(114,'SAFREAVIA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(115,'SCHMID\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(116,'SEEE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(117,'SELEX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(118,'SELEX OTE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(119,'SENSIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(120,'SITA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(121,'SITTI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(122,'SKYTRAX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(123,'SOFREAVIA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(124,'SPILSBURY\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(125,'STANGL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(126,'SUN AIR\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(127,'SUNAIR\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(128,'TBE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(129,'TELEFUNKEN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(130,'TELERAD\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(131,'TERMA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(132,'TERN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(133,'TERNA PLUS HMI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(134,'THALES\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(135,'THOMSON\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(136,'THOMSON CSF\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(137,'TIDAK ADA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(138,'TIDAK ADA FASILITAS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(139,'TIDAK DIKETAHUI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(140,'TOPSKY\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(141,'TOSKA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(142,'TOWER\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(143,'TUSB\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(144,'UTS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(145,'VAISALA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(146,'VERSADIAL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(147,'VERTEX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(148,'VERTEX STANDARD\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(149,'WEIERWEI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(150,'WILCOX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(151,'WOUXON\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(152,'X VOICE REC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(153,'YAESU\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(154,'YOUNG\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(155,'ZETRON\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(156,'ZITTO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(157,'crew kapal','2021-03-14 06:16:44',1,'2021-03-14 06:17:16',1),
(158,'supervisor','2021-03-14 06:18:01',1,'0000-00-00 00:00:00',0);

/*Table structure for table `m_kapal` */

DROP TABLE IF EXISTS `m_kapal`;

CREATE TABLE `m_kapal` (
  `kapal_id` int(11) NOT NULL AUTO_INCREMENT,
  `kapal_nama` varchar(255) NOT NULL,
  `kapal_created_at` datetime NOT NULL,
  `kapal_created_by` int(11) NOT NULL,
  `kapal_update_at` datetime NOT NULL,
  `kapal_update_by` int(11) NOT NULL,
  PRIMARY KEY (`kapal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `m_kapal` */

insert  into `m_kapal`(`kapal_id`,`kapal_nama`,`kapal_created_at`,`kapal_created_by`,`kapal_update_at`,`kapal_update_by`) values 
(1,'kapal roro nusantara','2021-03-15 08:06:26',1,'0000-00-00 00:00:00',0);

/*Table structure for table `m_setup_email` */

DROP TABLE IF EXISTS `m_setup_email`;

CREATE TABLE `m_setup_email` (
  `setup_email_id` int(11) NOT NULL AUTO_INCREMENT,
  `setup_email_nama` varchar(255) DEFAULT NULL,
  `setup_email_username` varchar(255) DEFAULT NULL,
  `setup_email_password` varchar(255) DEFAULT NULL,
  `setup_email_port` int(11) NOT NULL,
  `setup_email_tipe` varchar(255) DEFAULT NULL,
  `setup_email_created_at` datetime NOT NULL,
  `setup_email_created_by` int(11) NOT NULL,
  `setup_email_update_at` datetime NOT NULL,
  `setup_email_update_by` int(11) NOT NULL,
  PRIMARY KEY (`setup_email_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `m_setup_email` */

insert  into `m_setup_email`(`setup_email_id`,`setup_email_nama`,`setup_email_username`,`setup_email_password`,`setup_email_port`,`setup_email_tipe`,`setup_email_created_at`,`setup_email_created_by`,`setup_email_update_at`,`setup_email_update_by`) values 
(1,'awaluddinbinusep@gmail.com','awal12','bismillah',567,'tls','2021-03-15 15:50:31',1,'2021-03-15 15:58:09',1);

/*Table structure for table `m_setup_expired` */

DROP TABLE IF EXISTS `m_setup_expired`;

CREATE TABLE `m_setup_expired` (
  `setup_expired_id` int(11) NOT NULL AUTO_INCREMENT,
  `setup_expired_hari` int(11) NOT NULL,
  `setup_expired_created_at` datetime NOT NULL,
  `setup_expired_created_by` int(11) NOT NULL,
  `setup_expired_update_at` datetime NOT NULL,
  `setup_expired_update_by` int(11) NOT NULL,
  PRIMARY KEY (`setup_expired_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `m_setup_expired` */

insert  into `m_setup_expired`(`setup_expired_id`,`setup_expired_hari`,`setup_expired_created_at`,`setup_expired_created_by`,`setup_expired_update_at`,`setup_expired_update_by`) values 
(1,124,'2021-03-15 16:10:07',1,'2021-03-15 16:10:14',1);

/*Table structure for table `manifest_kapal` */

DROP TABLE IF EXISTS `manifest_kapal`;

CREATE TABLE `manifest_kapal` (
  `manifest_kapal_id` int(11) NOT NULL AUTO_INCREMENT,
  `manifest_kapal_nama` int(11) NOT NULL,
  `manifest_kapal_tanggal` date DEFAULT NULL,
  `manitest_kapal_ket` varchar(255) DEFAULT NULL,
  `manifest_kapal_created_at` datetime DEFAULT NULL,
  `manifest_kapal_created_by` int(11) NOT NULL,
  `manifest_kapal_updated_at` datetime DEFAULT NULL,
  `manifest_kapal_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`manifest_kapal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `manifest_kapal` */

/*Table structure for table `menu_roles` */

DROP TABLE IF EXISTS `menu_roles`;

CREATE TABLE `menu_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mr_menu` int(11) NOT NULL,
  `mr_role` int(11) NOT NULL,
  `mr_create` int(11) NOT NULL,
  `mr_read` int(11) NOT NULL,
  `mr_update` int(11) NOT NULL,
  `mr_delete` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mr_menu_mr_role` (`mr_menu`,`mr_role`),
  KEY `mr_menu_idx` (`mr_menu`),
  KEY `mr_role_idx` (`mr_role`),
  CONSTRAINT `menu_roles_ibfk_1` FOREIGN KEY (`mr_menu`) REFERENCES `menus` (`id`),
  CONSTRAINT `menu_roles_ibfk_2` FOREIGN KEY (`mr_role`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1;

/*Data for the table `menu_roles` */

insert  into `menu_roles`(`id`,`mr_menu`,`mr_role`,`mr_create`,`mr_read`,`mr_update`,`mr_delete`,`created_at`,`updated_at`,`created_by`,`updated_by`) values 
(2,2,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(3,3,1,1,1,1,1,'2019-10-09 23:16:46',NULL,1,NULL),
(4,4,1,1,1,1,1,'2019-10-09 23:16:46',NULL,1,NULL),
(5,5,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(7,6,1,1,1,1,1,'2019-10-13 17:18:31',NULL,1,NULL),
(20,13,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(21,13,2,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(22,14,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(23,14,2,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(24,15,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(25,15,2,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(26,16,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(27,16,2,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(34,20,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(35,20,2,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(36,21,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(37,21,2,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(45,1,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(46,1,2,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(47,1,3,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(48,11,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(49,11,2,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(50,11,3,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(51,22,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(52,22,2,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(53,22,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(57,12,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(58,12,2,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(59,12,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(60,7,1,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(61,7,2,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(62,7,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(66,23,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(67,23,2,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(68,23,3,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(69,24,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(70,24,2,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(71,24,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(72,25,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(73,25,2,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(74,25,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(75,26,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(76,26,2,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(77,26,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(81,27,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(82,27,2,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(83,27,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(84,8,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(85,8,2,1,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(86,8,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(87,9,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(88,9,2,1,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(89,9,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(90,10,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(91,10,2,1,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(92,10,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(102,17,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(103,17,2,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(104,17,3,0,1,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(105,28,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(106,28,2,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(107,28,3,1,1,1,0,'0000-00-00 00:00:00',NULL,1,NULL),
(108,29,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(109,29,2,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(110,29,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(111,18,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(112,18,2,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(113,18,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL),
(114,19,1,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(115,19,2,1,1,1,1,'0000-00-00 00:00:00',NULL,1,NULL),
(116,19,3,0,0,0,0,'0000-00-00 00:00:00',NULL,1,NULL);

/*Table structure for table `menus` */

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_title` varchar(255) NOT NULL,
  `menu_url` varchar(255) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `menu_parent` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu_title_menu_url` (`menu_title`,`menu_url`),
  KEY `menu_title_idx` (`menu_title`),
  KEY `menu_url_idx` (`menu_url`),
  KEY `menu_parent_idx` (`menu_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `menus` */

insert  into `menus`(`id`,`menu_title`,`menu_url`,`menu_order`,`menu_parent`,`active`,`created_at`,`updated_at`,`created_by`,`updated_by`) values 
(1,'Dashboard','dashboard',1,0,1,'2019-10-09 23:08:19','2021-03-13 03:35:56',1,1),
(2,'Settings','#',2,0,1,'2019-10-09 23:16:08','2019-10-19 10:36:08',1,1),
(3,'User','user',1,2,1,'2019-10-09 23:16:08',NULL,1,NULL),
(4,'Role','role',2,2,1,'2019-10-09 23:16:08',NULL,1,NULL),
(5,'peralatan','peralatan',5,0,1,'2019-10-13 15:51:11','2019-10-19 23:40:20',1,1),
(6,'Menu','menu',3,2,1,'2019-10-13 17:17:51',NULL,1,NULL),
(7,'Document','#',3,0,1,'2019-10-19 13:05:12','2021-03-14 06:27:07',1,1),
(8,'Kobu','kobu',3,7,1,'2019-10-19 13:37:38','2021-03-17 06:30:16',1,1),
(9,'Kacab Pembina','kacab_pembina',4,7,1,'2019-10-19 23:01:51','2021-03-17 06:30:31',1,1),
(10,'LPPNPI','lppnpi',5,7,1,'2019-10-19 23:23:04','2021-03-17 06:30:42',1,1),
(11,'Master','#',4,0,1,'2019-10-19 23:40:10','2021-03-13 03:44:15',1,1),
(12,'Jabatan','jabatan',1,11,1,'2019-10-19 23:41:25','2021-03-14 06:08:09',1,1),
(13,'Tipe','tipe',2,11,1,'2019-10-19 23:56:56',NULL,1,NULL),
(14,'Lokasi Alat','lokasi_alat',3,11,1,'2019-10-20 00:03:51',NULL,1,NULL),
(15,'Fungsi','fungsi',4,11,1,'2019-10-20 00:04:34',NULL,1,NULL),
(16,'Pelayanan','pelayanan',5,11,1,'2019-10-20 00:09:16',NULL,1,NULL),
(17,'Forms','#',6,0,1,'2019-12-02 00:52:05','2021-03-19 16:33:59',1,1),
(18,'Sertifikat 171','sertifikat171',3,17,1,'2019-12-02 00:52:52','2021-03-20 01:34:55',1,1),
(19,'ISR','isr',4,17,1,'2019-12-02 00:53:17','2021-03-20 01:34:59',1,1),
(20,'Ground Check','groundcheck',3,17,1,'2019-12-02 00:53:43',NULL,1,NULL),
(21,'Kalibrasi','kalibrasi',4,17,1,'2019-12-02 00:54:13',NULL,1,NULL),
(22,'setup email','logo_website',7,11,1,'2021-03-13 04:10:02',NULL,1,NULL),
(23,'kapal','kapal',2,11,1,'2021-03-15 06:16:11',NULL,1,NULL),
(24,'Setup email','setup_email',4,2,0,'2021-03-15 15:12:48',NULL,1,NULL),
(25,'Setup Expired Dokumen','setup_expired',5,2,1,'2021-03-15 15:59:04',NULL,1,NULL),
(26,'Arsip Kapal','arsip_kapal',1,7,1,'2021-03-15 16:11:40',NULL,1,NULL),
(27,'arsip crew','arsip_crew',2,7,1,'2021-03-17 00:36:39',NULL,1,NULL),
(28,'Form Ijin','form_ijin',1,17,0,'2021-03-18 17:00:49','2021-03-19 16:34:55',1,1),
(29,'Manifest Kapal','manifest_kapal',2,17,1,'2021-03-20 01:34:33',NULL,1,NULL);

/*Table structure for table `merk` */

DROP TABLE IF EXISTS `merk`;

CREATE TABLE `merk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_UNIQUE` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=latin1;

/*Data for the table `merk` */

insert  into `merk`(`id`,`nama`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,' ALINCO \r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(2,' MOTOROLA \r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(3,'4RF\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(4,'ACAMS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(5,'ADVANTECH\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(6,'ADVENTECH\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(7,'ALCATEL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(8,'ALINCCO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(9,'ALINCO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(10,'ALPHA OMEGA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(11,'AMS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(12,'AMS/ASII\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(13,'AN/URN-5\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(14,'AODR\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(15,'ARINC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(16,'ARTYSIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(17,'ASII\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(18,'ASII AMS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(19,'ASII/SELEX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(20,'ASSMAN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(21,'ATALIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(22,'ATIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(23,'ATIS UHER\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(24,'AVIBIT\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(25,'AWA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(26,'AWA/INTERSCAN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(27,'BAE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(28,'BAOFENG\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(29,'BASELT\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(30,'BECKER\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(31,'BODET\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(32,'CAIXING\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(33,'CARDION\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(34,'CHINA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(35,'CODAN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(36,'COMPAQ\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(37,'COMSOFT\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(38,'COMTECH\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(39,'CPU\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(40,'DAVIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(41,'DITTEL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(42,'EIZO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(43,'ELDIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(44,'ELSA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(45,'ENLIGHT\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(46,'ERA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(47,'EVENTIDE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(48,'FERNAU\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(49,'FREE WAVE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(50,'FREQUENTIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(51,'FSG90-11\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(52,'FUNKE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(53,'GANESHA AVIONICS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(54,'GAREX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(55,'GAREX \r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(56,'HAGENUX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(57,'HANJIN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(58,'HANJIN \r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(59,'HARRIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(60,'HP\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(61,'HUGHES\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(62,'ICOM\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(63,'IDS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(64,'INALIX/DELL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(65,'INDRA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(66,'INPRIMA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(67,'INTERCOM\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(68,'INTERSCAN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(69,'INTERSOFT\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(70,'INTI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(71,'INTUS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(72,'I-TECH\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(73,'JACOTRON\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(74,'JITO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(75,'JOTRON\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(76,'JRC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(77,'KAMBIUM\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(78,'KARNA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(79,'KENWOOD\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(80,'KRAME\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(81,'LAELA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(82,'LEONARDO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(83,'LES\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(84,'LG\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(85,'MARCONI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(86,'MARCONI (OTE)\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(87,'MCRS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(88,'MDK\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(89,'MDK2\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(90,'MDR PL3200\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(91,'MESOTECH INTERNATIONAL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(92,'MICROSTEP\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(93,'MOPIENS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(94,'MOTOROLA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(95,'MULTISUN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(96,'NARDEUX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(97,'NAUTEL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(98,'NAUTEL \r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(99,'NCH\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(100,'NEC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(101,'NEPTUNO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(102,'NORMARC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(103,'NRPL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(104,'OTE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(105,'OTE SELEX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(106,'PAE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(107,'PAS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(108,'PCE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(109,'PELORUS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(110,'PELORUS LDB-102\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(111,'RICOCHET\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(112,'ROHDE & SCHWARZ\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(113,'SAC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(114,'SAFREAVIA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(115,'SCHMID\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(116,'SEEE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(117,'SELEX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(118,'SELEX OTE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(119,'SENSIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(120,'SITA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(121,'SITTI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(122,'SKYTRAX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(123,'SOFREAVIA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(124,'SPILSBURY\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(125,'STANGL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(126,'SUN AIR\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(127,'SUNAIR\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(128,'TBE\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(129,'TELEFUNKEN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(130,'TELERAD\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(131,'TERMA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(132,'TERN\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(133,'TERNA PLUS HMI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(134,'THALES\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(135,'THOMSON\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(136,'THOMSON CSF\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(137,'TIDAK ADA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(138,'TIDAK ADA FASILITAS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(139,'TIDAK DIKETAHUI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(140,'TOPSKY\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(141,'TOSKA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(142,'TOWER\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(143,'TUSB\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(144,'UTS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(145,'VAISALA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(146,'VERSADIAL\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(147,'VERTEX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(148,'VERTEX STANDARD\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(149,'WEIERWEI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(150,'WILCOX\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(151,'WOUXON\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(152,'X VOICE REC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(153,'YAESU\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(154,'YOUNG\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(155,'ZETRON\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(156,'ZITTO\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0);

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values 
('m000000_000000_base',1541442610),
('m140209_132017_init',1541442619),
('m140403_174025_create_account_table',1541442619),
('m140504_113157_update_tables',1541442621),
('m140504_130429_create_token_table',1541442621),
('m140506_102106_rbac_init',1541453214),
('m140602_111327_create_menu_table',1541452985),
('m140830_171933_fix_ip_field',1541442622),
('m140830_172703_change_account_table_name',1541442622),
('m141222_110026_update_ip_field',1541442623),
('m141222_135246_alter_username_length',1541442623),
('m150614_103145_update_social_account_table',1541442624),
('m150623_212711_fix_username_notnull',1541442624),
('m151021_200401_create_article_categories_table',1564030320),
('m151021_200427_create_article_items_table',1564030497),
('m151021_200518_create_article_attachments_table',1564030497),
('m151218_234654_add_timezone_to_profile',1541442625),
('m160312_050000_create_user',1541452985),
('m160929_103127_add_last_login_at_to_user_table',1541442625),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id',1541453214);

/*Table structure for table `pelayanan` */

DROP TABLE IF EXISTS `pelayanan`;

CREATE TABLE `pelayanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `pelayanan` */

insert  into `pelayanan`(`id`,`nama`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(2,'ACC MATSC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(3,'ADC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(4,'APP\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(5,'EMERGENCY\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(6,'ACC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(7,'APP/ACC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(8,'NAVIGASI\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(9,'-\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(10,'TWR\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(11,'FSS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(12,'ACC JATSC\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(13,'AFIS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(14,'TWR/APP Combine\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(15,'AS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(16,'G/G\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(17,'TWR/FSS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(18,'TIDAK ADA FASILITAS\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0),
(19,'DATA TIDAK ADA\r','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0);

/*Table structure for table `peralatan` */

DROP TABLE IF EXISTS `peralatan`;

CREATE TABLE `peralatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fasilitas` enum('Communication','Navigation','Surveillance','ATC Automation') NOT NULL,
  `nama` varchar(45) NOT NULL,
  `lat` varchar(45) DEFAULT NULL,
  `long` varchar(45) DEFAULT NULL,
  `tahun_installasi` char(4) NOT NULL,
  `frekuensi` varchar(45) DEFAULT NULL,
  `ident` varchar(45) DEFAULT NULL,
  `merk_id` int(11) NOT NULL,
  `lokasi_alat_id` int(11) NOT NULL,
  `fungsi_id` int(11) NOT NULL,
  `pelayanan_id` int(11) NOT NULL,
  `lppnpi_id` int(11) NOT NULL,
  `poweroutput` varchar(45) DEFAULT NULL,
  `catatan` text DEFAULT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `kondisi` enum('Normal','Intermittent','U/S') NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_peralatan_lokasi_alat1` (`lokasi_alat_id`),
  KEY `fk_peralatan_fungsi1` (`fungsi_id`),
  KEY `fk_peralatan_merk1` (`merk_id`),
  KEY `fk_peralatan_cabang1` (`lppnpi_id`),
  KEY `fk_peralatan_pelayanan1` (`pelayanan_id`),
  CONSTRAINT `fk_peralatan_cabang1` FOREIGN KEY (`lppnpi_id`) REFERENCES `lppnpi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_peralatan_fungsi1` FOREIGN KEY (`fungsi_id`) REFERENCES `fungsi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_peralatan_lokasi_alat1` FOREIGN KEY (`lokasi_alat_id`) REFERENCES `lokasi_alat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_peralatan_merk1` FOREIGN KEY (`merk_id`) REFERENCES `m_jabatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_peralatan_pelayanan1` FOREIGN KEY (`pelayanan_id`) REFERENCES `pelayanan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `peralatan` */

insert  into `peralatan`(`id`,`fasilitas`,`nama`,`lat`,`long`,`tahun_installasi`,`frekuensi`,`ident`,`merk_id`,`lokasi_alat_id`,`fungsi_id`,`pelayanan_id`,`lppnpi_id`,`poweroutput`,`catatan`,`foto`,`kondisi`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,'Communication','peralatan 1','-6.373023699727276','106.79040982883612','2015','frekuensi','ident',1,1,1,2,1,'power output','catatan',NULL,'Normal','2019-10-23 01:36:34',1,'2019-11-13 21:49:25',1),
(2,'Navigation','peralatan 2','','','2015','frekuensi','ident',1,1,1,2,1,'power output','catatan',NULL,'Normal','2019-10-23 01:44:44',1,'0000-00-00 00:00:00',0),
(3,'ATC Automation','peralatan 3','','','2016','frekuensi','ident',1,1,1,2,1,'power output','catatan',NULL,'Normal','2019-10-23 01:46:31',1,'0000-00-00 00:00:00',0),
(4,'ATC Automation','peralatan 3','-6.230709223249989','106.68123126925047','2016','frekuensi','ident',1,1,1,2,1,'power output','catatan',NULL,'Normal','2019-10-23 01:46:55',1,'2019-11-10 14:39:07',1),
(5,'Communication','peralatan 4','','','2016','frekuensi','ident',2,4,3,5,1,'power output','',NULL,'Intermittent','2019-10-23 01:49:24',1,'0000-00-00 00:00:00',0),
(6,'Surveillance','peralatan 5','-6.227765547997332','106.70161605776366','2017','frekuensi','ident',5,3,2,4,1,'power output','',NULL,'Intermittent','2019-10-23 01:51:04',1,'2019-11-10 14:40:02',1),
(7,'Communication','peralatan 6','-6.373023699727276','106.79040982883612','2015','frekuensi','ident',1,1,1,2,1,'power output','catatan',NULL,'Intermittent','2019-10-23 01:36:34',1,'2019-11-13 21:49:25',1),
(8,'Surveillance','peralatan 7','','','2015','frekuensi','ident',1,1,1,2,1,'power output','catatan',NULL,'U/S','2019-10-23 01:44:44',1,'0000-00-00 00:00:00',0),
(9,'Navigation','peralatan 8','','','2016','frekuensi','ident',1,1,1,2,1,'power output','catatan',NULL,'U/S','2019-10-23 01:46:31',1,'0000-00-00 00:00:00',0),
(10,'Communication','peralatan 9','-6.230709223249989','106.68123126925047','2016','frekuensi','ident',1,1,1,2,1,'power output','catatan',NULL,'Normal','2019-10-23 01:46:55',1,'2019-11-10 14:39:07',1),
(11,'Communication','peralatan 10','-6.229088070805218','106.68440700472411','2017','frekuensi','ident',2,4,3,5,1,'power output','',NULL,'Normal','2019-10-23 01:49:24',1,'2019-11-14 10:44:18',1),
(12,'Surveillance','peralatan 11','-6.227765547997332','106.70161605776366','2018','frekuensi','ident',5,3,2,4,1,'power output','',NULL,'Normal','2019-10-23 01:51:04',1,'2019-11-10 14:40:02',1);

/*Table structure for table `profile` */

DROP TABLE IF EXISTS `profile`;

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `profile` */

insert  into `profile`(`user_id`,`name`,`public_email`,`gravatar_email`,`gravatar_id`,`location`,`website`,`bio`,`timezone`) values 
(1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_code` char(2) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_code` (`role_code`),
  KEY `role_code_idx` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role_code`,`role_name`,`active`,`created_at`,`updated_at`,`created_by`,`updated_by`) values 
(1,'00','administrator',1,'2019-10-09 23:04:49',NULL,1,NULL),
(2,'01','writer',0,'2019-10-13 16:37:39',NULL,1,NULL),
(3,'02','user',0,'2021-03-12 09:22:44',NULL,1,NULL);

/*Table structure for table `sertifikat171` */

DROP TABLE IF EXISTS `sertifikat171`;

CREATE TABLE `sertifikat171` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_amandemen` varchar(45) NOT NULL,
  `no` varchar(45) NOT NULL,
  `lppnpi_id` int(11) NOT NULL,
  `tanggal_terbit` date NOT NULL,
  `temuan` text DEFAULT NULL,
  `bukti` varchar(45) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_UNIQUE` (`no`),
  KEY `fk_sertifikat171_lppnpi1` (`lppnpi_id`),
  CONSTRAINT `fk_sertifikat171_lppnpi1` FOREIGN KEY (`lppnpi_id`) REFERENCES `lppnpi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sertifikat171` */

/*Table structure for table `social_account` */

DROP TABLE IF EXISTS `social_account`;

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_unique` (`provider`,`client_id`),
  UNIQUE KEY `account_unique_code` (`code`),
  KEY `fk_user_account` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `social_account` */

/*Table structure for table `tipe` */

DROP TABLE IF EXISTS `tipe`;

CREATE TABLE `tipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idmerk` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_UNIQUE` (`nama`),
  KEY `fk_tipe_merk1` (`idmerk`),
  CONSTRAINT `fk_tipe_merk1` FOREIGN KEY (`idmerk`) REFERENCES `m_jabatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tipe` */

/*Table structure for table `token` */

DROP TABLE IF EXISTS `token`;

CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `token` */

insert  into `token`(`user_id`,`code`,`created_at`,`type`) values 
(1,'cEv30urMV-JJyJFlhkLoAjTIge6SSD0t',1563951997,0),
(2,'M7vDsLSq8pDBX1wlxc4z9blRgKCFoofo',1543172230,0);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT 0,
  `last_login_at` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_username` (`username`),
  UNIQUE KEY `user_unique_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`email`,`password_hash`,`auth_key`,`confirmed_at`,`unconfirmed_email`,`blocked_at`,`registration_ip`,`created_at`,`updated_at`,`flags`,`last_login_at`,`status`,`password_reset_token`) values 
(1,'admin','admin@kemenhub.go.id','$2y$12$CynLkXvF28227O1dDVkbR.cHJS1k//7hDN.M/70TxKDsLWKjipPMO','tJqXJ-Nx4q4IgrlBkrDtfMPQMFwAjgc4',1566352910,NULL,NULL,'127.0.0.1',1563951997,1563951997,0,1571160476,NULL,NULL);

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ur_user` int(11) NOT NULL,
  `ur_role` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ur_user_ur_role` (`ur_user`,`ur_role`),
  KEY `ur_role` (`ur_role`),
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`ur_user`) REFERENCES `users` (`id`),
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`ur_role`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `user_roles` */

insert  into `user_roles`(`id`,`ur_user`,`ur_role`,`created_at`,`updated_at`,`created_by`,`updated_by`) values 
(1,1,1,'2019-10-09 23:06:57',NULL,1,NULL),
(16,2,2,NULL,'2021-03-17 01:16:13',NULL,1),
(17,3,3,NULL,'2021-03-17 01:30:34',NULL,1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_jabatan` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email` (`user_email`),
  KEY `user_name_idx` (`user_name`),
  KEY `user_email_idx` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`user_name`,`user_password`,`user_email`,`user_jabatan`,`active`,`created_at`,`updated_at`,`created_by`,`updated_by`) values 
(1,'admin','$2y$08$FDtAJbVHDTC6X1dVht5OouOAlygAzOKAmlXVmaC1G0vzM3eNB/0YO','admin@localhost.com',0,1,'2019-10-09 23:06:35',NULL,1,NULL),
(2,'awal','$2y$10$lZ0/wt5t5MV/KLvOFl9MJel5YXwp1TOy6ihoESUfpeNY1W8IcwSRe','awaluddinbinusep@gmail.com',157,1,'2021-03-12 09:22:25','2021-03-17 01:16:13',1,1),
(3,'hanif','$2y$10$PujC7ZooXHlITPfc9Vmw3ehjg6tz8maztNYeVFGbNmzm39G.dN7Ga','hanif@bintangpelajar.com',157,1,'2021-03-12 09:23:11','2021-03-17 01:30:34',1,1);

/*Table structure for table `v_arsip_crew` */

DROP TABLE IF EXISTS `v_arsip_crew`;

/*!50001 DROP VIEW IF EXISTS `v_arsip_crew` */;
/*!50001 DROP TABLE IF EXISTS `v_arsip_crew` */;

/*!50001 CREATE TABLE  `v_arsip_crew`(
 `arsip_crew_id` int(11) ,
 `arsip_crew_nomor` varchar(255) ,
 `arsip_crew_nama` int(11) ,
 `arsip_crew_deskripsi` varchar(255) ,
 `arsip_crew_expired` date ,
 `status_dokumen` varchar(9) ,
 `arsip_crew_file` varchar(255) ,
 `arsip_crew_created_at` datetime ,
 `id` int(11) ,
 `user_name` varchar(100) 
)*/;

/*Table structure for table `v_arsip_kapal` */

DROP TABLE IF EXISTS `v_arsip_kapal`;

/*!50001 DROP VIEW IF EXISTS `v_arsip_kapal` */;
/*!50001 DROP TABLE IF EXISTS `v_arsip_kapal` */;

/*!50001 CREATE TABLE  `v_arsip_kapal`(
 `arsip_kapal_id` int(11) ,
 `arsip_kapal_nomor` varchar(255) ,
 `arsip_kapal_nama` int(11) ,
 `arsip_kapal_deskripsi` varchar(255) ,
 `arsip_kapal_expired` date ,
 `status_dokumen_kapal` varchar(9) ,
 `arsip_kapal_file` varchar(255) ,
 `arsip_kapal_created_at` datetime ,
 `kapal_id` int(11) ,
 `kapal_nama` varchar(255) 
)*/;

/*Table structure for table `v_form_ijin` */

DROP TABLE IF EXISTS `v_form_ijin`;

/*!50001 DROP VIEW IF EXISTS `v_form_ijin` */;
/*!50001 DROP TABLE IF EXISTS `v_form_ijin` */;

/*!50001 CREATE TABLE  `v_form_ijin`(
 `ijin_id` int(11) ,
 `ijin_nama` int(11) ,
 `ijin_jenis` int(11) ,
 `ijin_Ket` varchar(255) ,
 `ijin_status` int(11) ,
 `ijin_tanggal` date ,
 `status_ijin` varchar(9) ,
 `ijin_created_at` datetime ,
 `user_id` int(11) ,
 `user_name` varchar(100) ,
 `jabatan_id` int(11) ,
 `jabatan_nama` varchar(50) 
)*/;

/*Table structure for table `v_kacab_pembina` */

DROP TABLE IF EXISTS `v_kacab_pembina`;

/*!50001 DROP VIEW IF EXISTS `v_kacab_pembina` */;
/*!50001 DROP TABLE IF EXISTS `v_kacab_pembina` */;

/*!50001 CREATE TABLE  `v_kacab_pembina`(
 `id` int(11) ,
 `nama` varchar(50) ,
 `kobu_id` int(11) ,
 `created_at` datetime ,
 `created_by` int(11) ,
 `updated_at` datetime ,
 `updated_by` int(11) ,
 `nama_kobu` varchar(20) 
)*/;

/*Table structure for table `v_kondisi_fasilitas` */

DROP TABLE IF EXISTS `v_kondisi_fasilitas`;

/*!50001 DROP VIEW IF EXISTS `v_kondisi_fasilitas` */;
/*!50001 DROP TABLE IF EXISTS `v_kondisi_fasilitas` */;

/*!50001 CREATE TABLE  `v_kondisi_fasilitas`(
 `fasilitas` enum('Communication','Navigation','Surveillance','ATC Automation') ,
 `jumlah_fasilitas` decimal(36,0) ,
 `kondisi` enum('Normal','Intermittent','U/S') ,
 `jml_kondisi` bigint(21) ,
 `jml_fasilitas` bigint(21) 
)*/;

/*Table structure for table `v_kondisi_fasilitas_2` */

DROP TABLE IF EXISTS `v_kondisi_fasilitas_2`;

/*!50001 DROP VIEW IF EXISTS `v_kondisi_fasilitas_2` */;
/*!50001 DROP TABLE IF EXISTS `v_kondisi_fasilitas_2` */;

/*!50001 CREATE TABLE  `v_kondisi_fasilitas_2`(
 `fasilitas` enum('Communication','Navigation','Surveillance','ATC Automation') ,
 `kondisi` enum('Normal','Intermittent','U/S') ,
 `jml_kondisi` bigint(21) ,
 `jml_fasilitas` bigint(21) 
)*/;

/*Table structure for table `v_lppnpi` */

DROP TABLE IF EXISTS `v_lppnpi`;

/*!50001 DROP VIEW IF EXISTS `v_lppnpi` */;
/*!50001 DROP TABLE IF EXISTS `v_lppnpi` */;

/*!50001 CREATE TABLE  `v_lppnpi`(
 `id` int(11) ,
 `nama` varchar(50) ,
 `provinsi_id` int(11) ,
 `created_at` datetime ,
 `created_by` int(11) ,
 `updated_at` datetime ,
 `updated_by` int(11) ,
 `provinsi` varchar(50) 
)*/;

/*Table structure for table `v_menu_roles` */

DROP TABLE IF EXISTS `v_menu_roles`;

/*!50001 DROP VIEW IF EXISTS `v_menu_roles` */;
/*!50001 DROP TABLE IF EXISTS `v_menu_roles` */;

/*!50001 CREATE TABLE  `v_menu_roles`(
 `id` int(11) ,
 `mr_menu` int(11) ,
 `mr_role` int(11) ,
 `mr_create` int(11) ,
 `mr_read` int(11) ,
 `mr_update` int(11) ,
 `mr_delete` int(11) ,
 `created_at` timestamp ,
 `updated_at` timestamp ,
 `created_by` int(11) ,
 `updated_by` int(11) ,
 `menu_title` varchar(255) ,
 `menu_url` varchar(255) ,
 `menu_order` int(11) ,
 `id_parent` int(11) ,
 `menu_parent` varchar(255) ,
 `role_code` char(2) ,
 `role_name` varchar(100) 
)*/;

/*Table structure for table `v_peralatan` */

DROP TABLE IF EXISTS `v_peralatan`;

/*!50001 DROP VIEW IF EXISTS `v_peralatan` */;
/*!50001 DROP TABLE IF EXISTS `v_peralatan` */;

/*!50001 CREATE TABLE  `v_peralatan`(
 `id` int(11) ,
 `fasilitas` enum('Communication','Navigation','Surveillance','ATC Automation') ,
 `nama` varchar(45) ,
 `lat` varchar(45) ,
 `long` varchar(45) ,
 `tahun_installasi` char(4) ,
 `frekuensi` varchar(45) ,
 `ident` varchar(45) ,
 `merk_id` int(11) ,
 `lokasi_alat_id` int(11) ,
 `fungsi_id` int(11) ,
 `pelayanan_id` int(11) ,
 `lppnpi_id` int(11) ,
 `poweroutput` varchar(45) ,
 `catatan` text ,
 `foto` varchar(45) ,
 `kondisi` enum('Normal','Intermittent','U/S') ,
 `created_at` datetime ,
 `created_by` int(11) ,
 `updated_at` datetime ,
 `updated_by` int(11) ,
 `merk` varchar(50) ,
 `lokasi_alat` varchar(50) ,
 `fungsi` varchar(50) ,
 `pelayanan` varchar(45) ,
 `lppnpi` varchar(50) ,
 `provinsi_id` int(11) ,
 `nama_kacab_pembina` varchar(50) ,
 `kobu_id` int(11) ,
 `nama_kobu` varchar(20) 
)*/;

/*Table structure for table `v_tipe` */

DROP TABLE IF EXISTS `v_tipe`;

/*!50001 DROP VIEW IF EXISTS `v_tipe` */;
/*!50001 DROP TABLE IF EXISTS `v_tipe` */;

/*!50001 CREATE TABLE  `v_tipe`(
 `id` int(11) ,
 `idmerk` int(11) ,
 `nama` varchar(50) ,
 `created_at` datetime ,
 `created_by` int(11) ,
 `updated_at` datetime ,
 `updated_by` int(11) ,
 `nama_merk` varchar(50) 
)*/;

/*Table structure for table `v_user` */

DROP TABLE IF EXISTS `v_user`;

/*!50001 DROP VIEW IF EXISTS `v_user` */;
/*!50001 DROP TABLE IF EXISTS `v_user` */;

/*!50001 CREATE TABLE  `v_user`(
 `user_id` int(11) ,
 `user_name` varchar(100) ,
 `user_password` varchar(255) ,
 `user_email` varchar(255) ,
 `user_jabatan` int(11) ,
 `jabatan_id` int(11) ,
 `active` int(11) ,
 `created_at` timestamp ,
 `jabatan_nama` varchar(50) 
)*/;

/*Table structure for table `v_user_roles` */

DROP TABLE IF EXISTS `v_user_roles`;

/*!50001 DROP VIEW IF EXISTS `v_user_roles` */;
/*!50001 DROP TABLE IF EXISTS `v_user_roles` */;

/*!50001 CREATE TABLE  `v_user_roles`(
 `id` int(11) ,
 `ur_user` int(11) ,
 `ur_role` int(11) ,
 `created_at` timestamp ,
 `updated_at` timestamp ,
 `created_by` int(11) ,
 `updated_by` int(11) ,
 `user_name` varchar(100) ,
 `user_password` varchar(255) ,
 `user_email` varchar(255) ,
 `active` int(11) ,
 `role_code` char(2) ,
 `role_name` varchar(100) 
)*/;

/*View structure for view v_arsip_crew */

/*!50001 DROP TABLE IF EXISTS `v_arsip_crew` */;
/*!50001 DROP VIEW IF EXISTS `v_arsip_crew` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_arsip_crew` AS (select `a`.`arsip_crew_id` AS `arsip_crew_id`,`a`.`arsip_crew_nomor` AS `arsip_crew_nomor`,`a`.`arsip_crew_nama` AS `arsip_crew_nama`,`a`.`arsip_crew_deskripsi` AS `arsip_crew_deskripsi`,`a`.`arsip_crew_expired` AS `arsip_crew_expired`,case when curdate() > `a`.`arsip_crew_expired` then 'Expired' else 'Unexpired' end AS `status_dokumen`,`a`.`arsip_crew_file` AS `arsip_crew_file`,`a`.`arsip_crew_created_at` AS `arsip_crew_created_at`,`u`.`id` AS `id`,`u`.`user_name` AS `user_name` from (`arsip_crew` `a` left join `users` `u` on(`a`.`arsip_crew_nama` = `u`.`id`))) */;

/*View structure for view v_arsip_kapal */

/*!50001 DROP TABLE IF EXISTS `v_arsip_kapal` */;
/*!50001 DROP VIEW IF EXISTS `v_arsip_kapal` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_arsip_kapal` AS (select `a`.`arsip_kapal_id` AS `arsip_kapal_id`,`a`.`arsip_kapal_nomor` AS `arsip_kapal_nomor`,`a`.`arsip_kapal_nama` AS `arsip_kapal_nama`,`a`.`arsip_kapal_deskripsi` AS `arsip_kapal_deskripsi`,`a`.`arsip_kapal_expired` AS `arsip_kapal_expired`,case when curdate() > `a`.`arsip_kapal_expired` then 'Expired' else 'Unexpired' end AS `status_dokumen_kapal`,`a`.`arsip_kapal_file` AS `arsip_kapal_file`,`a`.`arsip_kapal_created_at` AS `arsip_kapal_created_at`,`k`.`kapal_id` AS `kapal_id`,`k`.`kapal_nama` AS `kapal_nama` from (`arsip_kapal` `a` left join `m_kapal` `k` on(`a`.`arsip_kapal_nama` = `k`.`kapal_id`))) */;

/*View structure for view v_form_ijin */

/*!50001 DROP TABLE IF EXISTS `v_form_ijin` */;
/*!50001 DROP VIEW IF EXISTS `v_form_ijin` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_form_ijin` AS (select `f`.`ijin_id` AS `ijin_id`,`f`.`ijin_nama` AS `ijin_nama`,`f`.`ijin_jenis` AS `ijin_jenis`,`f`.`ijin_ket` AS `ijin_Ket`,`f`.`ijin_status` AS `ijin_status`,`f`.`ijin_tanggal` AS `ijin_tanggal`,case when `f`.`ijin_status` = 0 then 'Pengajuan' when `f`.`ijin_status` = 1 then 'Disetujui' end AS `status_ijin`,`f`.`ijin_created_at` AS `ijin_created_at`,`u`.`id` AS `user_id`,`u`.`user_name` AS `user_name`,`j`.`id` AS `jabatan_id`,`j`.`nama` AS `jabatan_nama` from ((`form_ijin` `f` left join `users` `u` on(`f`.`ijin_nama` = `u`.`id`)) left join `m_jabatan` `j` on(`u`.`user_jabatan` = `j`.`id`))) */;

/*View structure for view v_kacab_pembina */

/*!50001 DROP TABLE IF EXISTS `v_kacab_pembina` */;
/*!50001 DROP VIEW IF EXISTS `v_kacab_pembina` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_kacab_pembina` AS select `kp`.`id` AS `id`,`kp`.`nama` AS `nama`,`kp`.`kobu_id` AS `kobu_id`,`kp`.`created_at` AS `created_at`,`kp`.`created_by` AS `created_by`,`kp`.`updated_at` AS `updated_at`,`kp`.`updated_by` AS `updated_by`,`k`.`nama` AS `nama_kobu` from (`kacab_pembina` `kp` join `kobu` `k` on(`kp`.`kobu_id` = `k`.`id`)) */;

/*View structure for view v_kondisi_fasilitas */

/*!50001 DROP TABLE IF EXISTS `v_kondisi_fasilitas` */;
/*!50001 DROP VIEW IF EXISTS `v_kondisi_fasilitas` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_kondisi_fasilitas` AS select `peralatan`.`fasilitas` AS `fasilitas`,(select sum(`v_peralatan`.`fasilitas`) from `v_peralatan` where `v_peralatan`.`fasilitas` = `peralatan`.`fasilitas` group by `v_peralatan`.`fasilitas`) AS `jumlah_fasilitas`,`peralatan`.`kondisi` AS `kondisi`,count(`peralatan`.`kondisi`) AS `jml_kondisi`,count(`peralatan`.`fasilitas`) AS `jml_fasilitas` from `peralatan` group by `peralatan`.`fasilitas`,`peralatan`.`kondisi` */;

/*View structure for view v_kondisi_fasilitas_2 */

/*!50001 DROP TABLE IF EXISTS `v_kondisi_fasilitas_2` */;
/*!50001 DROP VIEW IF EXISTS `v_kondisi_fasilitas_2` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_kondisi_fasilitas_2` AS select `peralatan`.`fasilitas` AS `fasilitas`,`peralatan`.`kondisi` AS `kondisi`,count(`peralatan`.`kondisi`) AS `jml_kondisi`,count(`peralatan`.`fasilitas`) AS `jml_fasilitas` from `peralatan` group by `peralatan`.`fasilitas`,`peralatan`.`kondisi` */;

/*View structure for view v_lppnpi */

/*!50001 DROP TABLE IF EXISTS `v_lppnpi` */;
/*!50001 DROP VIEW IF EXISTS `v_lppnpi` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_lppnpi` AS select `l`.`id` AS `id`,`l`.`nama` AS `nama`,`l`.`provinsi_id` AS `provinsi_id`,`l`.`created_at` AS `created_at`,`l`.`created_by` AS `created_by`,`l`.`updated_at` AS `updated_at`,`l`.`updated_by` AS `updated_by`,`kp`.`nama` AS `provinsi` from (`lppnpi` `l` join `kacab_pembina` `kp` on(`l`.`provinsi_id` = `kp`.`id`)) */;

/*View structure for view v_menu_roles */

/*!50001 DROP TABLE IF EXISTS `v_menu_roles` */;
/*!50001 DROP VIEW IF EXISTS `v_menu_roles` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_menu_roles` AS select `mr`.`id` AS `id`,`mr`.`mr_menu` AS `mr_menu`,`mr`.`mr_role` AS `mr_role`,`mr`.`mr_create` AS `mr_create`,`mr`.`mr_read` AS `mr_read`,`mr`.`mr_update` AS `mr_update`,`mr`.`mr_delete` AS `mr_delete`,`mr`.`created_at` AS `created_at`,`mr`.`updated_at` AS `updated_at`,`mr`.`created_by` AS `created_by`,`mr`.`updated_by` AS `updated_by`,`m`.`menu_title` AS `menu_title`,`m`.`menu_url` AS `menu_url`,`m`.`menu_order` AS `menu_order`,`m`.`menu_parent` AS `id_parent`,`mp`.`menu_title` AS `menu_parent`,`r`.`role_code` AS `role_code`,`r`.`role_name` AS `role_name` from (((`menu_roles` `mr` join `menus` `m` on(`mr`.`mr_menu` = `m`.`id`)) join `roles` `r` on(`mr`.`mr_role` = `r`.`id`)) left join `menus` `mp` on(`m`.`menu_parent` = `mp`.`id`)) */;

/*View structure for view v_peralatan */

/*!50001 DROP TABLE IF EXISTS `v_peralatan` */;
/*!50001 DROP VIEW IF EXISTS `v_peralatan` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_peralatan` AS select `p`.`id` AS `id`,`p`.`fasilitas` AS `fasilitas`,`p`.`nama` AS `nama`,`p`.`lat` AS `lat`,`p`.`long` AS `long`,`p`.`tahun_installasi` AS `tahun_installasi`,`p`.`frekuensi` AS `frekuensi`,`p`.`ident` AS `ident`,`p`.`merk_id` AS `merk_id`,`p`.`lokasi_alat_id` AS `lokasi_alat_id`,`p`.`fungsi_id` AS `fungsi_id`,`p`.`pelayanan_id` AS `pelayanan_id`,`p`.`lppnpi_id` AS `lppnpi_id`,`p`.`poweroutput` AS `poweroutput`,`p`.`catatan` AS `catatan`,`p`.`foto` AS `foto`,`p`.`kondisi` AS `kondisi`,`p`.`created_at` AS `created_at`,`p`.`created_by` AS `created_by`,`p`.`updated_at` AS `updated_at`,`p`.`updated_by` AS `updated_by`,`m`.`nama` AS `merk`,`la`.`nama` AS `lokasi_alat`,`f`.`nama` AS `fungsi`,`ply`.`nama` AS `pelayanan`,`l`.`nama` AS `lppnpi`,`l`.`provinsi_id` AS `provinsi_id`,`kp`.`nama` AS `nama_kacab_pembina`,`kp`.`kobu_id` AS `kobu_id`,`kb`.`nama` AS `nama_kobu` from (((((((`peralatan` `p` join `merk` `m` on(`p`.`merk_id` = `m`.`id`)) join `lokasi_alat` `la` on(`p`.`lokasi_alat_id` = `la`.`id`)) join `fungsi` `f` on(`p`.`fungsi_id` = `f`.`id`)) join `pelayanan` `ply` on(`p`.`pelayanan_id` = `ply`.`id`)) join `lppnpi` `l` on(`p`.`lppnpi_id` = `l`.`id`)) join `kacab_pembina` `kp` on(`l`.`provinsi_id` = `kp`.`id`)) join `kobu` `kb` on(`kp`.`kobu_id` = `kb`.`id`)) */;

/*View structure for view v_tipe */

/*!50001 DROP TABLE IF EXISTS `v_tipe` */;
/*!50001 DROP VIEW IF EXISTS `v_tipe` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_tipe` AS select `t`.`id` AS `id`,`t`.`idmerk` AS `idmerk`,`t`.`nama` AS `nama`,`t`.`created_at` AS `created_at`,`t`.`created_by` AS `created_by`,`t`.`updated_at` AS `updated_at`,`t`.`updated_by` AS `updated_by`,`m`.`nama` AS `nama_merk` from (`tipe` `t` join `merk` `m` on(`t`.`idmerk` = `m`.`id`)) */;

/*View structure for view v_user */

/*!50001 DROP TABLE IF EXISTS `v_user` */;
/*!50001 DROP VIEW IF EXISTS `v_user` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user` AS (select `a`.`id` AS `user_id`,`a`.`user_name` AS `user_name`,`a`.`user_password` AS `user_password`,`a`.`user_email` AS `user_email`,`a`.`user_jabatan` AS `user_jabatan`,`j`.`id` AS `jabatan_id`,`a`.`active` AS `active`,`a`.`created_at` AS `created_at`,`j`.`nama` AS `jabatan_nama` from (`users` `a` left join `m_jabatan` `j` on(`a`.`user_jabatan` = `j`.`id`))) */;

/*View structure for view v_user_roles */

/*!50001 DROP TABLE IF EXISTS `v_user_roles` */;
/*!50001 DROP VIEW IF EXISTS `v_user_roles` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user_roles` AS select `ur`.`id` AS `id`,`ur`.`ur_user` AS `ur_user`,`ur`.`ur_role` AS `ur_role`,`ur`.`created_at` AS `created_at`,`ur`.`updated_at` AS `updated_at`,`ur`.`created_by` AS `created_by`,`ur`.`updated_by` AS `updated_by`,`u`.`user_name` AS `user_name`,`u`.`user_password` AS `user_password`,`u`.`user_email` AS `user_email`,`u`.`active` AS `active`,`r`.`role_code` AS `role_code`,`r`.`role_name` AS `role_name` from ((`user_roles` `ur` join `users` `u` on(`ur`.`ur_user` = `u`.`id`)) join `roles` `r` on(`ur`.`ur_role` = `r`.`id`)) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
